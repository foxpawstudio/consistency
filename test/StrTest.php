<?php

declare(strict_types=1);

use Foxpaw\Consistency\Str;
use PHPUnit\Framework\Error\Error;
use PHPUnit\Framework\TestCase;


/**
 * Str Test
 *
 * Tests the Str class.
 */
final class StrTest extends TestCase {

  /**
   * Test Append Preserve Original
   *
   * Test append preserves the originals.
   * @return void
   */
  public function testAppendPreserveOriginal( ) : void {
    $start = 'ab';
    $end   = 'cd';
    Str::append($start, $end);
    $this->assertSame('ab', $start);
    $this->assertSame('cd', $end);
  }


  /**
   * Test Append Standard
   *
   * Test a standard append.
   * @return void
   */
  public function testAppendStandard( ) : void {
    $this->assertSame('abcd', Str::append('ab', 'cd'));
  }


  /**
   * Test Append Empty
   *
   * Test append with empty strings.
   * @return void
   */
  public function testAppendEmpty( ) : void {
    $this->assertSame('ab', Str::append('ab', ''));
    $this->assertSame('cd', Str::append('', 'cd'));
    $this->assertSame('', Str::append('', ''));
  }


  /**
   * Test At Preserve Original
   *
   * Tests at preserves the original.
   * @return void
   */
  public function testAtPreserveOriginal( ) : void {
    $test = 'Sample';
    Str::at($test, 1);
    $this->assertSame('Sample', $test);
  }


  /**
   * Test At Positive Index
   *
   * Tests at with a positive index.
   * @return void
   */
  public function testAtPositiveIndex( ) : void {
    $test = 'Sample';
    $this->assertSame('a', Str::at($test, 1));
  }


  /**
   * Test At Zero Index
   *
   * Test at with a zero index.
   * @return void
   */
  public function testAtZeroIndex( ) : void {
    $test = 'Sample';
    $this->assertSame('S', Str::at($test, 0));
  }


  /**
   * Test At Negative Index
   *
   * Tests at with a negative index.
   * @return void
   */
  public function testAtNegativeIndex( ) : void {
    $test = 'Sample';
    $this->assertSame('e', Str::at($test, -1));
  }


  /**
   * Test At Boundary
   *
   * Test at on boundary character.
   * @return void
   */
  public function testAtBoundary( ) : void {
    $test = 'Sample';
    $this->assertSame('e', Str::at($test, 5));
    $this->assertSame('', Str::at($test, 6));
  }


  /**
   * Test At Out of Bounds
   *
   * Test at with out of bounds values.
   * @return void
   */
  public function testAtOutOfBounds( ) : void {
    $test = 'Sample';
    $this->assertSame('', Str::at($test, 25));
    $this->assertSame('', Str::at($test, -25));
  }


  /**
   * Test At Empty String
   *
   * Test at on an empty string.
   * @return void
   */
  public function testAtEmptyString( ) : void {
    $test = '';
    $this->assertSame('', Str::at($test, 5));
    $this->assertSame('', Str::at($test, -5));
    $this->assertSame('', Str::at($test, 0));
  }


  /**
   * Tests Camel Preserves Original
   *
   * Tests camel preserves the original.
   * @return void
   */
  public function testCamelPreserveOriginal( ) : void {
    $test = 'Sample String';
    Str::camel($test);
    $this->assertSame('Sample String', $test);
  }
  

  /**
   * Test Camel Delimiters
   *
   * Tests that camel splits on the correct delimiters ( _,.;:).
   * @return void
   */
  public function testCamelDelimiters( ) : void {
    $this->assertSame('sampleText', Str::camel('sample text'));
    $this->assertSame('sampleText', Str::camel('sample_text'));
    $this->assertSame('sampleText', Str::camel('sample,text'));
    $this->assertSame('sampleText', Str::camel('sample.text'));
    $this->assertSame('sampleText', Str::camel('sample;text'));
    $this->assertSame('sampleText', Str::camel('sample:text'));
  }


  /**
   * Test Camel Capitals
   *
   * Tests camel correctly handles capital letter word splits.
   * @return void
   */
  public function testCamelCapitals( ) : void { 
    $this->assertSame('sampleTextCaps', Str::camel('SampleTextCAPS'));
    $this->assertSame('sampleTextCaps', Str::camel('Sample Text_CAPS'));
  }

  /**
   * Test Camel Other Chars
   *
   * Tests camel preserves other characters.
   * @return void
   */
  public function testCamelOtherChars( ) : void {
    $this->assertSame('sample-text101', Str::camel('sample-text101'));
    $this->assertSame('sample101text', Str::camel('Sample101text'));
    $this->assertSame('sample101text', Str::camel('Sample101TEXT'));
  }


  /**
   * Test Camel Empty
   *
   * Test camel on an empty string.
   * @return void
   */
  public function testCamelEmpty( ) : void {
    $this->assertSame('', Str::camel(''));
  }


  /**
   * Test Chunk Preserve Original
   *
   * Test chunk preserves the orignial.
   * @return void
   */
  public function testChunkPreserveOriginal( ) : void {
    $str = 'Sample';
    Str::chunk($str, 2);
    $this->assertSame('Sample', $str);
  }


  /**
   * Test Chunk Positive Size
   *
   * Tests chunk functions with positive sizes.
   * @return void
   */
  public function testChunkPositiveSize( ) : void {
    $this->assertSame(['ab', 'cd', 'ef'], Str::chunk('abcdef', 2));
    $this->assertSame(['abcd', 'ef'], Str::chunk('abcdef', 4));
    $this->assertSame(['abcdef'], Str::chunk('abcdef', 10));
  }


  /**
   * Test Chunk Zero Size
   *
   * Test chunk with a zero size.
   * @return void
   */
  public function testChunkZeroSize( ) : void {
    $this->assertSame([ ], Str::chunk('abcdef', 0));
  }


  /**
   * Test Chunk Negative Size
   *
   * Test chunk with a neative size.
   * @return void
   */
  public function testChunkNegativeSize( ) : void {
    $this->assertSame(['ab', 'cd', 'ef'], Str::chunk('abcdef', -2));
    $this->assertSame(['ab', 'cdef'], Str::chunk('abcdef', -4));
    $this->assertSame(['abcdef'], Str::chunk('abcdef', -10));
  }


  /**
   * Test Chunk Empty String
   *
   * Test chunk with an empty string.
   * @return void
   */
  public function testChunkEmptyString( ) : void {
    $this->assertSame([''], Str::chunk('', 1));
    $this->assertSame([''], Str::chunk('', 3));
    $this->assertSame([ ], Str::chunk('', 0));
    $this->assertSame([''], Str::chunk('', -1));
  }


  /**
   * Test Contains Preserve Original
   *
   * Test contains preserves the original.
   * @return void
   */
  public function testContainsPreserveOriginal( ) : void {
    $test = 'Sample';
    Str::contains($test, 'S');
    $this->assertSame('Sample', $test);
  }


  /**
   * Test Contains Success
   *
   * Tests a success contains query.
   * @return void
   */
  public function testContainsSuccess( ) : void {
    $this->assertTrue(Str::contains('Sample', 'amp'));
  }


  /**
   * Test Contains Fail
   *
   * Test a fail contains query.
   * @return void
   */
  public function testContainsFail( ) : void {
    $this->assertFalse(Str::contains('Sample', 'test'));
    $this->assertFalse(Str::contains('Sample', 'amP'));
  }


  /**
   * Test Contains Longer Search
   *
   * Test contains where the search is longer.
   * @return void
   */
  public function testContainsLongerSearch( ) : void {
    $this->assertFalse(Str::contains('ample', 'Sample'));
  }


  /**
   * Test Contains Empty
   *
   * Tests contains with empty strings.
   * @return void
   */
  public function testContainsEmpty( ) : void {
    $this->assertFalse(Str::contains('Sample', ''));
    $this->assertFalse(Str::contains('', 'Sample'));
    $this->assertFalse(Str::contains('', ''));
  }

  
  /**
   * Test Count Preserve Original
   *
   * Test count preserves the original.
   * @return void
   */
  public function testCountPreserveOriginal( ) : void {
    $str    = 'Sample';
    $search = 'amp';
    Str::count($str, $search);
    $this->assertSame('Sample', $str);
    $this->assertSame('amp', $search);
  }


  /**
   * Test Count Correct
   *
   * Test count is correct.
   * @return void
   */
  public function testCountCorrect( ) : void {
    $this->assertSame(1, Str::count('Sample', 'a'));
    $this->assertSame(3, Str::count('Saaample', 'a'));
  }


  /**
   * Test Count Empty
   *
   * Test count with empty strings.
   * @return void
   */
  public function testCountEmpty( ) : void {
    $this->assertSame(0, Str::count('Sample', ''));
    $this->assertSame(0, Str::count('', 'Sample'));
    $this->assertSame(0, Str::count('', ''));
  }


  /**
   * Test Count None
   *
   * Test count with no overlap.
   * @return void
   */
  public function testCountNone( ) : void {
    $this->assertSame(0, Str::count('aaa', 'bbb'));
  }


  /**
   * Test Count Longer Search
   *
   * Test count where search is longer than strength.
   * @return void
   */
  public function testCountLongerSearch( ) : void {
    $this->assertSame(0, Str::count('aaa', 'aaaaaa'));
  }


  /**
   * Test Count Case
   *
   * Test count with case.
   * @return void
   */
  public function testCountCase( ) : void {
    $this->assertSame(1, Str::count('aA', 'a'));
    $this->assertSame(1, Str::count('aA', 'A'));
  }


  /**
   * Test Count Same
   *
   * Test count when str and search are the same.
   * @return void
   */
  public function testCountSame( ) : void {
    $this->assertSame(1, Str::count('aaa', 'aaa'));
  }


  /**
   * Test Delete Preserve Original
   *
   * Test delete preserves the original.
   * @return void
   */
  public function testDeletePreserveOriginal( ) : void {
    $test = 'Test';
    Str::delete($test, 0);
    $this->assertSame('Test', $test);
  }


  /**
   * Test Delete Positive
   *
   * Test delete with a positive offset.
   * @return void
   */
  public function testDeletePositive( ) : void {
    $this->assertSame('tst', Str::delete('test', 1));
  }


  /**
   * Test Delete Negative
   *
   * Test deleting with a negative offset.
   * @return void
   */
  public function testDeleteNegative( ) : void {
    $this->assertSame('tes', Str::delete('test', -1));
  }


  /**
   * Test Delete Zero
   *
   * Test delete the zeroth offset.
   * @return void
   */
  public function testDeleteZero( ) : void {
    $this->assertSame('est', Str::delete('test', 0));
  }


  /**
   * Test Delete Out Of Bounds
   *
   * Test delete out of bounds.
   * @return void
   */
  public function testDeleteOutOfBounds( ) : void {
    $this->assertSame('test', Str::delete('test', 100));
    $this->assertSame('test', Str::delete('test', -100));
  }


  /**
   * Test Delete Empty
   *
   * Test delete on an empty string.
   * @return void
   */
  public function testDeleteEmpty( ) : void {
    $this->assertSame('', Str::delete('', 100));
    $this->assertSame('', Str::delete('', -100));
    $this->assertSame('', Str::delete('', 0));
  }


  /**
   * Test Ends With Preserve Original
   *
   * Tests ends with preserves the original.
   * @return void
   */
  public function testEndsWithPreserveOriginal( ) : void {
    $str = 'Sample';
    $end = 'ple';
    Str::endsWith($str, $end);
    $this->assertSame('Sample', $str);
    $this->assertSame('ple', $end);
  }


  /**
   * Test Ends With True Case
   *
   * Tests a true case of endsWith.
   * @return void
   */
  public function testEndsWithTrueCase( ) : void {
    $this->assertTrue(Str::endsWith('Sample', 'e'));
    $this->assertTrue(Str::endsWith('Sample', 'ample'));
  }


  /**
   * Test Ends With False Case
   *
   * Test false case of endsWith.
   * @return void
   */
  public function testEndsWithFalseCase( ) : void {
    $this->assertFalse(Str::endsWith('Sample', 'z'));
    $this->assertFalse(Str::endsWith('Sample', 'zzz'));
  }


  /**
   * Test Ends With Empty
   *
   * Test endsWith using empty strings.
   * @return void
   */
  public function testEndsWithEmpty( ) : void {
    $this->assertFalse(Str::endsWith('Sample', ''));
    $this->assertFalse(Str::endsWith('', 'Sample'));
    $this->assertFalse(Str::endsWith('', ''));
  }


  /**
   * Test Ends With Longer End
   *
   * Test endsWith with a longer ending.
   * @return void
   */
  public function testEndsWithLongerEnd( ) : void {
    $this->assertFalse(Str::endsWith('Sample', 'SampleSample'));
  }

  /**
   * Test From Preserve Original
   *
   * Test from preserves the original.
   * @return void
   */
  public function testFromPreserveOriginal( ) : void {
    $str  = 'Sample';
    $from = 'a';
    Str::from($str, $from);
    $this->assertSame('Sample', $str);
    $this->assertSame('a', $from);
  }


  /**
   * Test From Single Find
   *
   * Test from when there is one found sequence.
   * @return void
   */
  public function testFromSingleFind( ) : void {
    $this->assertSame('ample', Str::from('Sample', 'a'));
    $this->assertSame('ample', Str::from('Sample', 'amp'));
  }


  /**
   * Test From Multiple Finds
   *
   * Test from when multiple instances of the find string exist.
   * @return void
   */
  public function testFromMultipleFinds( ) : void {
    $this->assertSame('b_b_c', Str::from('a_b_b_c', 'b'));
    $this->assertSame('bb_bb_cc', Str::from('aa_bb_bb_cc', 'bb'));
  }


  /**
   * Test From Not Found
   *
   * Test from when not found.
   * @return void
   */
  public function testFromNotFound( ) : void {
    $this->assertSame('', Str::from('Sample', 'z'));
    $this->assertSame('', Str::from('Sample', 'zzz'));
  }


  /**
   * Test From Delimiter Longer
   *
   * Test from when the delimiter is longer.
   * @return void
   */
  public function testFromDelimiterLonger( ) : void {
    $this->assertSame('', Str::from('aaa', 'aaaaaa'));
  }


  /**
   * Test From Empty
   *
   * Test from with empty strings.
   * @return void
   */
  public function testFromEmpty( ) : void {
    $this->assertSame('', Str::from('Sample', ''));
    $this->assertSame('', Str::from('', 'Sample'));
    $this->assertSame('', Str::from('', ''));
  }

  

  /**
   * Tests Hyphen Preserves Original
   *
   * Tests hyphen preserves the original.
   * @return void
   */
  public function testHyphenPreserveOriginal( ) : void {
    $test = 'Sample String';
    Str::hyphen($test);
    $this->assertSame('Sample String', $test);
  }
  

  /**
   * Test Hyphen Delimiters
   *
   * Tests that hyphen splits on the correct delimiters ( _,.;:).
   * @return void
   */
  public function testHyphenDelimiters( ) : void {
    $this->assertSame('sample-text', Str::hyphen('sample text'));
    $this->assertSame('sample-text', Str::hyphen('sample_text'));
    $this->assertSame('sample-text', Str::hyphen('sample,text'));
    $this->assertSame('sample-text', Str::hyphen('sample.text'));
    $this->assertSame('sample-text', Str::hyphen('sample;text'));
    $this->assertSame('sample-text', Str::hyphen('sample:text'));
  }


  /**
   * Test Hyphen Capitals
   *
   * Tests hyphen correctly handles capital letter word splits.
   * @return void
   */
  public function testHyphenCapitals( ) : void { 
    $this->assertSame('Sample-Text-CAPS', Str::hyphen('SampleTextCAPS'));
    $this->assertSame('Sample-Text-CAPS', Str::hyphen('Sample Text_CAPS'));
  }

  /**
   * Test Hyphen Other Chars
   *
   * Tests hyphen preserves other characters.
   * @return void
   */
  public function testHyphenOtherChars( ) : void {
    $this->assertSame('sample-text101', Str::hyphen('sample-text101'));
    $this->assertSame('Sample101text', Str::hyphen('Sample101text'));
    $this->assertSame('Sample101TEXT', Str::hyphen('Sample101TEXT'));
  }


  /**
   * Test Hyphen Empty
   *
   * Test hyphen on an empty string.
   * @return void
   */
  public function testHyphenEmpty( ) : void {
    $this->assertSame('', Str::hyphen(''));
  }


  /**
   * Test Length Preserve Original
   *
   * Test length preserves the original.
   * @return void
   */
  public function testLengthPreserveOriginal( ) : void {
    $test = 'Sample';
    Str::length($test);
    $this->assertSame('Sample', $test);
  }


  /**
   * Test Length Calculate
   *
   * Test the length correctly calculates.
   * @return void
   */
  public function testLengthCalculate( ) : void {
    $this->assertSame(6, Str::length('Sample'));
  }


  /**
   * Test Length Empty
   *
   * Test length on an empty string.
   * @return void
   */
  public function testLengthEmpty( ) : void {
    $this->assertSame(0, Str::length(''));
  }


  /**
   * Test Lower Preserve Original
   *
   * Test lower preserves the original.
   * @return void
   */
  public function testLowerPreserveOriginal( ) : void {
    $test = 'Sample';
    Str::lower($test);
    $this->assertSame('Sample', $test);
  }


  /**
   * Test Lower Correct
   *
   * Tests lower functions correctly.
   * @return void
   */
  public function testLowerCorrect( ) : void {
    $this->assertSame('lower', Str::lower('LoWeR'));
  }


  /**
   * Test Lower Special Chars
   *
   * Test lower with special characters.
   * @return void
   */
  public function testLowerSpecialChars( ) : void {
    $this->assertSame(' -_1;:<>.,?/\\', Str::lower(' -_1;:<>.,?/\\'));
  }


  /**
   * Test Pos Preserve Original
   *
   * Test pos preserves the original.
   * @return void
   */
  public function testPosPreserveOriginal( ) : void {
    $str    = 'abcd';
    $search = 'a';
    Str::pos($str, $search);
    $this->assertSame('abcd', $str);
    $this->assertSame('a', $search);
  }


  /**
   * Test Pos Single
   *
   * Test pos with a single match.
   * @return void
   */
  public function testPosSingle( ) : void {
    $this->assertSame(1, Str::pos('abcd', 'b'));
    $this->assertSame(2, Str::pos('aabbccdd', 'bb'));
  }


  /**
   * Test Pos Multiple
   *
   * Test pos with multiple matches.
   * @return void
   */
  public function testPosMultiple( ) : void {
    $this->assertSame(1, Str::pos('abcb', 'b'));
    $this->assertSame(1, Str::pos('abbcbb', 'bb'));
  }


  /**
   * Test Pos Not Found
   *
   * Test pos when search not found.
   * @return void
   */
  public function testPosNotFound( ) : void {
    $this->assertSame(-1, Str::pos('aaa', 'b'));
    $this->assertSame(-1, Str::pos('aaa', 'bb'));
  }


  /**
   * Test Pos Start
   *
   * Test pos when the first element is the correct pos.
   * @return void
   */
  public function testPosStart( ) : void {
    $this->assertSame(0, Str::pos('abcd', 'a'));
    $this->assertSame(0, Str::pos('aabcd', 'aa'));
  }


  /**
   * Test Pos Same
   *
   * Test pos when the str and search are the same.
   * @return void
   */
  public function testPosSame( ) : void {
    $this->assertSame(0, Str::pos('a', 'a'));
    $this->assertSame(0, Str::pos('abc', 'abc'));
  }


  /**
   * Test Pos Empty
   *
   * Test pos when empty strings provided.
   * @return void
   */
  public function testPosEmpty( ) : void {
    $this->assertSame(-1, Str::pos('abc', ''));
    $this->assertSame(-1, Str::pos('', 'abc'));
    $this->assertSame(-1, Str::pos('', ''));
  }


  /**
   * Test Pos Search Longer
   *
   * Test pos when search is longer.
   * @return void
   */
  public function testPosSearchLonger( ) : void {
    $this->assertSame(-1, Str::pos('abc', 'abcd'));
  }

  /**
   * Test Prepend Preserve Original
   *
   * Test prepend preserves the originals.
   * @return void
   */
  public function testPrependPreserveOriginal( ) : void {
    $original = 'ab';
    $prefix   = 'cd';
    Str::prepend($original, $prefix);
    $this->assertSame('ab', $original);
    $this->assertSame('cd', $prefix);
  }


  /**
   * Test Prepend Standard
   *
   * Test a standard prepend.
   * @return void
   */
  public function testPrependStandard( ) : void {
    $this->assertSame('cdab', Str::prepend('ab', 'cd'));
  }


  /**
   * Test Prepend Empty
   *
   * Test prepend with empty strings.
   * @return void
   */
  public function testPrependEmpty( ) : void {
    $this->assertSame('ab', Str::prepend('ab', ''));
    $this->assertSame('cd', Str::prepend('', 'cd'));
    $this->assertSame('', Str::prepend('', ''));
  }


  /**
   * Test Regex Replace Preserve Original
   *
   * Test regexReplace preserves original
   * @return void
   */
  public function testRegexReplacePreserveOriginal( ) : void {
    $str   = 'Sample';
    $regex = '/[a-z]/';
    Str::regexReplace($str, $regex, '');
    $this->assertSame('Sample', $str);
    $this->assertSame('/[a-z]/', $regex);
  }

  /**
   * Test Regex Replace String
   *
   * Test regexReplace with a string replacement.
   * @return void
   */
  public function testRegexReplaceString( ) : void {
    $this->assertSame('zbc', Str::regexReplace('abc', '/^[a-z]/', 'z'));
  }


  /**
   * Test Regex Replace Callback
   *
   * Test regexReplace with a callback replacement.
   * @return void
   */
  public function testRegexReplaceCallback( ) : void {
    $this->assertSame('(a)bc', Str::regexReplace('abc', '/^[a-z]/', function($matches) { return "({$matches[0]})"; }));
  }


  /**
   * Test Regex Replace Callback Invalid Args
   *
   * Test regexReplace with a callback replacement.
   * @return void
   */
  public function testRegexReplaceCallbackInvalidArgs( ) : void {
    $this->expectException(ArgumentCountError::class);
    Str::regexReplace('abc', '/[a-z]/', function($matches, $invalid) { return ""; });

    Str::regexReplace('abc', '/[a-z]/', function( ) { return ""; });
  }

  /**
   * Test Regex Replace Invalid Regex
   *
   * Test regexRaplce with an invalid regex.
   * @return void
   */
  public function testRegexReplaceInvalidRegex( ) : void {
    $this->expectException(Error::class);
    Str::regexReplace('abc', 'ab', 'zz');
  }


  /**
   * Test Regex Replace No Matches
   *
   * Test regex replace with no matches.
   * @return void
   */
  public function testRegexReplaceNoMatches( ) : void {
    $this->assertSame('abc', Str::regexReplace('abc', '/[0-9]/', '**'));
  }


  /**
   * Test Regex Replace Empty
   *
   * Test regex replace with empty strings.
   * @return void
   */
  public function testRegexReplaceEmpty( ) : void {
    $this->assertSame('abc', Str::regexReplace('abc', '', 'hello'));
    $this->assertSame('', Str::regexReplace('', '/[a-z]/', 'hello'));
    $this->assertSame('', Str::regexReplace('abc', '/[a-z]/', ''));
  }
  

  /**
   * Test Regex Split Preserve Original
   *
   * Test regexSplit preserves the originals.
   * @return void
   */
  public function testRegexSplitPreserveOriginal( ) : void {
    $subject = 'a.b.c';
    $regex   = '/\./';
    Str::regexSplit($subject, $regex);
    $this->assertSame('a.b.c', $subject);
    $this->assertSame('/\./', $regex);
  }


  /**
   * Test Regex Split Removes Match
   *
   * Test regexSplit removes the match.
   * @return void
   */
  public function testRegexSplitRemovesMatch( ) : void {
    $subject = 'a.b.c';
    $regex   = '/\./';
    $this->assertSame(['a', 'b', 'c'], Str::regexSplit($subject, $regex));

    $regex   = '/(?=\.)/';
    $this->assertSame(['a', '.b', '.c'], Str::regexSplit($subject, $regex));
  }


  /**
   * Test Regex Split Invalid Regex
   *
   * Test regexSplit with an invalid regex.
   * @return void
   */
  public function testRegexSplitInvalidRegex( ) : void {
    $this->expectException(Error::class);
    Str::regexSplit('a.b.c', '.');
  }


  /**
   * Test Regex Split Empty
   *
   * Test regex split with empty strings.
   * @return void
   */
  public function testRegexSplitEmpty( ) : void {
    $this->assertSame(['a.b.c'], Str::regexSplit('a.b.c', ''));
    $this->assertSame([''], Str::regexSplit('', '/\./'));
    $this->assertSame([''], Str::regexSplit('', ''));
  }


  /**
   * Test Remove Preserve Original
   *
   * Test remove preserves the original.
   * @return void
   */
  public function testRemovePreserveOriginal( ) : void {
    $subject = 'abcd';
    $remove  = 'b';
    Str::remove($subject, $remove);
    $this->assertSame('abcd', $subject);
    $this->assertSame('b', $remove);
  }


  /**
   * Test Remove Single Match
   *
   * Test remove on a single match.
   * @return void
   */
  public function testRemoveSingleMatch( ) : void {
    $this->assertSame('acd', Str::remove('abcd', 'b'));
    $this->assertSame('ad', Str::remove('abcd', 'bc'));
  }


  /**
   * Test Remove Many Match
   *
   * Test remove with many matches.
   * @return void
   */
  public function testRemoveManyMatch( ) : void {
    $this->assertSame('baba', Str::remove('ababa', 'a'));
    $this->assertSame('aba', Str::remove('ababa', 'ab'));
  }


  /**
   * Test Remove No Match
   *
   * Test remove when there are no matches.
   * @return void
   */
  public function testRemoveNoMatch( ) : void {
    $this->assertSame('abcd', Str::remove('abcd', 'z'));
    $this->assertSame('abcd', Str::remove('abcd', 'zyx'));
  }


  /**
   * Test Remove Empty
   *
   * Test remove with empty strings.
   * @return void
   */
  public function testRemoveEmpty( ) : void {
    $this->assertSame('abcd', Str::remove('abcd', ''));
    $this->assertSame('', Str::remove('', 'a'));
    $this->assertSame('', Str::remove('', 'a'));
  }


  /**
   * Test Replace Preserve Original
   *
   * Test replace preserves the original.
   * @return void
   */
  public function testReplacePreserveOriginal( ) : void {
    $subject = 'abcd';
    $search  = 'b';
    $replace = 'z';
    Str::replace($subject, $search, $replace);
    $this->assertSame('abcd', $subject);
    $this->assertSame('b', $search);
    $this->assertSame('z', $replace);
  }


  /**
   * Test Replace Single
   *
   * Test replace with a single match.
   * @return void
   */
  public function testReplaceSingle( ) : void {
    $this->assertSame('azcd', Str::replace('abcd', 'b', 'z'));
    $this->assertSame('azd', Str::replace('abcd', 'bc', 'z'));
  }


  /**
   * Test Replace Many
   *
   * Test replace many matches.
   * @return void
   */
  public function testReplaceMany( ) : void {
    $this->assertSame('zabab', Str::replace('babab', 'b', 'z'));
    $this->assertSame('zbab', Str::replace('babab', 'ba', 'z'));
  }


  /**
   * Test Replace No Match
   *
   * Test replace with no matches.
   * @return void
   */
  public function testReplaceNoMatch( ) : void {
    $this->assertSame('abcd', Str::replace('abcd', 'z', 'q'));
  }


  /**
   * Test Replace Empty
   *
   * Test replace with empty strings.
   * @return void
   */
  public function testReplaceEmpty( ) : void {
    $this->assertSame('abcd', Str::replace('abcd', '', 'z'));
    $this->assertSame('', Str::replace('', 'a', 'z'));
    $this->assertSame('', Str::replace('', '', 'z'));
  }


  /**
   * Test Reverse Preserve Original
   *
   * Test reverse preserves the original.
   * @return void
   */
  public function testReversePreserveOriginal( ) : void {
    $str = 'Sample';
    Str::reverse($str);
    $this->assertSame('Sample', $str);
  }


  /**
   * Test Reverse Correct
   *
   * Test reverse functions correctly.
   * @return void
   */
  public function testReverseCorrect( ) : void {
    $this->assertSame('elpmaS', Str::reverse('Sample'));
  }


  /**
   * Test Reverse Empty
   *
   * Test reverse on an empty string.
   * @return void
   */
  public function testReverseEmpty( ) : void {
    $this->assertSame('', Str::reverse(''));
  }


  /**
   * Test Section Preserve Original
   *
   * Test section preserves the orignial.
   * @return void
   */
  public function testSectionPreserveOriginal( ) : void {
    $str = 'Sample';
    Str::section($str, 2);
    $this->assertSame('Sample', $str);
  }


  /**
   * Test Section Positive Size
   *
   * Tests section functions with positive sizes.
   * @return void
   */
  public function testSectionPositiveSize( ) : void {
    $this->assertSame(['ab', 'cd', 'ef'], Str::section('abcdef', 3));
    $this->assertSame(['abc', 'de'], Str::section('abcde', 2));
    $this->assertSame(['a', 'b', 'c', ''], Str::section('abc', 4));
  }


  /**
   * Test Section Zero Size
   *
   * Test section with a zero size.
   * @return void
   */
  public function testSectionZeroSize( ) : void {
    $this->assertSame([ ], Str::section('abcdef', 0));
  }


  /**
   * Test Section Negative Size
   *
   * Test section with a neative size.
   * @return void
   */
  public function testSectionNegativeSize( ) : void {
    $this->assertSame(['ab', 'cd', 'ef'], Str::section('abcdef', -3));
    $this->assertSame(['ab', 'cde'], Str::section('abcde', -2));
    $this->assertSame(['', 'a', 'b', 'c'], Str::section('abc', -4));
  }


  /**
   * Test Section Empty String
   *
   * Test section with an empty string.
   * @return void
   */
  public function testSectionEmptyString( ) : void {
    $this->assertSame([''], Str::section('', 1));
    $this->assertSame(['', '', ''], Str::section('', 3));
    $this->assertSame([ ], Str::section('', 0));
    $this->assertSame([''], Str::section('', -1));
  }


  /**
   * Test Set Preserve Original
   *
   * Test set preserves the original.
   * @return void
   */
  public function testSetPreserveOriginal( ) : void {
    $test = 'Test';
    Str::set($test, '9', 0);
    $this->assertSame('Test', $test);
  }


  /**
   * Test Set Positive
   *
   * Test set with a positive offset.
   * @return void
   */
  public function testSetPositive( ) : void {
    $this->assertSame('t9st', Str::set('test', '9', 1));
  }


  /**
   * Test Set Negative
   *
   * Test deleting with a negative offset.
   * @return void
   */
  public function testSetNegative( ) : void {
    $this->assertSame('tes9', Str::set('test', '9', -1));
  }


  /**
   * Test Set Zero
   *
   * Test set the zeroth offset.
   * @return void
   */
  public function testSetZero( ) : void {
    $this->assertSame('9est', Str::set('test', '9', 0));
  }


  /**
   * Test Set Out Of Bounds
   *
   * Test set out of bounds.
   * @return void
   */
  public function testSetOutOfBounds( ) : void {
    $this->assertSame('test', Str::set('test', '9', 100));
    $this->assertSame('test', Str::set('test', '9', -100));
  }


  /**
   * Test Set Empty
   *
   * Test set on an empty string.
   * @return void
   */
  public function testSetEmpty( ) : void {
    $this->assertSame('', Str::set('', '9', 100));
    $this->assertSame('', Str::set('', '9', -100));
    $this->assertSame('', Str::set('', '9', 0));
  }


  /**
   * Tests Snake Preserves Original
   *
   * Tests snake preserves the original.
   * @return void
   */
  public function testSnakePreserveOriginal( ) : void {
    $test = 'Sample String';
    Str::snake($test);
    $this->assertSame('Sample String', $test);
  }
  

  /**
   * Test Snake Delimiters
   *
   * Tests that snake splits on the correct delimiters ( _,.;:).
   * @return void
   */
  public function testSnakeDelimiters( ) : void {
    $this->assertSame('sample_text', Str::snake('sample text'));
    $this->assertSame('sample_text', Str::snake('sample_text'));
    $this->assertSame('sample_text', Str::snake('sample,text'));
    $this->assertSame('sample_text', Str::snake('sample.text'));
    $this->assertSame('sample_text', Str::snake('sample;text'));
    $this->assertSame('sample_text', Str::snake('sample:text'));
  }


  /**
   * Test Snake Capitals
   *
   * Tests snake correctly handles capital letter word splits.
   * @return void
   */
  public function testSnakeCapitals( ) : void { 
    $this->assertSame('Sample_Text_CAPS', Str::snake('SampleTextCAPS'));
    $this->assertSame('Sample_Text_CAPS', Str::snake('Sample Text_CAPS'));
  }

  /**
   * Test Snake Other Chars
   *
   * Tests snake preserves other characters.
   * @return void
   */
  public function testSnakeOtherChars( ) : void {
    $this->assertSame('sample-text101', Str::snake('sample-text101'));
    $this->assertSame('Sample101text', Str::snake('Sample101text'));
    $this->assertSame('Sample101TEXT', Str::snake('Sample101TEXT'));
  }


  /**
   * Test Snake Empty
   *
   * Test snake on an empty string.
   * @return void
   */
  public function testSnakeEmpty( ) : void {
    $this->assertSame('', Str::snake(''));
  }


  /**
   * Test Snippet Preserve Original
   *
   * Test snippet preserves the original.
   * @return void
   */
  public function testSnippetPreserveOriginal( ) : void {
    $str = 'Sample text here';
    Str::snippet($str, 5);
    $this->assertSame('Sample text here', $str);
  }


  /**
   * Test Snippet Default
   *
   * Test a default snippet.
   * @return void
   */
  public function testSnippetDefault( ) : void {
    $this->assertSame('Sample', Str::snippet('Sample text here', 6));
  }


  /**
   * Test Snippet Break Words
   *
   * Test snippet around word breaks.
   * @return void
   */
  public function testSnippetBreakWords( ) : void {
    $this->assertSame('Sample', Str::snippet('Sample text here', 7));
    $this->assertSame('Sample', Str::snippet('Sample text here', 8));
    $this->assertSame('Sample', Str::snippet('Sample text here', 7, true));
    $this->assertSame('Sample t', Str::snippet('Sample text here', 8, true));
  }


  /**
   * Test Snippet Longer Than String
   *
   * Test snippet for longer than the provided string.
   * @return void
   */
  public function testSnippetLongerThanString( ) : void {
    $this->assertSame('Sample text here', Str::snippet('Sample text here', 25));
  }


  /**
   * Test Snippet Zero Length
   *
   * Test a snippet of a zero length.
   * @return void
   */
  public function testSnippetZeroLength( ) : void {
    $this->assertSame('', Str::snippet('Sample text here', 0));
  }

  /**
   * Test Snippet Negative Length
   *
   * Test snippet with a negative length
   * @return void
   */
  public function testSnippetNegativeLength( ) : void {
    $this->assertSame('here', Str::snippet('Sample text here', -4));
    $this->assertSame('here', Str::snippet('Sample text here', -5));
    $this->assertSame('here', Str::snippet('Sample text here', -6));
    $this->assertSame('here', Str::snippet('Sample text here', -5, true));
    $this->assertSame('t here', Str::snippet('Sample text here', -6, true));
  }


  /**
   * Test Snippet Under Word
   *
   * Test snippet under the first word length.
   * @return void
   */
  public function testSnippetUnderWord( ) : void {
    $this->assertSame('', Str::snippet('Sample text here', 1));
    $this->assertSame('', Str::snippet('Sample text here', -1));
  }


  /**
   * Test Snippet Empty String
   *
   * Test snippet on an empty string.
   * @return void
   */
  public function testSnippetEmptyString( ) : void {
    $this->assertSame('', Str::snippet('', 6));
    $this->assertSame('', Str::snippet('', -6));
  }

  /**
   * Test Split Preserve Original
   *
   * Test split preserves the original.
   * @return void
   */
  public function testSplitPreserveOriginal( ) : void {
    $split     = 'a b c d';
    $delimiter = ' ';
    Str::split($split, $delimiter);
    $this->assertSame('a b c d', $split);
    $this->assertSame(' ', $delimiter);
  }


  /**
   * Test Split Correctly
   *
   * Tests split functions as intended.
   * @return void
   */
  public function testSplitCorrectly( ) : void {
    $this->assertSame(['a', 'b', 'c'], Str::split('a b c', ' '));
    $this->assertSame(['a', 'b', 'c'], Str::split('az bz c', 'z '));
  }


  /**
   * Test Split Not Found
   *
   * Test split when delimiter not found.
   * @return void
   */
  public function testSplitNotFound( ) : void {
    $this->assertSame(['abc'], Str::split('abc', ' '));
  }


  /**
   * Test Split Empty
   *
   * Test split with empty strings provided.
   * @return void
   */
  public function testSplitEmpty( ) : void {
    $this->assertSame(['a', 'b', 'c'], Str::split('abc', ''));
    $this->assertSame([''], Str::split('', ' '));
    $this->assertSame([''], Str::split('', ''));
  }


  /**
   * Test Starts With Preserve Original
   *
   * Test startsWith preserves the original.
   * @return void
   */
  public function testStartsWithPreserveOriginal( ) : void {
    $str   = 'Sample';
    $start = 'Sam';
    Str::startsWith($str, $start);
    $this->assertSame('Sample', $str);
    $this->assertSame('Sam', $start);
  }


  /**
   * Test Starts With Success
   *
   * Test startsWith recognizes success case.
   * @return void
   */
  public function testStartsWithSuccess( ) : void {
    $this->assertTrue(Str::startsWith('Sample', 'S'));
    $this->assertTrue(Str::startsWith('Sample', 'Sam'));
  }


  /**
   * Test Starts With Fail
   *
   * Test startsWith recognizes fail case.
   * @return void
   */
  public function testStartsWithFail( ) : void {
    $this->assertFalse(Str::startsWith('Sample', 'zzz'));
    $this->assertFalse(Str::startsWith('Sample', 'Saz'));
    $this->assertFalse(Str::startsWith('Sample', 'sa'));
  }


  /**
   * Test Starts With Longer Search
   *
   * Test startsWith using a longer search than string.
   * @return void
   */
  public function testStartsWithLongerSearch( ) : void {
    $this->assertFalse(Str::startsWith('Sam', 'Sample'));
  }


  /**
   * Test Starts With Empty
   * @return void
   */
  public function testStartsWithEmpty( ) : void {
    $this->assertFalse(Str::startsWith('Sample', ''));
    $this->assertFalse(Str::startsWith('', 'Sample'));
    $this->assertFalse(Str::startsWith('', ''));
  }


  /**
   * Tests Studly Preserves Original
   *
   * Tests studly preserves the original.
   * @return void
   */
  public function testStudlyPreserveOriginal( ) : void {
    $test = 'Sample String';
    Str::studly($test);
    $this->assertSame('Sample String', $test);
  }
  

  /**
   * Test Studly Delimiters
   *
   * Tests that studly splits on the correct delimiters ( _,.;:).
   * @return void
   */
  public function testStudlyDelimiters( ) : void {
    $this->assertSame('SampleText', Str::studly('sample text'));
    $this->assertSame('SampleText', Str::studly('sample_text'));
    $this->assertSame('SampleText', Str::studly('sample,text'));
    $this->assertSame('SampleText', Str::studly('sample.text'));
    $this->assertSame('SampleText', Str::studly('sample;text'));
    $this->assertSame('SampleText', Str::studly('sample:text'));
  }


  /**
   * Test Studly Capitals
   *
   * Tests studly correctly handles capital letter word splits.
   * @return void
   */
  public function testStudlyCapitals( ) : void { 
    $this->assertSame('SampleTextCaps', Str::studly('SampleTextCAPS'));
    $this->assertSame('SampleTextCaps', Str::studly('Sample Text_CAPS'));
  }

  /**
   * Test Studly Other Chars
   *
   * Tests studly preserves other characters.
   * @return void
   */
  public function testStudlyOtherChars( ) : void {
    $this->assertSame('Sample-text101', Str::studly('sample-text101'));
    $this->assertSame('Sample101text', Str::studly('Sample101text'));
    $this->assertSame('Sample101text', Str::studly('Sample101TEXT'));
  }


  /**
   * Test Studly Empty
   *
   * Test studly on an empty string.
   * @return void
   */
  public function testStudlyEmpty( ) : void {
    $this->assertSame('', Str::studly(''));
  }
  

  /**
   * Test Substr Preserve Original
   *
   * Test substr preserves the original.
   * @return void
   */
  public function testSubstrPreserveOriginal( ) : void {
    $str = 'Sample';
    Str::substr($str, 1, 2);
    $this->assertSame('Sample', $str);
  }


  /**
   * Test Substr Correct
   *
   * Test substr produces desired outcome.
   * @return void
   */
  public function testSubstrCorrect( ) : void {
    $this->assertSame('amp', Str::substr('Sample', 1, 3));
    $this->assertSame('ample', Str::substr('Sample', 1));
  }


  /**
   * Test Substr Negative Start
   *
   * Test substr with a negative start.
   * @return void
   */
  public function testSubstrNegativeStart( ) : void {
    $this->assertSame('mp', Str::substr('Sample', -4, 2));
    $this->assertSame('mple', Str::substr('Sample', -4));
  }

  /**
   * Test Substr Negative Length
   *
   * Test substr with a negative length.
   * @return void
   */
  public function testSubstrNegativeLength( ) : void {
    $this->assertSame('amp', Str::substr('Sample', 1, -2));
  }

  /**
   * Test Substr Both Negative
   *
   * Test substr with both length and start negative.
   * @return void
   */
  public function testSubstrBothNegative( ) : void {
    $this->assertSame('mp', Str::substr('Sample', -4, -2));
  }


  /**
   * Test Substr Zeros
   *
   * Test substr with zero length and start.
   * @return void
   */
  public function testSubstrZeros( ) : void {
    $this->assertSame('Sample', Str::substr('Sample', 0));
    $this->assertSame('Sam', Str::substr('Sample', 0, 3));
    $this->assertSame('', Str::substr('Sample', 1, 0));
    $this->assertSame('', Str::substr('Sample', 0, 0));
  }


  /**
   * Test Substr Overflow
   *
   * Test substr with an overflow on start and length.
   * @return void
   */
  public function testSubstrOverflow( ) : void {
    $this->assertSame('', Str::substr('Sample', 10));
    $this->assertSame('', Str::substr('Sample', 10, 3));
    $this->assertSame('ple', Str::substr('Sample', 3, 10));
    $this->assertSame('', Str::substr('Sample', -10));
  }


  /**
   * Test Substr Empty
   *
   * Test substr with an empty string.
   * @return void
   */
  public function testSubstrEmpty( ) : void {
    $this->assertSame('', Str::substr('', 1));
    $this->assertSame('', Str::substr('', 1, 3));
    $this->assertSame('', Str::substr('', -1, 3));
    $this->assertSame('', Str::substr('', 1, -3));
    $this->assertSame('', Str::substr('', -1, -3));
    $this->assertSame('', Str::substr('', 0, 1));
    $this->assertSame('', Str::substr('', 1, 0));
    $this->assertSame('', Str::substr('', 0, 0));
  }


  /**
   * Tests Title Preserves Original
   *
   * Tests title preserves the original.
   * @return void
   */
  public function testTitlePreserveOriginal( ) : void {
    $test = 'Sample String';
    Str::title($test);
    $this->assertSame('Sample String', $test);
  }
  

  /**
   * Test Title Delimiters
   *
   * Tests that title splits on the correct delimiters ( _,.;:).
   * @return void
   */
  public function testTitleDelimiters( ) : void {
    $this->assertSame('Sample Text', Str::title('sample text'));
    $this->assertSame('Sample Text', Str::title('sample_text'));
    $this->assertSame('Sample Text', Str::title('sample,text'));
    $this->assertSame('Sample Text', Str::title('sample.text'));
    $this->assertSame('Sample Text', Str::title('sample;text'));
    $this->assertSame('Sample Text', Str::title('sample:text'));
  }


  /**
   * Test Title Capitals
   *
   * Tests title correctly handles capital letter word splits.
   * @return void
   */
  public function testTitleCapitals( ) : void { 
    $this->assertSame('Sample Text Caps', Str::title('SampleTextCAPS'));
    $this->assertSame('Sample Text Caps', Str::title('Sample Text_CAPS'));
  }

  /**
   * Test Title Other Chars
   *
   * Tests title preserves other characters.
   * @return void
   */
  public function testTitleOtherChars( ) : void {
    $this->assertSame('Sample-text101', Str::title('sample-text101'));
    $this->assertSame('Sample101text', Str::title('Sample101text'));
    $this->assertSame('Sample101text', Str::title('Sample101TEXT'));
  }


  /**
   * Test Title Empty
   *
   * Test title on an empty string.
   * @return void
   */
  public function testTitleEmpty( ) : void {
    $this->assertSame('', Str::title(''));
  }


  /**
   * Test Until Preserve Original
   *
   * Test until preserves the original.
   * @return void
   */
  public function testUntilPreserveOriginal( ) : void {
    $str       = 'Sample';
    $delimiter = 'm';
    Str::until($str, $delimiter);
    $this->assertSame('Sample', $str);
    $this->assertSame('m', $delimiter);
  }


  /**
   * Test Until Single
   *
   * Test until with a single match.
   * @return void
   */
  public function testUntilSingle( ) : void {
    $this->assertSame('ab', Str::until('abcd', 'c'));
    $this->assertSame('ab', Str::until('abcd', 'cd'));
  }

  /**
   * Test Until Many
   *
   * Test until with many matches.
   * @return void
   */
  public function testUntilMany( ) : void {
    $this->assertSame('ab', Str::until('abcdc', 'c'));
    $this->assertSame('ab', Str::until('abcdcdc', 'cd'));
  }


  /**
   * Test Until No Match
   *
   * Test until with no matches.
   * @return void
   */
  public function testUntilNoMatch( ) : void {
    $this->assertSame('abcd', Str::until('abcd', 'z'));
    $this->assertSame('abcd', Str::until('abcd', 'zz'));
  }


  /**
   * Test Until Exact Match
   *
   * Test until with an exact match.
   * @return void
   */
  public function testUntilExactMatch( ) : void {
    $this->assertSame('', Str::until('a', 'a'));
    $this->assertSame('', Str::until('abc', 'abc'));
  }


  /**
   * Test Until Longer Delimiter
   *
   * Test until with a longer delimiter.
   * @return void
   */
  public function testUntilLongerDelimiter( ) : void {
    $this->assertSame('abcd', Str::until('abcd', 'abcde'));
  }


  /**
   * Test Until Empty
   *
   * Test until with empty strings.
   * @return void
   */
  public function testUntilEmpty( ) : void {
    $this->assertSame('', Str::until('', 'a'));
    $this->assertSame('a', Str::until('a', ''));
    $this->assertSame('', Str::until('', ''));
  }


  /**
   * Test Upper Preserve Original
   *
   * Test upper preserves the original.
   * @return void
   */
  public function testUpperPreserveOriginal( ) : void {
    $test = 'Sample';
    Str::upper($test);
    $this->assertSame('Sample', $test);
  }


  /**
   * Test Upper Correct
   *
   * Tests upper functions correctly.
   * @return void
   */
  public function testUpperCorrect( ) : void {
    $this->assertSame('UPPER', Str::upper('UpPeR'));
  }


  /**
   * Test Upper Special Chars
   *
   * Test upper with special characters.
   * @return void
   */
  public function testUpperSpecialChars( ) : void {
    $this->assertSame(' -_1;:<>.,?/\\', Str::upper(' -_1;:<>.,?/\\'));
  }


  /**
   * Tests Words Preserves Original
   *
   * Tests words preserves the original.
   * @return void
   */
  public function testWordsPreserveOriginal( ) : void {
    $test = 'Sample String';
    Str::words($test);
    $this->assertSame('Sample String', $test);
  }
  

  /**
   * Test Words Delimiters
   *
   * Tests that words splits on the correct delimiters ( _,.;:).
   * @return void
   */
  public function testWordsDelimiters( ) : void {
    $this->assertSame(['sample', 'text'], Str::words('sample text'));
    $this->assertSame(['sample', 'text'], Str::words('sample_text'));
    $this->assertSame(['sample', 'text'], Str::words('sample,text'));
    $this->assertSame(['sample', 'text'], Str::words('sample.text'));
    $this->assertSame(['sample', 'text'], Str::words('sample;text'));
    $this->assertSame(['sample', 'text'], Str::words('sample:text'));
  }


  /**
   * Test Words Capitals
   *
   * Tests words correctly handles capital letter word splits.
   * @return void
   */
  public function testWordsCapitals( ) : void { 
    $this->assertSame(['Sample', 'Text', 'CAPS'], Str::words('SampleTextCAPS'));
    $this->assertSame(['Sample', 'Text', 'CAPS'], Str::words('Sample Text_CAPS'));
  }

  /**
   * Test Words Other Chars
   *
   * Tests words preserves other characters.
   * @return void
   */
  public function testWordsOtherChars( ) : void {
    $this->assertSame(['sample-text101'], Str::words('sample-text101'));
    $this->assertSame(['Sample101text'], Str::words('Sample101text'));
    $this->assertSame(['Sample101TEXT'], Str::words('Sample101TEXT'));
  }


  /**
   * Test Words Empty
   *
   * Test words on an empty string.
   * @return void
   */
  public function testWordsEmpty( ) : void {
    $this->assertSame([ ], Str::words(''));
  }
}