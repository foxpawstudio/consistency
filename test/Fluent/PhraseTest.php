<?php

declare(strict_types=1);

use Foxpaw\Consistency\Fluent\Collection;
use Foxpaw\Consistency\Fluent\Phrase;
use PHPUnit\Framework\Error\TypeError;
use PHPUnit\Framework\TestCase;


/**
 * Phrase Test
 *
 * Tests the Phrase class.
 */
final class PhraseTest extends TestCase {

  /**
   * Test Construct Default
   *
   * Test construct with no parameters.
   * @return void
   */
  public function testConstructDefault( ) : void {
    $phrase = new Phrase;
    $this->assertAttributeSame('', 'str', $phrase);
  }

  /**
   * Test Construct Provide
   *
   * Test construct with a provided string.
   * @return void
   */
  public function testConstructProvide( ) : void {
    $phrase = new Phrase('Hello World');
    $this->assertAttributeSame('Hello World', 'str', $phrase);
  }


  /**
   * Test Call Pipes
   *
   * Test __call pipes with the correct arguments.
   * @return void
   */
  public function testCallPipes( ) : void {
    $phrase = $this->getMockBuilder(Phrase::class)
      ->setConstructorArgs(['Hello'])
      ->setMethods(['pipe'])
      ->getMock( );
    $phrase->expects($this->once( ))->method('pipe')->with('reverse', ['Hello']);
    $phrase->reverse( );
  }


  /**
   * Test Call Chains
   *
   * Tests __call chains when appropriate.
   * @return void
   */
  public function testCallChains( ) : void {
    $phrase = new Phrase('Hello');
    $result = $phrase->reverse( );

    $this->assertInstanceOf(Phrase::class, $result);
    $this->assertNotSame($phrase, $result);
  }


  /**
   * Test Call Raw
   *
   * Test __call when returning a raw value.
   * @return void
   */
  public function testCallRaw( ) : void {
    $phrase = new Phrase('Hello');
    $this->assertInternalType('bool', $phrase->endsWith('o'));
  }


  /**
   * Test Call Converts
   *
   * Tests the __call method converts when required.
   * @return void
   */
  public function testCallConverts( ) : void {
    $phrase = new Phrase('Hello');
    $this->assertInstanceOf(Collection::class, $phrase->words( ));
  }


  /**
   * Test All Splits
   *
   * Test the all method splits the phrase.
   * @return void
   */
  public function testAllSplits( ) : void {
    $phrase = new Phrase('Hello');
    $this->assertSame(['H', 'e', 'l', 'l', 'o'], $phrase->all( ));
  }


  /**
   * Test All Empty
   *
   * Test all with an empty phrase.
   * @return void
   */
  public function testAllEmpty( ) : void {
    $phrase = new Phrase;
    $this->assertSame([''], $phrase->all( ));
  }


  /**
   * Test Get Iterator Retrieves
   *
   * Test getIterator retrieves the correct type.
   * @return void
   */
  public function testGetIteratorRetrieves( ) : void {
    $phrase = new Phrase;
    $this->assertInstanceOf(ArrayIterator::class, $phrase->getIterator( ));
  }


  /**
   * Test Get Iterator Loops
   *
   * Test using getIterator allows loops.
   * @return void
   */
  public function testGetIteratorLoops( ) : void {
    $result = ['H', 'e', 'l', 'l', 'o'];
    $phrase = new Phrase('Hello');

    foreach($phrase->getIterator( ) as $index => $value)
      $this->assertSame($result[$index], $value);

    foreach($phrase as $index => $value)
      $this->assertSame($result[$index], $value);
  }


  /**
   * Test Offset Get Method
   *
   * Test offsetGet through the method.
   * @return void
   */
  public function testOffsetGetMethod( ) : void {
    $phrase = new Phrase('Hello');
    $this->assertAttributeSame('e', 'str', $phrase->offsetGet(1));
  }


  /**
   * Test Offset Get Operator
   *
   * Test offsetGet through the operator.
   * @return void
   */
  public function testOffsetGetOperator( ) : void {
    $phrase = new Phrase('Hello');
    $this->assertAttributeSame('e', 'str', $phrase[1]);
  }


  /**
   * Test Offset Get String
   *
   * Test offsetGet using a string offset.
   * @return void
   */
  public function testoffsetGetString( ) : void {
    $phrase = new Phrase('Hello');
    $this->expectException(Error::class);
    $phrase['e'];
  }


  /**
   * Test Offset Get Negative
   *
   * Test offsetGet with a negative index.
   * @return void
   */
  public function testOffsetGetNegative( ) : void {
    $phrase = new Phrase('Hello');
    $this->assertAttributeSame('o', 'str', $phrase[-1]);
  }

  /**
   * Test Offset Get Zero
   *
   * Test offsetGet with a zero index.
   * @return void
   */
  public function testOffsetGetZero( ) : void {
    $phrase = new Phrase('Hello');
    $this->assertAttributeSame('H', 'str', $phrase[0]);
  }


  /**
   * Test Offset Get Out Of Bounds
   *
   * Test offsetGet with an out of bounds position.
   * @return void
   */
  public function testOffsetGetOutOfBounds( ) : void {
    $phrase = new Phrase('Hello');
    $this->assertAttributeSame('', 'str', $phrase[10]);
    $this->assertAttributeSame('', 'str', $phrase[-10]);
  }


  /**
   * Test Offset Get Empty
   *
   * Test offsetGet on an empty phrase.
   * @return void
   */
  public function testOffsetGetEmpty( ) : void {
    $phrase = new Phrase;
    $this->assertAttributeSame('', 'str', $phrase[10]);
    $this->assertAttributeSame('', 'str', $phrase[0]);
    $this->assertAttributeSame('', 'str', $phrase[-10]);
  }


  /**
   * Test Offset Exists Method
   *
   * Test offsetExists through the method.
   * @return void
   */
  public function testOffsetExistsMethod( ) : void {
    $phrase = new Phrase('Hello');
    $this->assertTrue($phrase->offsetExists(1));
  }


  /**
   * Test Offset Exists Operator
   *
   * Test offsetExists through the operator.
   * @return void
   */
  public function testOffsetExistsOperator( ) : void {
    $phrase = new Phrase('Hello');
    $this->assertTrue(isset($phrase[1]));
  }


  /**
   * Test Offset Exists String
   *
   * Test offsetExists using a string offset.
   * @return void
   */
  public function testoffsetExistsString( ) : void {
    $phrase = new Phrase('Hello');
    $this->expectException(Error::class);
    isset($phrase['e']);
  }


  /**
   * Test Offset Exists Negative
   *
   * Test offsetExists with a negative index.
   * @return void
   */
  public function testOffsetExistsNegative( ) : void {
    $phrase = new Phrase('Hello');
    $this->assertTrue(isset($phrase[-1]));
  }

  /**
   * Test Offset Exists Zero
   *
   * Test offsetExists with a zero index.
   * @return void
   */
  public function testOffsetExistsZero( ) : void {
    $phrase = new Phrase('Hello');
    $this->assertTrue(isset($phrase[0]));
  }


  /**
   * Test Offset Exists Out Of Bounds
   *
   * Test offsetExists with an out of bounds position.
   * @return void
   */
  public function testOffsetExistsOutOfBounds( ) : void {
    $phrase = new Phrase('Hello');
    $this->assertFalse(isset($phrase[10]));
    $this->assertFalse(isset($phrase[-10]));
  }


  /**
   * Test Offset Exists Empty
   *
   * Test offsetExists on an empty phrase.
   * @return void
   */
  public function testOffsetExistsEmpty( ) : void {
    $phrase = new Phrase;
    $this->assertFalse(isset($phrase[10]));
    $this->assertFalse(isset($phrase[0]));
    $this->assertFalse(isset($phrase[-10]));
  }


  /**
   * Test Offset Set Method
   *
   * Test offsetSet through the method.
   * @return void
   */
  public function testOffsetSetMethod( ) : void {
    $phrase = new Phrase('Hello');
    $phrase->offsetSet(1, '9');
    $this->assertAttributeSame('H9llo', 'str', $phrase);
  }


  /**
   * Test Offset Set Operator
   *
   * Test offsetSet through the operator.
   * @return void
   */
  public function testOffsetSetOperator( ) : void {
    $phrase = new Phrase('Hello');
    $phrase[1] = '9';
    $this->assertAttributeSame('H9llo', 'str', $phrase);
  }


  /**
   * Test Offset Set String
   *
   * Test offsetSet using a string offset.
   * @return void
   */
  public function testoffsetSetString( ) : void {
    $phrase = new Phrase('Hello');
    $this->expectException(Error::class);
    $phrase['e'] = '9';
  }


  /**
   * Test Offset Set Number
   *
   * Test offsetSet using a number value.
   * @return void
   */
  public function testoffsetSetNumber( ) : void {
    $phrase = new Phrase('Hello');
    $this->expectException(Error::class);
    $phrase[2] = 9;
  }


  /**
   * Test Offset Set Negative
   *
   * Test offsetSet with a negative index.
   * @return void
   */
  public function testOffsetSetNegative( ) : void {
    $phrase = new Phrase('Hello');
    $phrase[-1] = '9';
    $this->assertAttributeSame('Hell9', 'str', $phrase);
  }

  /**
   * Test Offset Set Zero
   *
   * Test offsetSet with a zero index.
   * @return void
   */
  public function testOffsetSetZero( ) : void {
    $phrase = new Phrase('Hello');
    $phrase[0] = '9';
    $this->assertAttributeSame('9ello', 'str', $phrase);
  }


  /**
   * Test Offset Set Out Of Bounds
   *
   * Test offsetSet with an out of bounds position.
   * @return void
   */
  public function testOffsetSetOutOfBounds( ) : void {
    $phrase = new Phrase('Hello');
    $phrase[10] = '9';
    $this->assertAttributeSame('Hello', 'str', $phrase);
    
    $phrase = new Phrase('Hello');
    $phrase[-10] = '9Hello';
    $this->assertAttributeSame('Hello', 'str', $phrase);
  }


  /**
   * Test Offset Set Empty
   *
   * Test offsetSet on an empty phrase.
   * @return void
   */
  public function testOffsetSetEmpty( ) : void {
    $phrase = new Phrase;
    $phrase[10] = '9';
    $this->assertAttributeSame('', 'str', $phrase);

    $phrase = new Phrase;
    $phrase[0] = '9';
    $this->assertAttributeSame('', 'str', $phrase);

    $phrase = new Phrase;
    $phrase[-10] = '9';
    $this->assertAttributeSame('', 'str', $phrase);
  }


  /**
   * Test Offset Unset Method
   *
   * Test offsetUnset through the method.
   * @return void
   */
  public function testOffsetUnsetMethod( ) : void {
    $phrase = new Phrase('Hello');
    $phrase->offsetUnset(1);
    $this->assertAttributeSame('Hllo', 'str', $phrase);
  }


  /**
   * Test Offset Unset Operator
   *
   * Test offsetUnset through the operator.
   * @return void
   */
  public function testOffsetUnsetOperator( ) : void {
    $phrase = new Phrase('Hello');
    unset($phrase[1]);
    $this->assertAttributeSame('Hllo', 'str', $phrase);
  }


  /**
   * Test Offset Unset String
   *
   * Test offsetUnset using a string offset.
   * @return void
   */
  public function testoffsetUnsetString( ) : void {
    $phrase = new Phrase('Hello');
    $this->expectException(Error::class);
    unset($phrase['e']);
  }


  /**
   * Test Offset Unset Negative
   *
   * Test offsetUnset with a negative index.
   * @return void
   */
  public function testOffsetUnsetNegative( ) : void {
    $phrase = new Phrase('Hello');
    unset($phrase[-1]);
    $this->assertAttributeSame('Hell', 'str', $phrase);
  }

  /**
   * Test Offset Unset Zero
   *
   * Test offsetUnset with a zero index.
   * @return void
   */
  public function testOffsetUnsetZero( ) : void {
    $phrase = new Phrase('Hello');
    unset($phrase[0]);
    $this->assertAttributeSame('ello', 'str', $phrase);
  }


  /**
   * Test Offset Unset Out Of Bounds
   *
   * Test offsetUnset with an out of bounds position.
   * @return void
   */
  public function testOffsetUnsetOutOfBounds( ) : void {
    $phrase = new Phrase('Hello');
    unset($phrase[10]);
    $this->assertAttributeSame('Hello', 'str', $phrase);
    
    $phrase = new Phrase('Hello');
    unset($phrase[-10]);
    $this->assertAttributeSame('Hello', 'str', $phrase);
  }


  /**
   * Test Offset Unset Empty
   *
   * Test offsetUnset on an empty phrase.
   * @return void
   */
  public function testOffsetUnsetEmpty( ) : void {
    $phrase = new Phrase;
    unset($phrase[10]);
    $this->assertAttributeSame('', 'str', $phrase);

    $phrase = new Phrase;
    unset($phrase[0]);
    $this->assertAttributeSame('', 'str', $phrase);

    $phrase = new Phrase;
    unset($phrase[-10]);
    $this->assertAttributeSame('', 'str', $phrase);
  }


  /**
   * Test Raw Returns
   *
   * Test raw returns the internal string
   * @return void
   */
  public function testRawReturns( ) : void {
    $phrase = new Phrase('Hello');
    $this->assertSame('Hello', $phrase->raw( ));
    $this->assertInternalType('string', $phrase->raw( ));
  }


  /**
   * Test Raw Zero
   *
   * Test raw returns correctly when the internal string is default.
   * @return void
   */
  public function testRawZero( ) : void {
    $phrase = new Phrase;
    $this->assertSame('', $phrase->raw( ));
    $this->assertInternalType('string', $phrase->raw( ));
  }



  /**
   * Test To Array Splits
   *
   * Test the toArray method splits the phrase.
   * @return void
   */
  public function testToArraySplits( ) : void {
    $phrase = new Phrase('Hello');
    $this->assertSame(['H', 'e', 'l', 'l', 'o'], $phrase->toArray( ));
  }


  /**
   * Test To Array Empty
   *
   * Test toArray with an empty phrase.
   * @return void
   */
  public function testToArrayEmpty( ) : void {
    $phrase = new Phrase;
    $this->assertSame([''], $phrase->toArray( ));
  }


  /**
   * Test To String Method
   *
   * Test toString through the method.
   * @return void
   */
  public function testToStringMethod( ) : void {
    $phrase = new Phrase('Hello');
    $this->assertSame('Hello', $phrase->toString( ));
  }


  /**
   * Test To String Magic
   *
   * Test toString through the magic method
   * @return void
   */
  public function testToStringMagic( ) : void {
    $phrase = new Phrase('Hello');
    $this->assertSame('Hello', (string)$phrase);
  }


  /**
   * Test To String Empty
   *
   * Test toString when the string is empty.
   * @return void
   */
  public function testToStringEmpty( ) : void {
    $phrase = new Phrase;
    $this->assertSame('', $phrase->toString( ));
    $this->assertSame('', (string)$phrase);
  }
}