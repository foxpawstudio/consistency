<?php

declare(strict_types=1);

use Foxpaw\Consistency\Fluent\Number;
use Foxpaw\Consistency\Fluent\Phrase;
use PHPUnit\Framework\Error\TypeError;
use PHPUnit\Framework\TestCase;


/**
 * Number Test
 *
 * Tests the Number class.
 */
final class NumberTest extends TestCase {

  /**
   * Test Construct Default
   *
   * Test construct with no parameters.
   * @return void
   */
  public function testConstructDefault( ) : void {
    $number = new Number;
    $this->assertAttributeSame(0.0, 'num', $number);
  }

  /**
   * Test Construct Provide
   *
   * Test construct with a provided string.
   * @return void
   */
  public function testConstructProvide( ) : void {
    $number = new Number(10);
    $this->assertAttributeSame(10.0, 'num', $number);
  }


  /**
   * Test Call Pipes
   *
   * Test __call pipes with the correct arguments.
   * @return void
   */
  public function testCallPipes( ) : void {
    $number = $this->getMockBuilder(Number::class)
      ->setConstructorArgs([10])
      ->setMethods(['pipe'])
      ->getMock( );
    $number->expects($this->once( ))->method('pipe')->with('abs', [10]);
    $number->abs( );
  }


  /**
   * Test Call Chains
   *
   * Tests __call chains when appropriate.
   * @return void
   */
  public function testCallChains( ) : void {
    $number = new Number(10);
    $result = $number->abs( );

    $this->assertInstanceOf(Number::class, $result);
    $this->assertNotSame($number, $result);
  }


  /**
   * Test Call Raw
   *
   * Test __call when returning a raw value.
   * @return void
   */
  public function testCallRaw( ) : void {
    $number = new Number(10);
    $this->assertInternalType('bool', $number->isEven( ));
  }


  /**
   * Test Call Converts
   *
   * Tests the __call method converts when required.
   * @return void
   */
  public function testCallConverts( ) : void {
    $number = new Number(10);
    $this->assertInstanceOf(Phrase::class, $number->format( ));
  }


  /**
   * Test Raw Returns
   *
   * Test raw returns the internal number
   * @return void
   */
  public function testRawReturns( ) : void {
    $number = new Number(10);
    $this->assertSame(10.0, $number->raw( ));
    $this->assertInternalType('float', $number->raw( ));
  }


  /**
   * Test Raw Zero
   *
   * Test raw returns correctly when the internal number is default.
   * @return void
   */
  public function testRawZero( ) : void {
    $number = new Number;
    $this->assertSame(0.0, $number->raw( ));
    $this->assertInternalType('float', $number->raw( ));
  }


  /**
   * Test To String Method
   *
   * Test toString through the method.
   * @return void
   */
  public function testToStringMethod( ) : void {
    $number = new Number(10);
    $this->assertSame('10', $number->toString( ));
  }


  /**
   * Test To String Magic
   *
   * Test toString through the magic method
   * @return void
   */
  public function testToStringMagic( ) : void {
    $number = new Number(10);
    $this->assertSame('10', (string)$number);
  }


  /**
   * Test To String Zero
   *
   * Test toString when the number is zero.
   * @return void
   */
  public function testToStringZero( ) : void {
    $number = new Number;
    $this->assertSame('0', $number->toString( ));
    $this->assertSame('0', (string)$number);
  }
}