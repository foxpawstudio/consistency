<?php

declare(strict_types=1);

use Foxpaw\Consistency\Fluent\Collection;
use Foxpaw\Consistency\Fluent\Cookie;
use PHPUnit\Framework\Error\Error;
use PHPUnit\Framework\TestCase;


/**
 * Cookie Test
 *
 * Tests the Cookie class.
 */
final class CookieTest extends TestCase {

  /** @var Foxpaw\Consistency\Fluent\Cookie  The cookie to quickly test on. */
  protected $cookie;


  /**
   * Set Up
   *
   * Sets up a cookie for tests.
   * @return void
   */
  public function setUp( ) : void {
    $this->cookie = new Cookie(['name' => 'Test']);

    $reflection = new ReflectionClass(Cookie::class);
    $cache = $reflection->getProperty('cache');
    $cache->setAccessible(true);
    $cache->setValue(new Collection(['Test' => $this->cookie]));
    $cache->setAccessible(false);
    $_COOKIE = ['Test' => $this->cookie->toJson( )];
  }


  /**
   * Tear Down
   *
   * Teardown after the tests.
   * @return void
   */
  public function tearDown( ) : void {
    $_COOKIE = [ ];
    $reflection = new ReflectionClass(Cookie::class);
    $cache = $reflection->getProperty('cache');
    $cache->setAccessible(true);
    $cache->setValue([ ]);
    $cache->setAccessible(false);
  }


  /**
   * Test Empty Constructor
   *
   * Tests the constructor when empty.
   * @return void
   */
  public function testEmptyConstructor( ) : void {
    $cookie = new Cookie;
    $this->assertAttributeSame(
      ['name' => '', 'value' => '', 'expires' => 0, 'path' => '', 'domain' => '', 'secure' => false, 'http' => false],
      'attributes',
      $cookie
    );
  }


  /**
   * Test Constructor
   *
   * Test a constructor with values.
   * @return void
   */
  public function testConstructor( ) : void {
    $cookie = new Cookie([
      'name' => 'name',
      'value' => 'value',
      'expires' => 'expires',
      'path' => 'path',
      'domain' => 'domain',
      'secure' => 'secure',
      'http' => 'http',
      'stripped' => 'stripped'
    ]);

    $this->assertAttributeSame([
        'name' => 'name',
        'value' => 'value',
        'expires' => 'expires',
        'path' => 'path',
        'domain' => 'domain',
        'secure' => 'secure',
        'http' => 'http'
      ],
      'attributes',
      $cookie
    );
  }


  /**
   * Test Delete
   *
   * Tests delete removes from the cookie array and the cache.
   * @return void
   */
  public function testDelete( ) : void {
    $this->assertAttributeEquals(new Collection(['Test' => $this->cookie]), 'cache', Cookie::class);
    $this->assertSame(['Test' => $this->cookie->toJson( )], $_COOKIE);

    $this->assertTrue(@$this->cookie->delete( ));

    $this->assertAttributeEquals(new Collection, 'cache', Cookie::class);
    $this->assertSame([ ], $_COOKIE);
  }


  /**
   * Test Find Success
   *
   * Test a successful find.
   * @return void
   */
  public function testFindSuccess( ) : void {
    $this->assertSame($this->cookie, Cookie::find('Test'));
  }


  /**
   * Test Find Fail
   *
   * Test an unsuccessful find.
   * @return void
   */
  public function testFindFail( ) : void {
    $this->assertNull(Cookie::find('NotThere'));
  }


  /**
   * Test Find Default
   *
   * Test find will use the default.
   * @return void
   */
  public function testFindDefault( ) : void {
    $this->assertSame($this->cookie, Cookie::find('Test'));
    $this->assertSame('default', Cookie::find('NotThere', 'default'));
  }


  /**
   * Test Get Retrieves
   *
   * Test get retrieves the cache.
   * @return void
   */
  public function testGetRetrieves( ) : void {
    $this->assertEquals(new Collection(['Test' => $this->cookie]), Cookie::get( ));

    $reflection = new ReflectionClass(Cookie::class);
    $cache = $reflection->getProperty('cache');
    $cache->setAccessible(true);
    $cache->setValue(null);
    $cache->setAccessible(false);

    $this->assertEquals(new Collection(['Test' => $this->cookie]), Cookie::get( ));
  }


  /**
   * Test Get Force
   *
   * Test forcing a recalculate of get.
   * @return void
   */
  public function testGetForce( ) : void {
    $this->assertEquals(new Collection(['Test' => $this->cookie]), Cookie::get( ));
    $this->assertSame(['Test' => $this->cookie->toJson( )], $_COOKIE);
    $this->assertAttributeEquals(new Collection(['Test' => $this->cookie]), 'cache', Cookie::class);

    $temp = new Cookie(['name' => 'Temp']);
    $_COOKIE['Temp'] = $temp->toJson( );

    $this->assertSame(['Test' => $this->cookie->toJson( ), 'Temp' => $temp->toJson( )], $_COOKIE);
    $this->assertEquals(new Collection(['Test' => $this->cookie]), Cookie::get( ));
    $this->assertAttributeEquals(new Collection(['Test' => $this->cookie]), 'cache', Cookie::class);

    $this->assertEquals(new Collection(['Test' => $this->cookie, 'Temp' => $temp]), Cookie::get(true));
    $this->assertAttributeEquals(new Collection(['Test' => $this->cookie, 'Temp' => $temp]), 'cache', Cookie::class);
  }


  /**
   * Test Get Converts
   *
   * Test get converts basic cookie data.
   * @return void
   */
  public function testGetConverts( ) : void {
    $reflection = new ReflectionClass(Cookie::class);
    $cache = $reflection->getProperty('cache');
    $cache->setAccessible(true);
    $cache->setValue([ ]);
    $cache->setAccessible(false);
    $_COOKIE = ['Something' => 'basic'];

    $expected = new Collection(['Something' => new Cookie(['name' => 'Something', 'value' => 'basic'])]);

    $this->assertEquals($expected, Cookie::get( ));
    $this->assertAttributeEquals($expected, 'cache', Cookie::class);
  }


  /**
   * Test Save Success
   *
   * Test a successful save.
   * @return void
   */
  public function testSaveSuccess( ) : void {
    $_COOKIE = [ ];
    $reflection = new ReflectionClass(Cookie::class);
    $cache = $reflection->getProperty('cache');
    $cache->setAccessible(true);
    $cache->setValue([ ]);
    $cache->setAccessible(false);

    @$this->cookie->save( );
    $this->assertSame(['Test' => $this->cookie->toJson( )], $_COOKIE);
    $this->assertAttributeEquals(new Collection(['Test' => $this->cookie]), 'cache', Cookie::class);
  }


  /**
   * Test Save No Name
   *
   * Tests saving with no name.
   * @return void
   */
  public function testSaveNoName( ) : void {
    $this->expectException(InvalidArgumentException::class);
    $this->cookie->name = '';
    @$this->cookie->save( );
  }


  /**
   * Test Save In Past
   *
   * Test saving with an expiry in past.
   * @return void
   */
  public function testSaveInPast( ) : void {
    $_COOKIE = [ ];
    $reflection = new ReflectionClass(Cookie::class);
    $cache = $reflection->getProperty('cache');
    $cache->setAccessible(true);
    $cache->setValue([ ]);
    $cache->setAccessible(false);

    $this->cookie->expires = time( ) - 3600;

    $this->assertFalse(@$this->cookie->save( ));
    $this->assertSame([ ], $_COOKIE);
    $this->assertAttributeEquals(new Collection, 'cache', Cookie::class);
  }
}