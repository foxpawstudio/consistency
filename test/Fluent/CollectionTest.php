<?php

declare(strict_types=1);

use Foxpaw\Consistency\Fluent\Collection;
use Foxpaw\Consistency\Fluent\Number;
use PHPUnit\Framework\Error\TypeError;
use PHPUnit\Framework\TestCase;


/**
 * Collection Test
 *
 * Tests the Collection class.
 */
final class CollectionTest extends TestCase {

  /**
   * Test Construct Default
   *
   * Test construct with no parameters.
   * @return void
   */
  public function testConstructDefault( ) : void {
    $collection = new Collection;
    $this->assertAttributeSame([ ], 'arr', $collection);
  }

  /**
   * Test Construct Provide
   *
   * Test construct with a provided string.
   * @return void
   */
  public function testConstructProvide( ) : void {
    $collection = new Collection(['Hello', 'World']);
    $this->assertAttributeSame(['Hello', 'World'], 'arr', $collection);
  }


  /**
   * Test Call Pipes
   *
   * Test __call pipes with the correct arguments.
   * @return void
   */
  public function testCallPipes( ) : void {
    $collection = $this->getMockBuilder(Collection::class)
      ->setConstructorArgs([['Hello', 'World']])
      ->setMethods(['pipe'])
      ->getMock( );
    $collection->expects($this->once( ))->method('pipe')->with('filter', [['Hello', 'World']]);
    $collection->filter( );
  }


  /**
   * Test Call Chains
   *
   * Tests __call chains when appropriate.
   * @return void
   */
  public function testCallChains( ) : void {
    $collection = new Collection(['Hello', 'World']);
    $result = $collection->filter( );

    $this->assertInstanceOf(Collection::class, $result);
    $this->assertNotSame($collection, $result);
  }


  /**
   * Test Call Raw
   *
   * Test __call when returning a raw value.
   * @return void
   */
  public function testCallRaw( ) : void {
    $collection = new Collection(['Hello', 'World']);
    $this->assertInternalType('string', $collection->last( ));
  }


  /**
   * Test Call Converts
   *
   * Tests the __call method converts when required.
   * @return void
   */
  public function testCallConverts( ) : void {
    $collection = new Collection(['Hello', 'World']);
    $this->assertInstanceOf(Number::class, $collection->count( ));
  }


  /**
   * Test All Splits
   *
   * Test the all method retrieves the collection.
   * @return void
   */
  public function testAllSplits( ) : void {
    $collection = new Collection(['Hello', 'World']);
    $this->assertSame(['Hello', 'World'], $collection->all( ));
  }


  /**
   * Test All Empty
   *
   * Test all with an empty collection.
   * @return void
   */
  public function testAllEmpty( ) : void {
    $collection = new Collection;
    $this->assertSame([ ], $collection->all( ));
  }


  /**
   * Test Get Iterator Retrieves
   *
   * Test getIterator retrieves the correct type.
   * @return void
   */
  public function testGetIteratorRetrieves( ) : void {
    $collection = new Collection;
    $this->assertInstanceOf(ArrayIterator::class, $collection->getIterator( ));
  }


  /**
   * Test Get Iterator Loops
   *
   * Test using getIterator allows loops.
   * @return void
   */
  public function testGetIteratorLoops( ) : void {
    $result = ['Hello', 'World'];
    $collection = new Collection(['Hello', 'World']);

    foreach($collection->getIterator( ) as $index => $value)
      $this->assertSame($result[$index], $value);

    foreach($collection as $index => $value)
      $this->assertSame($result[$index], $value);
  }


  /**
   * Test Offset Get Method
   *
   * Test offsetGet through the method.
   * @return void
   */
  public function testOffsetGetMethod( ) : void {
    $collection = new Collection(['Hello', 'World']);
    $this->assertSame('World', $collection->offsetGet(1));
  }


  /**
   * Test Offset Get Operator
   *
   * Test offsetGet through the operator.
   * @return void
   */
  public function testOffsetGetOperator( ) : void {
    $collection = new Collection(['Hello', 'World']);
    $this->assertSame('World', $collection[1]);
  }


  /**
   * Test Offset Get String
   *
   * Test offsetGet using a string offset.
   * @return void
   */
  public function testoffsetGetString( ) : void {
    $collection = new Collection(['first' => 'Hello', 'second' => 'World']);
    $this->assertSame('Hello', $collection['first']);
  }

  /**
   * Test Offset Get Zero
   *
   * Test offsetGet with a zero index.
   * @return void
   */
  public function testOffsetGetZero( ) : void {
    $collection = new Collection(['Hello', 'World']);
    $this->assertSame('Hello', $collection[0]);
  }


  /**
   * Test Offset Get Out Of Bounds
   *
   * Test offsetGet with an out of bounds position.
   * @return void
   */
  public function testOffsetGetOutOfBounds( ) : void {
    $collection = new Collection(['Hello', 'World']);
    $this->assertNull($collection[10]);
    $this->assertNull($collection[-10]);
    $this->assertNull($collection['first']);
  }


  /**
   * Test Offset Get Empty
   *
   * Test offsetGet on an empty collection.
   * @return void
   */
  public function testOffsetGetEmpty( ) : void {
    $collection = new Collection;
    $this->assertNull($collection[10]);
    $this->assertNull($collection[0]);
    $this->assertNull($collection[-10]);
  }


  /**
   * Test Offset Exists Method
   *
   * Test offsetExists through the method.
   * @return void
   */
  public function testOffsetExistsMethod( ) : void {
    $collection = new Collection(['Hello', 'World']);
    $this->assertTrue($collection->offsetExists(1));
  }


  /**
   * Test Offset Exists Operator
   *
   * Test offsetExists through the operator.
   * @return void
   */
  public function testOffsetExistsOperator( ) : void {
    $collection = new Collection(['Hello', 'World']);
    $this->assertTrue(isset($collection[1]));
  }


  /**
   * Test Offset Exists String
   *
   * Test offsetExists using a string offset.
   * @return void
   */
  public function testoffsetExistsString( ) : void {
    $collection = new Collection(['first' => 'Hello', 'second' => 'World']);
    $this->assertTrue(isset($collection['first']));
  }


  /**
   * Test Offset Exists Zero
   *
   * Test offsetExists with a zero index.
   * @return void
   */
  public function testOffsetExistsZero( ) : void {
    $collection = new Collection(['Hello', 'World']);
    $this->assertTrue(isset($collection[0]));
  }


  /**
   * Test Offset Exists Out Of Bounds
   *
   * Test offsetExists with an out of bounds position.
   * @return void
   */
  public function testOffsetExistsOutOfBounds( ) : void {
    $collection = new Collection(['Hello', 'World']);
    $this->assertFalse(isset($collection[10]));
    $this->assertFalse(isset($collection[-10]));
    $this->assertFalse(isset($collection['first']));
  }


  /**
   * Test Offset Exists Empty
   *
   * Test offsetExists on an empty collection.
   * @return void
   */
  public function testOffsetExistsEmpty( ) : void {
    $collection = new Collection;
    $this->assertFalse(isset($collection[10]));
    $this->assertFalse(isset($collection[0]));
    $this->assertFalse(isset($collection[-10]));
  }


  /**
   * Test Offset Set Method
   *
   * Test offsetSet through the method.
   * @return void
   */
  public function testOffsetSetMethod( ) : void {
    $collection = new Collection(['Hello']);
    $collection->offsetSet(1, 'World');
    $this->assertAttributeSame(['Hello', 'World'], 'arr', $collection);
  }


  /**
   * Test Offset Set Operator
   *
   * Test offsetSet through the operator.
   * @return void
   */
  public function testOffsetSetOperator( ) : void {
    $collection = new Collection(['Hello']);
    $collection[1] = 'World';
    $this->assertAttributeSame(['Hello', 'World'], 'arr', $collection);

    $collection = new Collection(['Hello']);
    $collection[ ] = 'World';
    $this->assertAttributeSame(['Hello', 'World'], 'arr', $collection);
  }


  /**
   * Test Offset Set String
   *
   * Test offsetSet using a string offset.
   * @return void
   */
  public function testoffsetSetString( ) : void {
    $collection = new Collection(['first' => 'one', 'second' => 'two']);
    $collection['second'] = 'not-two';
    $this->assertAttributeSame(['first' => 'one', 'second' => 'not-two'], 'arr', $collection);
  }


  /**
   * Test Offset Set Number
   *
   * Test offsetSet using a number value.
   * @return void
   */
  public function testoffsetSetNumber( ) : void {
    $collection = new Collection(['Hello', 'World']);
    $collection[1] = 'Not-world';
    $this->assertAttributeSame(['Hello', 'Not-world'], 'arr', $collection);

  }


  /**
   * Test Offset Set Negative
   *
   * Test offsetSet with a negative index.
   * @return void
   */
  public function testOffsetSetNegative( ) : void {
    $collection = new Collection(['Hello']);
    $collection[-1] = 'World';
    $this->assertAttributeSame([ 0 => 'Hello', -1 => 'World'], 'arr', $collection);
  }


  /**
   * Test Offset Set Zero
   *
   * Test offsetSet with a zero index.
   * @return void
   */
  public function testOffsetSetZero( ) : void {
    $collection = new Collection(['Hello', 'World']);
    $collection[0] = 'Not-Hello';
    $this->assertAttributeSame(['Not-Hello', 'World'], 'arr', $collection);
  }


  /**
   * Test Offset Unset Method
   *
   * Test offsetUnset through the method.
   * @return void
   */
  public function testOffsetUnsetMethod( ) : void {
    $collection = new Collection(['Hello', 'World']);
    $collection->offsetUnset(1);
    $this->assertAttributeSame([0 => 'Hello'], 'arr', $collection);
  }


  /**
   * Test Offset Unset Operator
   *
   * Test offsetUnset through the operator.
   * @return void
   */
  public function testOffsetUnsetOperator( ) : void {
    $collection = new Collection(['Hello', 'World']);
    unset($collection[1]);
    $this->assertAttributeSame([0 => 'Hello'], 'arr', $collection);
  }


  /**
   * Test Offset Unset String
   *
   * Test offsetUnset using a string offset.
   * @return void
   */
  public function testoffsetUnsetString( ) : void {
    $collection = new Collection(['first' => 'one', 'second' => 'two']);
    unset($collection['first']);
    $this->assertAttributeSame(['second' => 'two'], 'arr', $collection);
  }


  /**
   * Test Offset Unset Out Of Bounds
   *
   * Test offsetUnset with an out of bounds position.
   * @return void
   */
  public function testOffsetUnsetOutOfBounds( ) : void {
    $collection = new Collection(['Hello', 'World']);
    unset($collection[10]);
    $this->assertAttributeSame(['Hello', 'World'], 'arr', $collection);
    
    $collection = new Collection(['Hello', 'World']);
    unset($collection[-10]);
    $this->assertAttributeSame(['Hello', 'World'], 'arr', $collection);
  }


  /**
   * Test Offset Unset Empty
   *
   * Test offsetUnset on an empty collection.
   * @return void
   */
  public function testOffsetUnsetEmpty( ) : void {
    $collection = new Collection;
    unset($collection[10]);
    $this->assertAttributeSame([ ], 'arr', $collection);

    $collection = new Collection;
    unset($collection[0]);
    $this->assertAttributeSame([ ], 'arr', $collection);

    $collection = new Collection;
    unset($collection[-10]);
    $this->assertAttributeSame([ ], 'arr', $collection);
  }


  /**
   * Test Raw Returns
   *
   * Test raw returns the internal string
   * @return void
   */
  public function testRawReturns( ) : void {
    $collection = new Collection(['Hello', 'World']);
    $this->assertSame(['Hello', 'World'], $collection->raw( ));
    $this->assertInternalType('array', $collection->raw( ));
  }


  /**
   * Test Raw Default
   *
   * Test raw returns correctly when the internal string is default.
   * @return void
   */
  public function testRawDefault( ) : void {
    $collection = new Collection;
    $this->assertSame([ ], $collection->raw( ));
    $this->assertInternalType('array', $collection->raw( ));
  }


  /**
   * Test To Array Correct
   *
   * Test the toArray method returns the collection.
   * @return void
   */
  public function testToArrayCorrect( ) : void {
    $collection = new Collection(['Hello', 'World']);
    $this->assertSame(['Hello', 'World'], $collection->toArray( ));
  }


  /**
   * Test To Array Empty
   *
   * Test toArray with an empty collection.
   * @return void
   */
  public function testToArrayEmpty( ) : void {
    $collection = new Collection;
    $this->assertSame([ ], $collection->toArray( ));
  }


  /**
   * Test To Json Method
   *
   * Test toJson through the method.
   * @return void
   */
  public function testToJsonMethod( ) : void {
    $collection = new Collection(['Hello', 'World']);
    $this->assertJsonStringEqualsJsonString('["Hello", "World"]', $collection->toJson( ));
  }


  /**
   * Test To Json Empty
   *
   * Test toJson when the string is empty.
   * @return void
   */
  public function testToJsonEmpty( ) : void {
    $collection = new Collection;
    $this->assertJsonStringEqualsJsonString('[ ]', $collection->toJson( ));
  }


  /**
   * Test To String Method
   *
   * Test toString through the method.
   * @return void
   */
  public function testToStringMethod( ) : void {
    $collection = new Collection(['Hello', 'World']);
    $this->assertJsonStringEqualsJsonString('["Hello", "World"]', $collection->toString( ));
  }


  /**
   * Test To String Magic
   *
   * Test toString through the magic method
   * @return void
   */
  public function testToStringMagic( ) : void {
    $collection = new Collection(['Hello', 'World']);
    $this->assertJsonStringEqualsJsonString('["Hello", "World"]', (string)$collection);
  }


  /**
   * Test To String Empty
   *
   * Test toString when the string is empty.
   * @return void
   */
  public function testToStringEmpty( ) : void {
    $collection = new Collection;
    $this->assertJsonStringEqualsJsonString('[ ]', $collection->toString( ));
    $this->assertJsonStringEqualsJsonString('[ ]', (string)$collection);
  }
}