<?php

declare(strict_types=1);

use Foxpaw\Consistency\Components\Pipes;
use PHPUnit\Framework\TestCase;


/**
 * Pipes Test
 *
 * Tests the pipes component.
 */
final class PipesTest extends TestCase {

  /** @var stdClass  A destination for pipe testing. */
  protected $destination;


  /**
   * Set Up
   *
   * Set up for the test case.
   * @return void
   */
  public function setUp( ) : void {
    $this->destination = $this->getMockBuilder(stdClass::class)->setMethods(['run'])->getMock( );
    $this->destination->expects($this->any( ))->method('run')->willReturn(11);
  }


  /**
   * Test Pipe Defined
   *
   * Test pipe with a defined destination.
   * @return void
   */
  public function testPipeDefined( ) : void {
    $mock = $this->getMockBuilder(Pipes::class)->getMockForTrait( );
    $mock->destination = $this->destination;
    $this->destination->expects($this->once( ))->method('run');
    $result = $mock->pipe('run', [ ]);
  }


  /**
   * Test Pipe Provided
   *
   * Test pipe with a provided destination.
   * @return void
   */
  public function testPipeProvided( ) : void {
    $mock = $this->getMockBuilder(Pipes::class)->getMockForTrait( );
    $this->destination->expects($this->once( ))->method('run');
    $result = $mock->pipe('run', [ ], $this->destination);
  }

  /**
   * Test Pipe Chained
   *
   * Tests that pipe chains.
   * @return void
   */
  public function testPipeChained( ) : void {
    $mock = $this->getMockBuilder(Pipes::class)->getMockForTrait( );
    $mock->destination = $this->destination;

    $result = $mock->pipe('run', [ ]);
    $this->assertInstanceOf(get_class($mock), $result);
    $this->assertNotSame($mock, $result);

    $result = $mock->pipe('run', [ ], $this->destination);
    $this->assertInstanceOf(get_class($mock), $result);
    $this->assertNotSame($mock, $result);
  }


  /**
   * Test Pipe Converted
   *
   * Test pipe will convert to the specified type.
   * @return vois
   */
  public function testPipeConverted( ) : void {
    $mock = $this->getMockBuilder(Pipes::class)->getMockForTrait( );
    $mock->destination = $this->destination;
    $mock->converts = ['run' => 'string'];
    $this->assertSame('11', $mock->pipe('run', [ ]));
  }


  /**
   * Test Pipe Raw
   *
   * Test pipe will return the raw value when converted to null.
   * @return void
   */
  public function testPipeRaw( ) : void {
    $mock = $this->getMockBuilder(Pipes::class)->getMockForTrait( );
    $mock->destination = $this->destination;
    $mock->converts = ['run' => null];
    $this->assertSame(11, $mock->pipe('run', [ ]));
  }


  /**
   * Test Pipe Magic
   *
   * Test pipe works through the provided __call magic method.
   * @return void
   */
  public function testPipeMagic( ) : void {
    $mock = $this->getMockBuilder(Pipes::class)->setMethods(['pipe'])->getMockForTrait( );
    $mock->destination = $this->destination;
    
    $mock->expects($this->once( ))->method('pipe')->with('run', ['some', 'args']);
    $mock->run('some', 'args');
  }
}