<?php

declare(strict_types=1);

use Foxpaw\Consistency\Components\Attributes;
use PHPUnit\Framework\TestCase;


/**
 * Attributes Test
 *
 * Tests the attributes component.
 */
final class AttributesTest extends TestCase {

  /** @var Foxpaw\Consistency\Components\Attributes  The mock for the attributes trait. */
  protected $mock;


  /**
   * Set Up
   *
   * Sets up the mock for the trait to test.
   * @return void
   */
  public function setUp( ) : void {
    $this->mock = $this->getMockBuilder(Attributes::class)
      ->setMethods(['getAppends', 'getFillable', 'getHides'])
      ->getMockForTrait( );
    $reflection = new ReflectionClass($this->mock);

    $attributes = $reflection->getProperty('attributes');
    $attributes->setAccessible(true);
    $attributes->setValue($this->mock, ['first' => 'one', 'second' => 'two']);
    $attributes->setAccessible(false);
  }


  /**
   * Test All Returns Attributes
   *
   * Tests all returns the correct attributes.
   * @return void
   */
  public function testAllReturnsAttributes( ) : void {
    $this->assertSame(['first' => 'one', 'second' => 'two'], $this->mock->all( ));
  }


  /**
   * Test All Appends
   *
   * Test all appends from the appends array.
   * @return void
   */
  public function testAllAppends( ) : void {
    $this->mock->expects($this->any( ))->method('getAppends')->willReturn(['third']);
    $this->assertSame(['first' => 'one', 'second' => 'two', 'third' => null], $this->mock->all( ));
  }


  /**
   * Test All Hides
   *
   * Test all hides from the hides array.
   * @return void
   */
  public function testAllHides( ) : void {
    $this->mock->expects($this->any( ))->method('getHides')->willReturn(['first']);
    $this->assertSame(['second' => 'two'], $this->mock->all( ));
  }


  /**
   * Test All Applies Accessor
   *
   * Test all applies accessor methods.
   * @return void
   */
  public function testAllAppliesAccessor( ) : void {
    $mock = $this->getMockBuilder(Attributes::class)->setMethods(['accessFirst'])->getMockForTrait( );
    $mock->method('accessFirst')->willReturn('three');

    $reflection = new ReflectionClass($mock);
    $attributes = $reflection->getProperty('attributes');
    $attributes->setAccessible(true);
    $attributes->setValue($mock, ['first' => 'one', 'second' => 'two']);
    $attributes->setAccessible(false);

    $mock->expects($this->once( ))->method('accessFirst');
    $this->assertSame(['first' => 'three', 'second' => 'two'], $mock->all( ));
  }


  /**
   * Test Access Attribute No Method
   *
   * Test the accessAttribute method where no accessor method exists.
   * @return void
   */
  public function testAccessAttributeNoMethod( ) : void {
    $this->assertSame('one', $this->mock->accessAttribute('first', 'one'));
  }


  /**
   * Test Access Attribute With Method
   * 
   * Test the accessAttribute method where an accessor method exists.
   * @return void
   */
  public function testAccessAttributeWithMethod( ) : void {
    $mock = $this->getMockBuilder(Attributes::class)->setMethods(['accessFirst'])->getMockForTrait( );
    $mock->expects($this->once( ))->method('accessFirst')->willReturn('three');

    $reflection = new ReflectionClass($mock);
    $attributes = $reflection->getProperty('attributes');
    $attributes->setAccessible(true);
    $attributes->setValue($mock, ['first' => 'one']);
    $attributes->setAccessible(false);

    $this->assertSame('three', $mock->accessAttribute('first', 'one'));
  }


  /**
   * Test Delete Attribute Chains
   *
   * Test delete attribute chains.
   * @return void
   */
  public function testDeleteAttributeChains( ) : void {
    $this->assertInstanceOf(get_class($this->mock), $this->mock->deleteAttribute('first'));
    $this->assertNotSame($this->mock, $this->mock->deleteAttribute('first'));
  }


  /**
   * Test Delete Attribute Existing
   *
   * Test deleting an existing attribute.
   * @return void
   */
  public function testDeleteAttributeExisting( ) : void {
    $this->assertAttributeSame(['second' => 'two'], 'attributes', $this->mock->deleteAttribute('first'));
  }


  /**
   * Test Delete Attribute Missing
   *
   * Test deleting a missing attribute.
   * @return void
   */
  public function testDeleteAttributeMissing( ) : void {
    $this->mock->deleteAttribute('three');
    $this->assertAttributeSame(['first' => 'one', 'second' => 'two'], 'attributes', $this->mock);
  }


  /**
   * Test Delete Attribute Magic
   *
   * Tests deleting using native unset function through __unset magic
   * method.
   * @return void
   */
  public function testDeleteAttributeMagic( ) : void {
    unset($this->mock->first);
    $this->assertAttributeSame(['second' => 'two'], 'attributes', $this->mock);
  }


  /**
   * Test Fill Chains
   *
   * Test fill returns iteself to chain.
   * @return void
   */
  public function testFillChains( ) : void {
    $this->assertInstanceOf(get_class($this->mock), $this->mock->fill([ ]));
    $this->assertNotSame($this->mock, $this->mock->fill([ ]));
  }


  /**
   * Test Fill Additions
   *
   * Test fill with additions to existing data.
   * @return void
   */
  public function testFillAdditions( ) : void {
    $this->assertAttributeSame(
      ['first' => 'one', 'second' => 'two', 'third' => 'three'],
      'attributes',
      $this->mock->fill(['third' => 'three'])
    );
  }


  /**
   * Test Fill Overwrite
   *
   * Test fill overwriting a value.
   * @return void
   */
  public function testFillOverwrite( ) : void {
    $this->assertAttributeSame(
      ['first' => 'three', 'second' => 'two'],
      'attributes',
      $this->mock->fill(['first' => 'three'])
    );
  }


  /**
   * Test Fill Mutator
   *
   * Test fill with a mutator.
   * @return void
   */
  public function testFillMutator( ) : void {
    $mock = $this->getMockBuilder(Attributes::class)->setMethods(['mutateFirst'])->getMockForTrait( );
    $mock->expects($this->once( ))->method('mutateFirst')->willReturn('three');
    $this->assertAttributeSame(['first' => 'three'], 'attributes', $mock->fill(['first' => 'one']));
  }


  /**
   * Test Fill Fillable
   *
   * Test fill with the fillable restriction on it.
   * @return void
   */
  public function testFillFillable( ) : void {
    $mock = $this->getMockBuilder(Attributes::class)
      ->setMethods(['getAppends', 'getFillable', 'getHides'])
      ->getMockForTrait( );
    $mock->expects($this->any( ))->method('getFillable')->willReturn(['first']);

    $this->assertAttributeSame(['first' => 'one'], 'attributes', $mock->fill(['first' => 'one', 'second' => 'two']));
  }


  /**
   * Test Get Attribute Available
   *
   * Test getAttributes on an available attribute.
   * @return void
   */
  public function testGetAttributeAvailable( ) : void {
    $this->assertSame('one', $this->mock->getAttribute('first'));
  }


  /**
   * Test Get Attribute Missing
   *
   * Test getAttribute with a missing attribute.
   * @return void
   */
  public function testGetAttributeMissing( ) : void {
    $this->assertNull($this->mock->getAttribute('third'));
  }


  /**
   * Test Get Attribute Default
   *
   * Test getAttribute with a default value.
   * @return void
   */
  public function testGetAttributeDefault( ) : void {
    $this->assertSame('one', $this->mock->getAttribute('first', 'default'));
    $this->assertSame('default', $this->mock->getAttribute('third', 'default'));
  }


  /**
   * Test Get Attribute Magic
   *
   * Tests the __get magic method.
   * @return void
   */
  public function testGetAttributeMagic( ) : void {
    $this->assertSame('one', $this->mock->first);
    $this->assertNull($this->mock->third);
  }


  /**
   * Test Get Attribute Accessor
   *
   * Tests getAttribute with an accessor.
   * @return void
   */
  public function testGetAttributeAccessor( ) : void {
    $mock = $this->getMockBuilder(Attributes::class)->setMethods(['accessFirst', 'accessSecond'])->getMockForTrait( );
    $mock->expects($this->any( ))
      ->method('accessFirst')
      ->will($this->returnCallback(function($value) { return "$value.accessed"; }));

    $mock->expects($this->any( ))
      ->method('accessSecond')
      ->will($this->returnCallback(function($value) { return "$value.accessed"; }));

    $reflection = new ReflectionClass($mock);
    $attributes = $reflection->getProperty('attributes');
    $attributes->setAccessible(true);
    $attributes->setValue($mock, ['first' => 'one']);
    $attributes->setAccessible(false);

    $this->assertSame('one.accessed', $mock->getAttribute('first'));
    $this->assertSame('one.accessed', $mock->first);
    $this->assertSame('one.accessed', $mock->getAttribute('first', 'default'));
    $this->assertSame('.accessed', $mock->getAttribute('second'));
    $this->assertSame('default.accessed', $mock->getAttribute('second', 'default'));

  }


  /**
   * Test Get Iterator
   *
   * Tests the getIterator method returns a Traversable object.
   * @return void
   */
  public function testGetIterator( ) : void {
    $this->assertInstanceOf(Traversable::class, $this->mock->getIterator( ));
  }


  /**
   * Test Get Iterator Allows Looping
   *
   * Test getIterator then allows attributed objects to loop.
   * @return void
   */
  public function testGetIteratorAllowsLooping( ) : void {
    $results = ['first' => 'one', 'second' => 'two'];
    foreach($this->mock->getIterator( ) as $key => $value)
      $this->assertSame($results[$key], $value);
  }


  /**
   * Test Has Accessor Positive
   *
   * Tests hasAccessor for a positive result.
   * @return void
   */
  public function testHasAccessorPositive( ) : void {
    $mock = $this->getMockBuilder(Attributes::class)->setMethods(['accessFirst'])->getMockForTrait( );
    $mock->expects($this->any( ))->method('accessFirst');
    $this->assertTrue($mock->hasAccessor('first'));
  }


  /**
   * Test Has Accessor Negative
   *
   * Tests hasAccessor for negative results.
   * @return void
   */
  public function testHasAccessorNegative( ) : void {
    $mock = $this->getMockBuilder(Attributes::class)->setMethods(['accessFirst'])->getMockForTrait( );
    $mock->expects($this->any( ))->method('accessFirst');
    $this->assertFalse($mock->hasAccessor('firsts'));
    $this->assertFalse($mock->hasAccessor('second'));
  }


  /**
   * Test Has Attribute Positive
   *
   * Test hasAttribute with a positive result.
   * @return void
   */
  public function testHasAttributePositive( ) : void {
    $this->assertTrue($this->mock->hasAttribute('first'));
  }


  /**
   * Test Has Attribute Negative
   *
   * Test hasAttribute with a negative result.
   * @return void
   */
  public function testHasAttributeNegative( ) : void {
    $this->assertFalse($this->mock->hasAttribute('third'));
  }


  /**
   * Test Has Attribute Magic
   *
   * Test the __isset magic method version of hasAttribute.
   * @return void
   */
  public function testHasAttributeMagic( ) : void {
    $this->assertTrue(isset($this->mock->first));
    $this->assertFalse(isset($this->mock->third));
  }


  /**
   * Test Has Mutator Positive
   *
   * Tests hasMutator for a positive result.
   * @return void
   */
  public function testHasMutatorPositive( ) : void {
    $mock = $this->getMockBuilder(Attributes::class)->setMethods(['mutateFirst'])->getMockForTrait( );
    $mock->expects($this->any( ))->method('mutateFirst');
    $this->assertTrue($mock->hasMutator('first'));
  }


  /**
   * Test Has Mutator Negative
   *
   * Tests hasMutator for negative results.
   * @return void
   */
  public function testHasMutatorNegative( ) : void {
    $mock = $this->getMockBuilder(Attributes::class)->setMethods(['mutateFirst'])->getMockForTrait( );
    $mock->expects($this->any( ))->method('mutateFirst');
    $this->assertFalse($mock->hasMutator('firsts'));
    $this->assertFalse($mock->hasMutator('second'));
  }


  /**
   * Test Is Fillable Positive
   *
   * Test isFillable with a positive result.
   * @return void
   */
  public function testIsFillablePositive( ) : void {
    $this->mock->expects($this->any( ))->method('getFillable')->willReturn(['first']);
    $this->assertTrue($this->mock->isFillable('first'));
  }


  /**
   * Test Is Fillable Negative
   *
   * Test isFillable with a negative result.
   * @return void
   */
  public function testIsFillableNegative( ) : void {
    $this->mock->expects($this->any( ))->method('getFillable')->willReturn(['first']);
    $this->assertFalse($this->mock->isFillable('second'));
  }


  /**
   * Test Is Fillable Empty
   *
   * Test isFillable with an empty fillable set.
   * @return void
   */
  public function testIsFillableEmpty( ) : void {
    $this->assertTrue($this->mock->isFillable('first'));
  }


  /**
   * Test Mutate Attribute No Method
   *
   * Tests the mutateAttribute with no mutator method.
   * @return void
   */
  public function testMutateAttributeNoMethod( ) : void {
    $this->assertSame('three', $this->mock->mutateAttribute('first', 'three'));
  }


  /**
   * Test Mutate Attribute With Method
   *
   * Test the mutateAttribute with a mutator method.
   * @return void
   */
  public function testMutateAttributeWithMethod( ) : void {
    $mock = $this->getMockBuilder(Attributes::class)->setMethods(['mutateFirst'])->getMockForTrait( );
    $mock->expects($this->once( ))->method('mutateFirst')->willReturn('not-one');
    $this->assertSame('not-one', $mock->mutateAttribute('first', 'one'));
  }


  /**
   * Test Offset Exists Positive
   *
   * Test offsetExists with a positive result.
   * @return void
   */
  public function testOffsetExistsPositive( ) : void {
    $this->assertTrue($this->mock->offsetExists('first'));
  }


  /**
   * Test Offset Exists Negative
   *
   * Test offsetExists with a negative result.
   * @return void
   */
  public function testOffsetExistsNegative( ) : void {
    $this->assertFalse($this->mock->offsetExists('third'));
  }


  /**
   * Test Offset Get Exists
   *
   * Test offsetGet with an existing attribute.
   * @return void
   */
  public function testOffsetGetExists( ) : void {
    $this->assertSame('one', $this->mock->offsetGet('first'));
  }


  /**
   * Test Offset Get Missing
   *
   * Test offsetGet with an missing attribute.
   * @return void
   */
  public function testOffsetGetMissing( ) : void {
    $this->assertNull($this->mock->offsetGet('third'));
  }


  /**
   * Test Offset Set Exists
   *
   * Test offsetSet with an existing attribute.
   * @return void
   */
  public function testOffsetSetExists( ) : void {
    $this->mock->offsetSet('first', 'not-one');
    $this->assertAttributeSame(['first' => 'not-one', 'second' => 'two'], 'attributes', $this->mock);
  }


  /**
   * Test Offset Set New
   *
   * Test offsetSet with an new attribute.
   * @return void
   */
  public function testOffsetSetNew( ) : void {
    $this->mock->offsetSet('third', 'three');
    $this->assertAttributeSame(['first' => 'one', 'second' => 'two', 'third' => 'three'], 'attributes', $this->mock);
  }


  /**
   * Test Offset Unset Exists
   *
   * Test offsetUnset with an existing attribute.
   * @return void
   */
  public function testOffsetUnsetExists( ) : void {
    $this->mock->offsetUnset('first');
    $this->assertAttributeSame(['second' => 'two'], 'attributes', $this->mock);
  }


  /**
   * Test Offset Unset New
   *
   * Test offsetUnset with an new attribute.
   * @return void
   */
  public function testOffsetUnsetNew( ) : void {
    $this->mock->offsetUnset('third');
    $this->assertAttributeSame(['first' => 'one', 'second' => 'two'], 'attributes', $this->mock);
  }


  /**
   * Test Raw
   *
   * Test the raw method.
   * @return void
   */
  public function testRaw( ) : void {
    $this->assertSame(['first' => 'one', 'second' => 'two'], $this->mock->raw( ));
  }


  /**
   * Test Set Attribute Not Mutate
   *
   * Test setAtribute does not mutate the original.
   * @return void
   */
  public function testSetAttributeNotMutate( ) : void {
    $this->assertNotSame($this->mock, $this->mock->setAttribute('third', 'three'));
    $this->assertAttributeSame(['first' => 'one', 'second' => 'two'], 'attributes', $this->mock);
  }


  /**
   * Test Set Attribute New
   *
   * Test setAttributw with a new attribute.
   * @return void
   */
  public function testSetAttributeNew( ) : void {
    $this->assertAttributeSame(
      ['first' => 'one', 'second' => 'two', 'third' => 'three'],
      'attributes',
      $this->mock->setAttribute('third', 'three')
    );
  }


  /**
   * Test Set Attribute Existing
   *
   * Test setAttribute on an existing attribute.
   * @return void
   */
  public function testSetAttributeExisting( ) : void {
    $this->assertAttributeSame(
      ['first' => 'not-one', 'second' => 'two'],
      'attributes',
      $this->mock->setAttribute('first', 'not-one')
    );
  }


  /**
   * Test Set Attribute Magic
   *
   * Test setAttribute using the __set magic method.
   * @return void
   */
  public function testSetAttributeMagic( ) : void {
    $this->mock->first = 'not-one';
    $this->assertAttributeSame(['first' => 'not-one', 'second' => 'two'], 'attributes', $this->mock);
  }


  /**
   * Test Set Attribute With Fillable
   *
   * Test setAttribute with a fillable attribute
   * @return void
   */
  public function testSetAttributeWithFillable( ) : void {
    $mock = $this->getMockBuilder(Attributes::class)
      ->setMethods(['getAppends', 'getFillable', 'getHides'])
      ->getMockForTrait( );
    $mock->expects($this->any( ))->method('getFillable')->willReturn(['first']);
    $this->assertAttributeSame(['first' => 'not-one'], 'attributes', $mock->setAttribute('first', 'not-one'));
  }


  /**
   * Test Set Attribute Not Fillable
   *
   * Test setAttribute with a fillable attribute blocking the set.
   * @return void
   */
  public function testSetAttributeNotFillable( ) : void {
    $mock = $this->getMockBuilder(Attributes::class)
      ->setMethods(['getAppends', 'getFillable', 'getHides'])
      ->getMockForTrait( );
    $mock->expects($this->any( ))->method('getFillable')->willReturn(['first']);
    $this->assertAttributeSame([ ], 'attributes', $mock->setAttribute('second', 'not-two'));
  }


  /**
   * Test Set Attribute With Mutator
   *
   * Test setAttribute with a mutator.
   * @return void
   */
  public function testSetAttributeWithMutator( ) : void {
    $mock = $this->getMockBuilder(Attributes::class)->setMethods(['mutateFirst'])->getMockForTrait( );
    $mock->expects($this->once( ))->method('mutateFirst')->willReturn('three');
    $this->assertAttributeSame(['first' => 'three'], 'attributes', $mock->setAttribute('first', 'one'));
  }


  /**
   * Test To Array Returns Attributes
   *
   * Tests toArray returns the correct attributes.
   * @return void
   */
  public function testToArrayReturnsAttributes( ) : void {
    $this->assertSame(['first' => 'one', 'second' => 'two'], $this->mock->toArray( ));
  }


  /**
   * Test To Array Appends
   *
   * Test toArray appends from the appends array.
   * @return void
   */
  public function testToArrayAppends( ) : void {
    $this->mock->expects($this->any( ))->method('getAppends')->willReturn(['third']);
    $this->assertSame(['first' => 'one', 'second' => 'two', 'third' => null], $this->mock->toArray( ));
  }


  /**
   * Test To Array Hides
   *
   * Test toArray hides from the hides array.
   * @return void
   */
  public function testToArrayHides( ) : void {
    $this->mock->expects($this->any( ))->method('getHides')->willReturn(['first']);
    $this->assertSame(['second' => 'two'], $this->mock->toArray( ));
  }


  /**
   * Test To Array Applies Accessor
   *
   * Test toArray applies accessor methods.
   * @return void
   */
  public function testToArrayAppliesAccessor( ) : void {
    $mock = $this->getMockBuilder(Attributes::class)->setMethods(['accessFirst'])->getMockForTrait( );
    $mock->expects($this->once( ))->method('accessFirst')->willReturn('three');

    $reflection = new ReflectionClass($mock);
    $attributes = $reflection->getProperty('attributes');
    $attributes->setAccessible(true);
    $attributes->setValue($mock, ['first' => 'one', 'second' => 'two']);
    $attributes->setAccessible(false);

    $this->assertSame(['first' => 'three', 'second' => 'two'], $mock->toArray( ));
  }


  /**
   * Test To Array Nests
   *
   * Tests toArray converts nested toArray objects.
   * @return void
   */
  public function testToArrayNests( ) : void {
    $nested = $this->getMockBuilder(Attributes::class)->getMockForTrait( );
    $reflection = new ReflectionClass($nested);
    $attributes = $reflection->getProperty('attributes');
    $attributes->setAccessible(true);
    $attributes->setValue($nested, ['first' => 'one', 'second' => 'two']);
    $attributes->setAccessible(false);

    $mock = $this->getMockBuilder(Attributes::class)->getMockForTrait( );
    $reflection = new ReflectionClass($mock);
    $attributes = $reflection->getProperty('attributes');
    $attributes->setAccessible(true);
    $attributes->setValue($mock, ['nested' => $nested]);
    $attributes->setAccessible(false);

    $this->assertSame(['nested' => ['first' => 'one', 'second' => 'two']], $mock->toArray( ));
  }


  /**
   * Test To Json Correct
   *
   * Tests toJson returns the correct attributes.
   * @return void
   */
  public function testToJsonCorrect( ) : void {
    $this->assertJsonStringEqualsJsonString('{"first": "one", "second": "two"}', $this->mock->toJson( ));
  }


  /**
   * Test To Json Appends
   *
   * Test toJson appends from the appends array.
   * @return void
   */
  public function testToJsonAppends( ) : void {
    $this->mock->expects($this->any( ))->method('getAppends')->willReturn(['third']);
    $this->assertJsonStringEqualsJsonString('{"first": "one", "second": "two", "third": null}', $this->mock->toJson( ));
  }


  /**
   * Test To Json Hides
   *
   * Test toJson hides from the hides array.
   * @return void
   */
  public function testToJsonHides( ) : void {
    $this->mock->expects($this->any( ))->method('getHides')->willReturn(['first']);
    $this->assertJsonStringEqualsJsonString('{"second": "two"}', $this->mock->toJson( ));
  }


  /**
   * Test To Json Applies Accessor
   *
   * Test toJson applies accessor methods.
   * @return void
   */
  public function testToJsonAppliesAccessor( ) : void {
    $mock = $this->getMockBuilder(Attributes::class)->setMethods(['accessFirst'])->getMockForTrait( );
    $mock->expects($this->once( ))->method('accessFirst')->willReturn('three');

    $reflection = new ReflectionClass($mock);
    $attributes = $reflection->getProperty('attributes');
    $attributes->setAccessible(true);
    $attributes->setValue($mock, ['first' => 'one', 'second' => 'two']);
    $attributes->setAccessible(false);

    $this->assertJsonStringEqualsJsonString('{"first": "three", "second": "two"}', $mock->toJson( ));
  }


  /**
   * Test To Json Nests
   *
   * Tests toJson converts nested toArray objects.
   * @return void
   */
  public function testToJsonNests( ) : void {
    $nested = $this->getMockBuilder(Attributes::class)->getMockForTrait( );
    $reflection = new ReflectionClass($nested);
    $attributes = $reflection->getProperty('attributes');
    $attributes->setAccessible(true);
    $attributes->setValue($nested, ['first' => 'one', 'second' => 'two']);
    $attributes->setAccessible(false);

    $mock = $this->getMockBuilder(Attributes::class)->getMockForTrait( );
    $reflection = new ReflectionClass($mock);
    $attributes = $reflection->getProperty('attributes');
    $attributes->setAccessible(true);
    $attributes->setValue($mock, ['nested' => $nested]);
    $attributes->setAccessible(false);

    $this->assertJsonStringEqualsJsonString('{"nested": {"first": "one", "second": "two"}}', $mock->toJson( ));
  }


  /**
   * Test To String Correct
   *
   * Tests toString returns the correct attributes.
   * @return void
   */
  public function testToStringCorrect( ) : void {
    $this->assertJsonStringEqualsJsonString('{"first": "one", "second": "two"}', $this->mock->toString( ));
  }


  /**
   * Test To String Appends
   *
   * Test toString appends from the appends array.
   * @return void
   */
  public function testToStringAppends( ) : void {
    $this->mock->expects($this->any( ))->method('getAppends')->willReturn(['third']);
    $this->assertJsonStringEqualsJsonString('{"first": "one", "second": "two", "third": null}', $this->mock->toString( ));
  }


  /**
   * Test To String Hides
   *
   * Test toString hides from the hides array.
   * @return void
   */
  public function testToStringHides( ) : void {
    $this->mock->expects($this->any( ))->method('getHides')->willReturn(['first']);
    $this->assertJsonStringEqualsJsonString('{"second": "two"}', $this->mock->toString( ));
  }


  /**
   * Test To String Applies Accessor
   *
   * Test toString applies accessor methods.
   * @return void
   */
  public function testToStringAppliesAccessor( ) : void {
    $mock = $this->getMockBuilder(Attributes::class)->setMethods(['accessFirst'])->getMockForTrait( );
    $mock->expects($this->once( ))->method('accessFirst')->willReturn('three');

    $reflection = new ReflectionClass($mock);
    $attributes = $reflection->getProperty('attributes');
    $attributes->setAccessible(true);
    $attributes->setValue($mock, ['first' => 'one', 'second' => 'two']);
    $attributes->setAccessible(false);

    $this->assertJsonStringEqualsJsonString('{"first": "three", "second": "two"}', $mock->toString( ));
  }


  /**
   * Test To String Nests
   *
   * Tests toString converts nested toArray objects.
   * @return void
   */
  public function testToStringNests( ) : void {
    $nested = $this->getMockBuilder(Attributes::class)->getMockForTrait( );
    $reflection = new ReflectionClass($nested);
    $attributes = $reflection->getProperty('attributes');
    $attributes->setAccessible(true);
    $attributes->setValue($nested, ['first' => 'one', 'second' => 'two']);
    $attributes->setAccessible(false);

    $mock = $this->getMockBuilder(Attributes::class)->getMockForTrait( );
    $reflection = new ReflectionClass($mock);
    $attributes = $reflection->getProperty('attributes');
    $attributes->setAccessible(true);
    $attributes->setValue($mock, ['nested' => $nested]);
    $attributes->setAccessible(false);

    $this->assertJsonStringEqualsJsonString('{"nested": {"first": "one", "second": "two"}}', $mock->toString( ));
  }
}