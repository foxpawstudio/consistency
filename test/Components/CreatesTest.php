<?php

declare(strict_types=1);

use Foxpaw\Consistency\Components\Creates;
use PHPUnit\Framework\TestCase;


/**
 * Creates Test
 *
 * Tests the creates component.
 */
final class CreatesTest extends TestCase {

  /** @var Foxpaw\Consistency\Components\Creates   The mocked class for the trait. */
  protected $mock;


  /**
   * Set Up
   *
   * Set up the mock to run the tests on.
   * @return void
   */
  public function setUp( ) : void {
    $this->mock = $this->getMockBuilder(Creates::class)->getMockForTrait( );
  }


  /**
   * Test Create
   *
   * Test the create method.
   * @return void
   */
  public function testCreate( ) : void {
    $this->assertInstanceOf(get_class($this->mock), $this->mock::create('hello'));
  }
}
