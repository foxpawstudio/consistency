<?php

declare(strict_types=1);

use Foxpaw\Consistency\Arr;
use PHPUnit\Framework\Error\Error;
use PHPUnit\Framework\TestCase;


/**
 * Arr Test
 *
 * Tests the Arr class.
 */
final class ArrTest extends TestCase {

  /**
   * Test At Preserve Original
   *
   * Tests at preserves the original.
   * @return void
   */
  public function testAtPreserveOriginal( ) : void {
    $test = ['one', 'two'];
    Arr::at($test, 1);
    $this->assertSame(['one', 'two'], $test);
  }


  /**
   * Test At Positive Position
   *
   * Tests at with a positive position.
   * @return void
   */
  public function testAtPositivePosition( ) : void {
    $test = ['one', 'two'];
    $this->assertSame('two', Arr::at($test, 1));
  }


  /**
   * Test At Negative Position
   *
   * Tests at with a negative position.
   * @return void
   */
  public function testAtNegativePosition( ) : void {
    $test = ['one', 'two'];
    $this->assertSame('two', Arr::at($test, -1));
  }

  /**
   * Test At Associative
   *
   * Tests at with an associative array.
   * @return void
   */
  public function testAtAssociative( ) : void {
    $test = ['first' => 'one', 'second' => 'two'];
    $this->assertSame('two', Arr::at($test, 1));
  }


  /**
   * Test At Empty
   *
   * Test at with an empty array.
   * @return void
   */
  public function testAtEmpty( ) : void {
    $test = [ ];
    $this->assertNull(Arr::at($test, 5));
  }


  /**
   * Test At Default
   *
   * Tests at retrieving the default.
   * @return void
   */
  public function testAtDefault( ) : void {
    $test = [ ];
    $this->assertSame('default', Arr::at($test, 5, 'default'));
  }


  /**
   * Test At Over Length
   *
   * Test at over the length of the array.
   * @return void
   */
  public function testAtOverLength( ) : void {
    $test = ['one', 'two'];
    $this->assertNull(Arr::at($test, 100));
  }


  /**
   * Test Chunk Not Mutate Original
   *
   * Tests the chunk method does not mutate the original array
   * @return void
   */
  public function testChunkPreserveOriginal( ) : void {
    $test = ['test'];
    Arr::chunk($test, 1);
    $this->assertSame($test, ['test']);
  }


  /**
   * Test Chunk Evenly
   *
   * Tests the chunk method will chunk correctly when provided an evenly
   * dividing chunk.
   * @return void
   */
  public function testChunkEvenly( ) : void {
    $test = ['one', 'two', 'three', 'four', 'five', 'six'];
    $this->assertSame([['one', 'two'], ['three', 'four'], ['five', 'six']], Arr::chunk($test, 2));
    $this->assertSame([['one', 'two', 'three'], ['four', 'five', 'six']], Arr::chunk($test, 3));
  }


  /**
   * Test Chunk Unevenly
   *
   * Tests the chunk method will chunk correctly when unevenly chunking
   * an array.
   * @return void
   */
  public function testChunkUnevenly( ) : void {
    $test = ['one', 'two', 'three', 'four', 'five', 'six'];
    $this->assertSame([['one', 'two', 'three', 'four'], ['five', 'six']], Arr::chunk($test, 4));
  }


  /**
   * Test Chunk Negative
   *
   * Test chunking with a negative size.
   * @return void
   */
  public function testChunkNegative( ) : void {
    $test = ['one', 'two', 'three', 'four', 'five', 'six'];
    $this->assertSame([['one', 'two'], ['three', 'four'], ['five', 'six']], Arr::chunk($test, -2));
    $this->assertSame([['one', 'two'], ['three', 'four', 'five', 'six']], Arr::chunk($test, -4));
  }


  /**
   * Test Chunk Zero
   *
   * Test chunking into 0 elements.
   * @return void
   */
  public function testChunkZero( ) : void {
    $this->assertSame([ ], Arr::chunk(['one', 'two'], 0));
  }


  /**
   * Test Combine Preserve Original
   *
   * Tests the combine method does not mutate the original array.
   * @return void
   */
  public function testCombinePreserveOriginal( ) : void {
    $test = ['test'];
    Arr::combine($test, ['item']);
    $this->assertSame(['test'], $test);
  }


  /**
   * Test Combine Correctly
   *
   * Tests the combine method correctly combines the keys and values.
   * @return void
   */
  public function testCombineCorrectly( ) : void {
    $keys   = ['key'];
    $values = ['value'];
    $this->assertSame(['key' => 'value'], Arr::combine($keys, $values));
  }


  /**
   * Test Combine Mismatch
   * 
   * Tests that a warning is thrown when a mismatched set of data is used
   * with combine.
   * @return void
   */
  public function testCombineMismatch( ) : void {
    $this->expectException(Error::class);
    Arr::combine(['one'], ['two', 'three']);
  }


  /**
   * Test Count Preserve Original
   *
   * Tests the count method does not mutate the original array.
   * @return void
   */
  public function testCountPreserveOriginal( ) : void {
    $test = ['test'];
    Arr::count($test);
    $this->assertSame(['test'], $test);
  }


  /**
   * Test Count Correctly
   *
   * Tests the count method will function correctly.
   * @return void
   */
  public function testCountCorrectly( ) : void {
    $test = ['one', 'two', 'three', 'four', 'five', 'six'];
    $this->assertSame(6, Arr::count($test));
  }


  /**
   * Test Count Empty
   *
   * Test the count of an empty array.
   * @return void
   */
  public function testCountEmpty( ) {
    $this->assertSame(0, Arr::count([ ]));
  }


  /**
   * Test Delete Preserve Original
   *
   * Tests delete preserves the original array.
   * @return void
   */
  public function testDeletePreserveOriginal( ) : void {
    $test = ['one', 'two'];
    Arr::delete($test, 1);
    $this->assertSame(['one', 'two'], $test);

  }


  /**
   * Test Delete Existing Indexed
   *
   * Tests a successful deletion of an existing element on an indexed
   * array.
   * @return void
   */
  public function testDeleteExistingIndexed( ) : void {
    $test = ['one', 'two'];
    $this->assertSame(['one'], Arr::delete($test, 1));
  }


  /**
   * Test Delete Existing
   *
   * Tests a successful deletion of an existing element.
   * @return void
   */
  public function testDeleteExisting( ) : void {
    $test = ['first' => 'one', 'second' => 'two'];
    $this->assertSame(['second' => 'two'], Arr::delete($test, 'first'));
  }


  /**
   * Test Delete Existing Nested
   *
   * Test deletion of a nested element.
   * @return void
   */
  public function testDeleteExistingNested( ) : void {
    $test = ['first' => ['second' => 'two', 'third' => 'three']];
    $this->assertSame(['first' => ['third' => 'three']], Arr::delete($test, 'first.second'));
  }


  /**
   * Test Delete Missing
   *
   * Tests deleting a missing item.
   * @return void
   */
  public function testDeleteMissing( ) : void {
    $test = ['first' => 'one', 'second' => 'two'];
    $this->assertSame(['first' => 'one', 'second' => 'two'], Arr::delete($test, 'third'));
  }


  /**
   * Test Delete Existing Nested
   *
   * Test deletion of a nested element.
   * @return void
   */
  public function testDeleteExistingNestedMissing( ) : void {
    $test = ['first' => ['second' => 'two', 'third' => 'three']];
    $this->assertSame(['first' => ['second' => 'two', 'third' => 'three']], Arr::delete($test, 'first.fourth'));
  }


  /**
   * Test Diff Preserve Original
   *
   * Tests that diff preserves the original
   * @return void
   */
  public function testDiffPreserveOriginal( ) : void {
    $test = ['one', 'two'];
    Arr::diff($test, $test);
    $this->assertSame(['one', 'two'], $test);

    Arr::diff($test, $test, function($a, $b) { return $a === $b; });
    $this->assertSame(['one', 'two'], $test);
  }


  /**
   * Test Diff Default
   *
   * Tests the default callback for diff functions correctly.
   * @return void
   */
  public function testDiffDefault( ) : void {
    $test = ['one', 'two'];
    $this->assertSame(['one'], Arr::diff($test, ['two']));
    $this->assertSame([1 => 'two'], Arr::diff($test, ['one']));
  }


  /**
   * Test Diff Custom Function
   *
   * Tests using a custom function for diffing.
   * @return void
   */
  public function testDiffCustomFunction( ) : void {
    $test   = ['one', 'two'];
    $method = function($a, $b) { return $a <=> $b; };

    $this->assertSame([1 => 'two'], Arr::diff($test, ['one'], $method));

    $mock   = $this->getMockBuilder(stdClass::class)->setMethods(['__invoke'])->getMock( );
    $mock->expects($this->exactly(3))->method('__invoke');
    Arr::diff($test, ['one'], $mock);
  }


  /**
   * Test Diff Custom Function Incorrect Args
   *
   * Tests using a custom diff function with the wrong number of args.
   * @return void
   */
  public function testDiffCustomFunctionIncorrectArgs( ) : void {
    $test   = ['one', 'two'];

    $method = function($a, $b, $c) { return 1; };
    $this->expectException(ArgumentCountError::class);
    Arr::diff($test, ['one'], $method);

    $method = function($a) { return 1; };
    Arr::diff($test, ['one'], $method);
  }


  /**
   * Test Diff Associated Preserve Original
   *
   * Tests the diffAssociated function preserves the original.
   * @return void
   */
  public function testDiffAssociatedPreserveOriginal( ) : void {
    $test = ['test'];
    Arr::diffAssociated($test, $test);
    $this->assertSame(['test'], $test);
  }


  /**
   * Test Diff Associated Standard
   *
   * Tests that the diffAssociated method works correctly.
   * @return void
   */
  public function testDiffAssociatedStandard( ) : void {
    $test = ['first' => 'one', 'second' => 'two'];
    $this->assertSame(['first' => 'one', 'second' => 'two'], Arr::diffAssociated($test, ['two']));
    $this->assertSame(['first' => 'one'], Arr::diffAssociated($test, ['second' => 'two']));
   
    $test = ['one', 'two'];
    $this->assertSame(['one', 'two'], Arr::diffAssociated($test, ['two']));
    $this->assertSame(['one'], Arr::diffAssociated($test, [1 => 'two']));
  }


  /**
   * Test Diff Associated Custom Value Function
   *
   * Tests using a custom value callback function.
   * @return void
   */
  public function testDiffAssociatedCustomValueFunction( ) : void {
    $test   = ['first' => 'one', 'second' => 'two'];
    $method = function($a, $b) { return $a <=> $b; };

    $this->assertSame(['first' => 'one'], Arr::diffAssociated($test, ['second' => 'two'], $method));

    $mock   = $this->getMockBuilder(stdClass::class)->setMethods(['__invoke'])->getMock( );
    $mock->expects($this->any( ))->method('__invoke');
    Arr::diffAssociated($test, ['first' => 'one'], $mock);
  }


  /**
   * Test Diff Associated Custom Value Function Incorrect Args
   *
   * Tests using incorrect args on the value comparison function.
   * @return void
   */
  public function testDiffAssociatedCustomValueFunctionIncorrectArgs( ) : void {
    $test   = ['first' => 'one', 'second' => 'two'];

    $method = function($a, $b, $c) { return 1; };
    $this->expectException(ArgumentCountError::class);
    Arr::diffAssociated($test, ['first' => 'one'], $method);

    $method = function($a) { return 1; };
    Arr::diffAssociated($test, ['first' => 'one'], $method);
  }


  /**
   * Test Diff Associated Custom Key Function
   *
   * Tests using a custom key callback.
   * @return void
   */
  public function testDiffAssociatedCustomKeyFunction( ) : void {
    $test   = ['first' => 'one', 'second' => 'two'];
    $method = function($a, $b) { return $a <=> $b; };

    $this->assertSame(['first' => 'one'], Arr::diffAssociated($test, ['second' => 'two'], null, $method));

    $mock   = $this->getMockBuilder(stdClass::class)->setMethods(['__invoke'])->getMock( );
    $mock->expects($this->any( ))->method('__invoke');
    Arr::diffAssociated($test, ['first' => 'one'], null, $mock);
  }


  /**
   * Test Diff Associated Custom Key Function Incorrect Args
   *
   * Tests using incorrect args on the key comparison function.
   * @return void
   */
  public function testDiffAssociatedCustomKeyFunctionIncorrectArgs( ) : void {
    $test   = ['first' => 'one', 'second' => 'two'];

    $method = function($a, $b, $c) { return 1; };
    $this->expectException(ArgumentCountError::class);
    Arr::diffAssociated($test, ['first' => 'one'], null, $method);

    $method = function($a) { return 1; };
    Arr::diffAssociated($test, ['first' => 'one'], null, $method);
  }


  /**
   * Test Diff Associated Custom Functions
   *
   * Tests using custom callbacks for both key and value.
   * @return void
   */
  public function testDiffAssociatedCustomFunctions( ) : void {
    $test   = ['first' => 'one', 'second' => 'two'];
    $method = function($a, $b) { return $a <=> $b; };

    $this->assertSame(['first' => 'one'], Arr::diffAssociated($test, ['second' => 'two'], $method, $method));

    $mock   = $this->getMockBuilder(stdClass::class)->setMethods(['__invoke'])->getMock( );
    $mock->expects($this->any( ))->method('__invoke');
    Arr::diffAssociated($test, ['first' => 'one'], $mock, $mock);
  }


  /**
   * Test Diff Associated Custom Functions Incorrect Args
   *
   * Tests using incorrect args on the key and value comparison
   * functions.
   * @return void
   */
  public function testDiffAssociatedCustomFunctionsIncorrectArgs( ) : void {
    $test   = ['first' => 'one', 'second' => 'two'];

    $method = function($a, $b, $c) { return 1; };
    $this->expectException(ArgumentCountError::class);
    Arr::diffAssociated($test, ['first' => 'one'], $method, $method);

    $method = function($a) { return 1; };
    Arr::diffAssociated($test, ['first' => 'one'], $method, $method);
  }


  /**
   * Test Diff Key Preserve Original
   *
   * Tests the diffKey method preserves the original.
   * @return void
   */
  public function testDiffKeyPreserveOriginal( ) : void {
    $test = ['first', 'second'];
    Arr::diffKey($test, $test);
    $this->assertSame(['first', 'second'], $test);
  }


  /**
   * Test Diff Key Default
   *
   * Tests the default diffKey callback.
   * @return void
   */
  public function testDiffKeyDefault( ) : void {
    $test = ['first', 'second'];
    $this->assertSame([1 => 'second'], Arr::diffKey($test, ['anything']));
    
    $test = ['first' => 'one', 'second' => 'two'];
    $this->assertSame(['first' => 'one'], Arr::diffKey($test, ['second' => 'anything']));
  }


  /**
   * Test Diff Key Custom Function
   *
   * Tests the diffKey method with a custom callback.
   * @return void
   */
  public function testDiffKeyCustomFunction( ) : void {
    $test   = ['first' => 'one', 'second' => 'two'];
    $method = function($a, $b) { return $a <=> $b; };

    $this->assertSame(['first' => 'one'], Arr::diffKey($test, ['second' => 'anything'], $method));

    $mock   = $this->getMockBuilder(stdClass::class)->setMethods(['__invoke'])->getMock( );
    $mock->expects($this->any( ))->method('__invoke');
    Arr::diffKey($test, ['first' => 'anything'], $mock);
  }


  /**
   * Test Diff Key Custom Function Incorrect Args
   *
   * Tests the diffKey function with incorrect number of args in the
   * callback.
   * @return void
   */
  public function testDiffKeyCustomFunctionIncorrectArgs( ) : void {
    $test   = ['first' => 'one', 'second' => 'two'];

    $method = function($a, $b, $c) { return 1; };
    $this->expectException(ArgumentCountError::class);
    Arr::diffKey($test, ['first' => 'one'], $method);

    $method = function($a) { return 1; };
    Arr::diffKey($test, ['first' => 'one'], $method);
  }


  /**
   * Test Exists Preserve Original
   *
   * Tests the exists method preserves the original.
   * @return void
   */
  public function testExistsPreserveOriginal( ) : void {
    $test = ['first' => 'one'];
    Arr::exists($test, 'first');
    $this->assertSame(['first' => 'one'], $test);
  }


  /**
   * Test Exists Indexed
   *
   * Tests exists functions correctly with an indexed array.
   * @return void
   */
  public function testExistsIndexed( ) : void {
    $test = ['one', 'two'];
    $this->assertTrue(Arr::exists($test, 1));
    $this->assertFalse(Arr::exists($test, 100));
    $this->assertTrue(Arr::exists($test, 0));
    $this->assertFalse(Arr::exists($test, -1));
  }


  /**
   * Test Exists Associative
   *
   * Tests the exists function works correctly with an associative array.
   * @return void
   */
  public function testExistsAssociative( ) : void {
    $test = ['first' => 'one', 'second' => 'two'];
    $this->assertTrue(Arr::exists($test, 'first'));
    $this->assertFalse(Arr::exists($test, 'third'));
    $this->assertFalse(Arr::exists($test, 0));
    $this->assertFalse(Arr::exists($test, -1));
  }


  /**
   * Test Exists Nested
   *
   * Tests exists with a nested array.
   * @return void
   */
  public function testExistsNested( ) : void {
    $test = ['first' => ['second' => 'value']];
    $this->assertTrue(Arr::exists($test, 'first.second'));
    $this->assertFalse(Arr::exists($test, 'first.third'));
    $this->assertFalse(Arr::exists($test, 'third.second'));
  }


  /**
   * Test Expand Preserve Original
   *
   * Tests the expand method preserves the original.
   * @return void
   */
  public function testExpandPreserveOriginal( ) : void {
    $test = ['first' => 'one', 'second' => 'two'];
    Arr::expand($test);
    $this->assertSame(['first' => 'one', 'second' => 'two'], $test);
  }


  /**
   * Test Expand Indexed
   *
   * Tests the expand method functions correctly with an indexed array.
   * @return void
   */
  public function testExpandIndexed( ) : void {
    $test = ['one', 'two'];
    $this->assertSame(['one', 'two'], Arr::expand($test));
  }


  /**
   * Test Expand No Work
   *
   * Tests expanding when there is nothing to do.
   * @return void
   */
  public function testExpandNoWork( ) : void {
    $test = ['first' => 'one', 'second' => 'two'];
    $this->assertSame(['first' => 'one', 'second' => 'two'], Arr::expand($test));
  }


  /**
   * Test Expand Flattened
   *
   * Tests expanding a flattened array
   * @return void
   */
  public function testExpandFlattened( ) : void {
    $test = ['first.second' => 'one', 'first.third' => 'two'];
    $this->assertSame(['first' => ['second' => 'one', 'third' => 'two']], Arr::expand($test));
  }


  /**
   * Test Expand Overwrite
   *
   * Test expand functions correctly when expanding a flattened array
   * which had overwrites.
   * @return void
   */
  public function testExpandOverwrite( ) : void {
    $test = ['first.second' => 'one', 'first.third' => 'two', 'first' => 'remaining'];
    $this->assertSame(['first' => 'remaining'], Arr::expand($test));
  }


  /**
   * Test Fill Default
   *
   * Tests a default fill.
   * @return void
   */
  public function testFillDefault( ) : void {
    $this->assertSame([1 => 'one', 2 => 'one', 3 => 'one'], Arr::fill(1, 3, 'one'));
  }


  /**
   * Test Fill Negative Start
   *
   * Tests fill with a negative start value.
   * @return void
   */
  public function testFillNegativeStart( ) : void {
    $this->assertSame([-5 => 'one', 0 => 'one', 1 => 'one'], Arr::fill(-5, 3, 'one'));
  }

  
  /**
   * Test Fill Negative Length
   *
   * Tests filling with a negative length.
   * @return void
   */
  public function testFillNegativeLength( ) : void {
    $this->expectException(Error::class);
    Arr::fill(0, -5, 'fail');
  }


  /**
   * Test Fill Zero Start
   *
   * Test using a zero start value.
   * @return void
   */
  public function testFillZeroStart( ) : void {
    $this->assertSame([0 => 'zero'], Arr::fill(0, 1, 'zero'));
  }


  /**
   * Test Fill Zero Length
   *
   * Test filling with a zero length.
   * @return void
   */
  public function testFillZeroLength( ) : void {
    $this->assertSame([ ], Arr::fill(0, 0, 'void'));
  }


  /**
   * Test Filter Preserve Original
   *
   * Test filter will not alter original.
   * @return void
   */
  public function testFilterPreserveOriginal( ) : void {
    $test = ['', 0, false];
    Arr::filter($test);
    $this->assertSame(['', 0, false], $test);
  }


  /**
   * Test Filter Correctly
   *
   * Tests filtering of falsey values.
   * @return void
   */
  public function testFilterCorrectly( ) : void {
    $test = ['keep', [ ], 0, '', false, ' ', null];
    $this->assertSame([0 => 'keep', 5 => ' '], Arr::filter($test));
  }


  /**
   * Test Filter Callback
   *
   * Tests filter using a custom callback.
   * @return void
   */
  public function testFilterCallback( ) : void {
    $test   = ['one', 'two'];
    $method = function($a) { return $a !== 'one'; };

    $this->assertSame([1 => 'two'], Arr::filter($test, $method));

    $mock   = $this->getMockBuilder(stdClass::class)->setMethods(['__invoke'])->getMock( );
    $mock->expects($this->any( ))->method('__invoke');
    Arr::filter($test, $mock);
  }


  /**
   * Test Filter Callback Incorrect Args
   *
   * Tests the filter callback if there are an incorrect number of args.
   * @return void
   */
  public function testFilterCallbackIncorrectArgs( ) : void {
    $test   = ['one', 'two'];

    $this->expectException(ArgumentCountError::class);
    $method = function($a, $b, $c) { return 1; };
    Arr::filter($test, $method);

    $method = function( ) { return 1; };
    Arr::filter($test, $method);
  }


  /**
   * Test Find Preserve Original
   *
   * Test find preserves the original.
   * @return void
   */
  public function testFindPreserveOriginal( ) : void {
    $test = ['test'];
    Arr::find($test, 0);
    $this->assertSame(['test'], $test);
  }


  /**
   * Test Find Retrieves
   *
   * Test find retrieves the correct value.
   * @return void
   */
  public function testFindRetrieves( ) : void {
    $test = ['first' => 'one', 'second' => 'two'];
    $this->assertSame('one', Arr::find($test, 'first'));
  }


  /**
   * Test Find Nested
   *
   * Test finding a nested element.
   * @return void
   */
  public function testFindNested( ) : void {
    $test = ['first' => ['second' => 'two']];
    $this->assertSame('two', Arr::find($test, 'first.second'));
  }


  /**
   * Test Find Missing
   *
   * Tests finding a missing value.
   * @return void
   */
  public function testFindMissing( ) : void {
    $test = ['first' => 'one', 'second' => 'two'];
    $this->assertNull(Arr::find($test, 'third'));
  }


  /**
   * Test Find Nested Same
   *
   * Test finding a nested element.
   * @return void
   */
  public function testFindNestedMissing( ) : void {
    $test = ['first' => ['second' => 'two']];
    $this->assertNull(Arr::find($test, 'third.second'));
    $this->assertNull(Arr::find($test, 'first.third'));
  }


  /**
   * Test Find Negative
   *
   * Tests find with a negative index.
   * @return void
   */
  public function testFindNegative( ) : void {
    $test = ['one', 'two'];
    $this->assertNull(Arr::find($test, -1));
  }


  /**
   * Test Find Default
   *
   * Test finding a default value.
   * @return void
   */
  public function testFindDefault( ) : void {
    $test = ['first' => 'one', 'second' => 'two'];
    $this->assertSame('default', Arr::find($test, 'third', 'default'));
  }


  /**
   * Test Find Empty
   *
   * Test finding from an empty array.
   * @return void
   */
  public function testFindEmpty( ) : void {
    $test = [ ];
    $this->assertNull(Arr::find($test, 'first'));
  }


  /**
   * Test Find Associative
   *
   * Tests find with index on associative array.
   * @return void
   */
  public function testFindAssociative( ) : void {
    $test = ['first' => 'one', 'second' => 'two'];
    $this->assertNull(Arr::find($test, 1));
  }


  /**
   * Test First Preserve Original
   *
   * Tests the first function preserves the original.
   * @return void
   */
  public function testFirstPreserveOriginal( ) : void {
    $test = ['first', 'second'];
    Arr::first($test);
    $this->assertSame(['first', 'second'], $test);
  }


  /**
   * Test First Retrieval
   *
   * Tests the first method retrieval.
   * @return void
   */
  public function testFirstRetrieval( ) : void {
    $test = ['first', 'second'];
    $this->assertSame('first', Arr::first($test));
  }


  /**
   * Test First Empty
   *
   * Tests the first method when the array is empty.
   * @return void
   */
  public function testFirstEmpty( ) : void {
    $test = [ ];
    $this->assertNull(Arr::first($test));
  }


  /**
   * Test First Default
   *
   * Tests the default value is used.
   * @return void
   */
  public function testFirstDefault( ) : void {
    $test = [ ];
    $this->assertSame('default', Arr::first($test, 'default'));
  }


  /**
   * Test Flatten Preserve Original
   *
   * Tests the flatten method preserves the original.
   * @return void
   */
  public function testFlattenPreserveOriginal( ) : void {
    $test = ['first' => ['second' => 'two', 'third' => 'three']];
    Arr::flatten($test);
    $this->assertSame(['first' => ['second' => 'two', 'third' => 'three']], $test);
  }


  /**
   * Test Flatten One Dimension
   *
   * Tests flatten on a one dimensional array.
   * @return void
   */
  public function testFlattenOneDimension( ) : void {
    $test = ['first' => 'one', 'second' => 'two'];
    $this->assertSame(['first' => 'one', 'second' => 'two'], Arr::flatten($test));
  }


  /**
   * Test Flatten Correct
   *
   * Tests flatten works as intended.
   * @return void
   */
  public function testFlattenCorrect( ) : void {
    $test = ['first' => ['second' => 'two', 'third' => 'three']];
    $this->assertSame(['first.second' => 'two', 'first.third' => 'three'], Arr::flatten($test));
  }


  /**
   * Test Flatten Indexed
   *
   * Test flatten on an indexed array.
   * @return void
   */
  public function testFlattenIndexed( ) : void {
    $test = ['one', 'two'];
    $this->assertSame(['one', 'two'], Arr::flatten($test));

    $test = [['one', 'two']];
    $this->assertSame(['0.0' => 'one', '0.1' => 'two'], Arr::flatten($test));
  }


  /**
   * Test Flatten Empty
   *
   * Tests flatten on an empty array.
   * @return void
   */
  public function testFlattenEmpty( ) : void {
    $test = [ ];
    $this->assertSame([ ], Arr::flatten($test));
  }


  /**
   * Test Flatten Overwrite
   *
   * Tests flatten will correctly overwrite if needed.
   * @return void
   */
  public function testFlattenOverwrite( ) : void {
    $test = ['first' => ['second' => 'two'], 'first.second' => 'replace'];
    $this->assertSame(['first.second' => 'replace'], Arr::flatten($test));
  }


  /**
   * Test Flip Preserve Original
   *
   * Tests flip preserves the original.
   * @return void
   */
  public function testFlipPreserveOriginal( ) : void {
    $test = ['first' => 'one', 'second' => 'two'];
    Arr::flip($test);
    $this->assertSame(['first' => 'one', 'second' => 'two'], $test);
  }


  /**
   * Test Flip Indexed
   *
   * Tests flip on an indexed array.
   * @return void
   */
  public function testFlipIndexed( ) : void {
    $test = ['one', 'two'];
    $this->assertSame(['one' => 0, 'two' => 1], Arr::flip($test));
  }


  /**
   * Test Flip Associative
   *
   * Tests flip on an associative array.
   * @return void
   */
  public function testFlipAssociative( ) : void {
    $test = ['first' => 'one', 'second' => 'two'];
    $this->assertSame(['one' => 'first', 'two' => 'second'], Arr::flip($test));
  }


  /**
   * Test Flip Invalid Values
   *
   * Tests flip when it contains invalid values.
   * @return void
   */
  public function testFlipInvalidValues( ) : void {
    $test = ['first' => [ ]];
    $this->expectException(Error::class);
    Arr::flip($test);
  }


  /**
   * Test Flip Empty
   *
   * Tests flipping an empty array.
   * @return void
   */
  public function testFlipEmpty( ) : void {
    $test = [ ];
    $this->assertSame([ ], Arr::flip($test));
  }


  /**
   * Test Has Preserve Original
   *
   * Tests that has preserves the original.
   * @return void
   */
  public function testHasPreserveOriginal( ) : void {
    $test = ['test'];
    Arr::has($test, 'test');
    $this->assertSame(['test'], $test);
  }


  /**
   * Test Has Finds Item
   *
   * Tests has finds an item.
   * @return void
   */
  public function testHasFindsItem( ) : void {
    $test = ['one', 'two'];
    $this->assertTrue(Arr::has($test, 'two'));
  }


  /**
   * Test Has Missing
   *
   * Tests has handles missing items.
   * @return void
   */
  public function testHasMissing( ) : void {
    $test = ['one', 'two'];
    $this->assertFalse(Arr::has($test, 'three'));
  }


  /**
   * Test Has Strict
   *
   * Test has using strict comparison
   * @return void
   */
  public function testHasStrict( ) : void {
    $test = [1.0, '2'];
    $this->assertTrue(Arr::has($test, '1'));
    $this->assertTrue(Arr::has($test, 1.0, true));
    $this->assertFalse(Arr::has($test, 1, true));
  }


  /**
   * Test Has Empty
   *
   * Tests the has method functions when empty.
   * @return void
   */
  public function testHasEmpty( ) : void {
    $test = [ ];
    $this->assertFalse(Arr::has($test, 'test'));
  }


  /**
   * Test Intersect Preserve Original
   *
   * Tests that intersect preserves the original
   * @return void
   */
  public function testIntersectPreserveOriginal( ) : void {
    $test = ['one', 'two'];
    Arr::intersect($test, $test);
    $this->assertSame(['one', 'two'], $test);

    Arr::intersect($test, $test, function($a, $b) { return $a === $b; });
    $this->assertSame(['one', 'two'], $test);
  }


  /**
   * Test Intersect Default
   *
   * Tests the default callback for intersect functions correctly.
   * @return void
   */
  public function testIntersectDefault( ) : void {
    $test = ['one', 'two'];
    $this->assertSame(['one'], Arr::intersect($test, ['one']));
    $this->assertSame([1 => 'two'], Arr::intersect($test, ['two']));
  }


  /**
   * Test Intersect Custom Function
   *
   * Tests using a custom function for intersecting.
   * @return void
   */
  public function testIntersectCustomFunction( ) : void {
    $test   = ['one', 'two'];
    $method = function($a, $b) { return $a <=> $b; };

    $this->assertSame(['one'], Arr::intersect($test, ['one'], $method));

    $mock   = $this->getMockBuilder(stdClass::class)->setMethods(['__invoke'])->getMock( );
    $mock->expects($this->exactly(3))->method('__invoke');
    Arr::intersect($test, ['one'], $mock);
  }


  /**
   * Test Intersect Custom Function Incorrect Args
   *
   * Tests using a custom intersect function with the wrong number of args.
   * @return void
   */
  public function testIntersectCustomFunctionIncorrectArgs( ) : void {
    $test   = ['one', 'two'];

    $method = function($a, $b, $c) { return 1; };
    $this->expectException(ArgumentCountError::class);
    Arr::intersect($test, ['one'], $method);

    $method = function($a) { return 1; };
    Arr::intersect($test, ['one'], $method);
  }


  /**
   * Test Intersect Associated Preserve Original
   *
   * Tests the intersectAssociated function preserves the original.
   * @return void
   */
  public function testIntersectAssociatedPreserveOriginal( ) : void {
    $test = ['test'];
    Arr::intersectAssociated($test, $test);
    $this->assertSame(['test'], $test);
  }


  /**
   * Test Intersect Associated Standard
   *
   * Tests that the intersectAssociated method works correctly.
   * @return void
   */
  public function testIntersectAssociatedStandard( ) : void {
    $test = ['first' => 'one', 'second' => 'two'];
    $this->assertSame([ ], Arr::intersectAssociated($test, ['two']));
    $this->assertSame(['second' => 'two'], Arr::intersectAssociated($test, ['second' => 'two']));
   
    $test = ['one', 'two'];
    $this->assertSame([ ], Arr::intersectAssociated($test, ['two']));
    $this->assertSame([1 => 'two'], Arr::intersectAssociated($test, [1 => 'two']));
  }


  /**
   * Test Intersect Associated Custom Value Function
   *
   * Tests using a custom value callback function.
   * @return void
   */
  public function testIntersectAssociatedCustomValueFunction( ) : void {
    $test   = ['first' => 'one', 'second' => 'two'];
    $method = function($a, $b) { return $a <=> $b; };

    $this->assertSame(['second' => 'two'], Arr::intersectAssociated($test, ['second' => 'two'], $method));

    $mock   = $this->getMockBuilder(stdClass::class)->setMethods(['__invoke'])->getMock( );
    $mock->expects($this->any( ))->method('__invoke');
    Arr::intersectAssociated($test, ['first' => 'one'], $mock);
  }


  /**
   * Test Intersect Associated Custom Value Function Incorrect Args
   *
   * Tests using incorrect args on the value comparison function.
   * @return void
   */
  public function testIntersectAssociatedCustomValueFunctionIncorrectArgs( ) : void {
    $test   = ['first' => 'one', 'second' => 'two'];

    $method = function($a, $b, $c) { return 1; };
    $this->expectException(ArgumentCountError::class);
    Arr::intersectAssociated($test, ['first' => 'one'], $method);

    $method = function($a) { return 1; };
    Arr::intersectAssociated($test, ['first' => 'one'], $method);
  }


  /**
   * Test Intersect Associated Custom Key Function
   *
   * Tests using a custom key callback.
   * @return void
   */
  public function testIntersectAssociatedCustomKeyFunction( ) : void {
    $test   = ['first' => 'one', 'second' => 'two'];
    $method = function($a, $b) { return $a <=> $b; };

    $this->assertSame(['second' => 'two'], Arr::intersectAssociated($test, ['second' => 'two'], null, $method));

    $mock   = $this->getMockBuilder(stdClass::class)->setMethods(['__invoke'])->getMock( );
    $mock->expects($this->any( ))->method('__invoke');
    Arr::intersectAssociated($test, ['first' => 'one'], null, $mock);
  }


  /**
   * Test Intersect Associated Custom Key Function Incorrect Args
   *
   * Tests using incorrect args on the key comparison function.
   * @return void
   */
  public function testIntersectAssociatedCustomKeyFunctionIncorrectArgs( ) : void {
    $test   = ['first' => 'one', 'second' => 'two'];

    $method = function($a, $b, $c) { return 1; };
    $this->expectException(ArgumentCountError::class);
    Arr::intersectAssociated($test, ['first' => 'one'], null, $method);

    $method = function($a) { return 1; };
    Arr::intersectAssociated($test, ['first' => 'one'], null, $method);
  }


  /**
   * Test Intersect Associated Custom Functions
   *
   * Tests using custom callbacks for both key and value.
   * @return void
   */
  public function testIntersectAssociatedCustomFunctions( ) : void {
    $test   = ['first' => 'one', 'second' => 'two'];
    $method = function($a, $b) { return $a <=> $b; };

    $this->assertSame(['second' => 'two'], Arr::intersectAssociated($test, ['second' => 'two'], $method, $method));

    $mock   = $this->getMockBuilder(stdClass::class)->setMethods(['__invoke'])->getMock( );
    $mock->expects($this->any( ))->method('__invoke');
    Arr::intersectAssociated($test, ['first' => 'one'], $mock, $mock);
  }


  /**
   * Test Intersect Associated Custom Functions Incorrect Args
   *
   * Tests using incorrect args on the key and value comparison
   * functions.
   * @return void
   */
  public function testIntersectAssociatedCustomFunctionsIncorrectArgs( ) : void {
    $test   = ['first' => 'one', 'second' => 'two'];

    $method = function($a, $b, $c) { return 1; };
    $this->expectException(ArgumentCountError::class);
    Arr::intersectAssociated($test, ['first' => 'one'], $method, $method);

    $method = function($a) { return 1; };
    Arr::intersectAssociated($test, ['first' => 'one'], $method, $method);
  }


  /**
   * Test Intersect Key Preserve Original
   *
   * Tests the intersectKey method preserves the original.
   * @return void
   */
  public function testIntersectKeyPreserveOriginal( ) : void {
    $test = ['first', 'second'];
    Arr::intersectKey($test, $test);
    $this->assertSame(['first', 'second'], $test);
  }


  /**
   * Test Intersect Key Default
   *
   * Tests the default intersectKey callback.
   * @return void
   */
  public function testIntersectKeyDefault( ) : void {
    $test = ['first', 'second'];
    $this->assertSame(['first'], Arr::intersectKey($test, ['anything']));
    
    $test = ['first' => 'one', 'second' => 'two'];
    $this->assertSame(['second' => 'two'], Arr::intersectKey($test, ['second' => 'anything']));
  }


  /**
   * Test Intersect Key Custom Function
   *
   * Tests the intersectKey method with a custom callback.
   * @return void
   */
  public function testIntersectKeyCustomFunction( ) : void {
    $test   = ['first' => 'one', 'second' => 'two'];
    $method = function($a, $b) { return $a <=> $b; };

    $this->assertSame(['second' => 'two'], Arr::intersectKey($test, ['second' => 'anything'], $method));

    $mock   = $this->getMockBuilder(stdClass::class)->setMethods(['__invoke'])->getMock( );
    $mock->expects($this->any( ))->method('__invoke');
    Arr::intersectKey($test, ['first' => 'anything'], $mock);
  }


  /**
   * Test Intersect Key Custom Function Incorrect Args
   *
   * Tests the intersectKey function with incorrect number of args in the
   * callback.
   * @return void
   */
  public function testIntersectKeyCustomFunctionIncorrectArgs( ) : void {
    $test   = ['first' => 'one', 'second' => 'two'];

    $method = function($a, $b, $c) { return 1; };
    $this->expectException(ArgumentCountError::class);
    Arr::intersectKey($test, ['first' => 'one'], $method);

    $method = function($a) { return 1; };
    Arr::intersectKey($test, ['first' => 'one'], $method);
  }


  /**
   * Test Join Preserve Original
   *
   * Tests join does not alter the original.
   * @return void
   */
  public function testJoinPreserveOriginal( ) : void {
    $test = ['first', 'second'];
    Arr::join($test);
    $this->assertSame(['first', 'second'], $test);
  }


  /**
   * Test Join Default
   *
   * Test a default join.
   * @return void
   */
  public function testJoinDefault( ) : void {
    $test = ['first', 'second'];
    $this->assertSame('firstsecond', Arr::join($test));
  }


  /**
   * Test Join Delimiter
   *
   * Tests join with a delimiter.
   * @return void
   */
  public function testJoinDelimiter( ) : void {
    $test = ['first', 'second'];
    $this->assertSame('first.second', Arr::join($test, '.'));
  }


  /**
   * Test Join Not Strings
   *
   * Test join with an array of not strings.
   * @return void
   */
  public function testJoinNotStrings( ) : void {
    $test = [1, 2, 3];
    $this->assertSame('123', Arr::join($test));
  }


  /**
   * Test Join Empty
   *
   * Tests joining an empty array.
   * @return void
   */
  public function testJoinEmpty( ) : void {
    $test = [ ];
    $this->assertSame('', Arr::join($test));
  }


  /**
   * Test Keys Preserve Original
   *
   * Tests keys preserves the original.
   * @return void
   */
  public function testKeysPreserveOriginal( ) : void {
    $test = ['test'];
    Arr::keys($test);
    $this->assertSame(['test'], $test);
  }


  /**
   * Test Keys Associative
   *
   * Tests keys on an associative array.
   * @return void
   */
  public function testKeysAssociative( ) : void {
    $test = ['first' => 'one', 'second' => 'two'];
    $this->assertSame(['first', 'second'], Arr::keys($test));
  }


  /**
   * Test Keys Indexed
   *
   * Test keys on an indexed array.
   * @return void
   */
  public function testKeysIndexed( ) : void {
    $test = ['one', 'two'];
    $this->assertSame([0, 1], Arr::keys($test));
  }


  /**
   * Test Keys Empty
   *
   * Tests keys on an empty array.
   * @return void
   */
  public function testKeysEmpty( ) : void {
    $test = [ ];
    $this->assertSame([ ], Arr::keys($test));
  }


  /**
   * Test Last Preserve Original
   *
   * Test last preserves the original
   * @return void
   */
  public function testLastPreserveOriginal( ) : void {
    $test = ['test'];
    Arr::last($test);
    $this->assertSame(['test'], $test);
  }


  /**
   * Test Last Retrieval
   *
   * Tests retrieving the last element.
   * @return void
   */
  public function testLastRetrieval( ) : void {
    $test = ['first', 'second'];
    $this->assertSame('second', Arr::last($test));
  }


  /**
   * Test Last Empty
   *
   * Test retrieving the last element of an empty array.
   * @return void
   */
  public function testLastEmpty( ) : void {
    $test = [ ];
    $this->assertNull(Arr::last($test));
  }


  /**
   * Test Last Default
   *
   * Tests last with a default value.
   * @return void
   */
  public function testLastDefault( ) : void {
    $test = [ ];
    $this->assertSame('default', Arr::last($test, 'default'));
  }


  /**
   * Test Map Preserve Original
   *
   * Tests that map preserves the original.
   * @return void
   */
  public function testMapPreserveOriginal( ) : void {
    $test = ['test'];
    Arr::map($test, function($value) { return strtoupper($value); });
    $this->assertSame(['test'], $test);
  }


  /**
   * Test Map Performs
   *
   * Test that map performs the operation.
   * @return void
   */
  public function testMapPerforms( ) : void {
    $test = ['first', 'second'];
    $this->assertSame(['FIRST', 'SECOND'], Arr::map($test, function($value) { return strtoupper($value); }));
  }


  /**
   * Test Map Incorrect Args
   *
   * Tests the outcome when the callback has incorrect args.
   * @return void
   */
  public function testMapIncorrectArgs( ) : void {
    $test = ['first', 'second'];

    $this->expectException(ArgumentCountError::class);
    $method = function($a, $b, $c) { return 1; };
    Arr::map($test, $method);

    $method = function( ) { return 1; };
    Arr::map($test, $method);
  }


  /**
   * Test Map Empty
   *
   * Tests mapping over an empty array.
   * @return void
   */
  public function testMapEmpty( ) : void {
    $test = [ ];
    $this->assertSame([ ], Arr::map($test, function($value) { return strtoupper($value); }));
  }


  /**
   * Test Merge Preserve Original
   *
   * Tests that merge does not alter the original.
   * @return void
   */
  public function testMergePreserveOriginal( ) : void {
    $test = ['test'];
    Arr::merge($test, $test);
    $this->assertSame($test, ['test']);
  }


  /**
   * Test Merge Success
   *
   * Tests a successful merge.
   * @return void
   */
  public function testMergeSuccess( ) : void {
    $test = ['first'];
    $this->assertSame(['first', 'first'], Arr::merge($test, $test));
  }


  /**
   * Test Merge Empty
   *
   * Tests merging with an empty array.
   * @return void
   */
  public function testMergeEmpty( ) : void {
    $test = ['first'];
    $this->assertSame(['first'], Arr::merge([ ], $test));
    $this->assertSame(['first'], Arr::merge($test, [ ]));
  }


  /**
   * Test Pad Preserves Original
   *
   * Tests the pad function preserves the original.
   * @return void
   */
  public function testPadPreserveOriginal( ) : void {
    $test = ['test'];
    Arr::pad($test, 2);
    $this->assertSame(['test'], $test);
  }


  /**
   * Test Pad Positive Length
   *
   * Tests pad with a positive length.
   * @return void
   */
  public function testPadPositiveLength( ) : void {
    $test = ['test'];
    $this->assertSame(['test', null], Arr::pad($test, 2));
  }


  /**
   * Test Pad Negative Length
   *
   * Tests pad with a negative length.
   * @return void
   */
  public function testPadNegativeLength( ) : void {
    $test = ['test'];
    $this->assertSame([null, 'test'], Arr::pad($test, -2));
  }


  /**
   * Test Pad Already Long
   *
   * Tests padding when the array is already that long.
   * @return void
   */
  public function testPadAlreadyLong( ) : void {
    $test = ['test', 'test'];
    $this->assertSame(['test', 'test'], Arr::pad($test, 2));
    $this->assertSame(['test', 'test'], Arr::pad($test, 1));
  }


  /**
   * Test Pad Custom Value
   *
   * Tests padding with a custom value.
   * @return void
   */
  public function testPadCustomValue( ) : void {
    $test = ['test', 'test'];
    $this->assertSame(['test', 'test', 'default'], Arr::pad($test, 3, 'default'));
  }


  /**
   * Test Pad Empty
   *
   * Tests padding an empty array.
   * @return void
   */
  public function testPadEmpty( ) : void {
    $test = [ ];
    $this->assertSame([null, null], Arr::pad($test, 2));
  }


  /**
   * Test Pop Mutate Original
   *
   * Tests pop mutates the original array.
   * @return void
   */
  public function testPopMutateOriginal( ) : void {
    $test = ['first', 'second'];
    Arr::pop($test);
    $this->assertSame(['first'], $test);
  }


  /**
   * Test Pop Correct
   *
   * Tests a correct pop.
   * @return void
   */
  public function testPopCorrect( ) : void {
    $test = ['first', 'second'];
    $this->assertSame('second', Arr::pop($test));
  }


  /**
   * Test Pop Empty
   *
   * Tests popping an empty array.
   * @return void
   */
  public function testPopEmpty( ) : void {
    $test = [ ];
    $this->assertNull(Arr::pop($test));
  }


  /**
   * Test Pop Default
   *
   * Tests popping and retrieving the default value.
   * @return void
   */
  public function testPopDefault( ) : void {
    $test = [ ];
    $this->assertSame('default', Arr::pop($test, 'default'));
  }


  /**
   * Test Pull Mutate Original
   *
   * Tests pull will mutate the original.
   * @return void
   */
  public function testPullMutateOriginal( ) : void {
    $test = ['one', 'two', 'three'];
    Arr::pull($test, 1);
    $this->assertSame($test, ['one']);
  }


  /**
   * Test Pull Indexed
   *
   * Test pull on an indexed array.
   * @return void
   */
  public function testPullIndexed( ) : void {
    $test = ['one', 'two', 'three'];
    $this->assertSame(['two', 'three'], Arr::pull($test, 1));
  }


  /**
   * Test Pull Associative
   *
   * Test pull on an associative array.
   * @return void
   */
  public function testPullAssociative( ) : void {
    $test = ['first' => 'one', 'second' => 'two', 'third' => 'three'];
    $this->assertSame(['second' => 'two', 'third' => 'three'], Arr::pull($test, 1));
  }


  /**
   * Test Pull By Key
   *
   * Test pulling by key.
   * @return void
   */
  public function testPullByKey( ) : void {
    $test = ['first' => 'one', 'second' => 'two', 'third' => 'three'];
    $this->assertSame(['second' => 'two', 'third' => 'three'], Arr::pull($test, 'second'));
  }


  /**
   * Test Pull Index Not Found
   *
   * Tests pulling when the index is not found.
   * @return void
   */
  public function testPullIndexNotFound( ) : void {
    $test = ['one', 'two', 'three'];
    $this->assertSame([ ], Arr::pull($test, 100));

    $test = ['first' => 'one', 'second' => 'two', 'third' => 'three'];
    $this->assertSame([ ], Arr::pull($test, 'fourth'));
  }


  /**
   * Test Pull Specified Length
   *
   * Tests pulling a specified length
   * @return void
   */
  public function testPullSpecifiedLength( ) : void {
    $test = ['one', 'two', 'three'];
    $this->assertSame(['two'], Arr::pull($test, 1, 1));

    $test = ['first' => 'one', 'second' => 'two', 'third' => 'three'];
    $this->assertSame(['second' => 'two'], Arr::pull($test, 1, 1));

    $test = ['first' => 'one', 'second' => 'two', 'third' => 'three'];
    $this->assertSame(['second' => 'two'], Arr::pull($test, 'second', 1));
  }


  /**
   * Test Pull Negative Index
   *
   * Tests pulling with a negative index.
   * @return void
   */
  public function testPullNegativeIndex( ) : void {
    $test = ['one', 'two', 'three'];
    $this->assertSame(['two', 'three'], Arr::pull($test, -2));

    $test = ['first' => 'one', 'second' => 'two', 'third' => 'three'];
    $this->assertSame(['second' => 'two', 'third' => 'three'], Arr::pull($test, -2));
  }


  /**
   * Test Pull Negative Length
   *
   * Test pulling with a negative length.
   * @return void
   */
  public function testPullNegativeLength( ) : void {
    $test = ['one', 'two', 'three'];
    $this->assertSame(['two'], Arr::pull($test, 1, -1));

    $test = ['first' => 'one', 'second' => 'two', 'third' => 'three'];
    $this->assertSame(['second' => 'two'], Arr::pull($test, 1, -1));

    $test = ['first' => 'one', 'second' => 'two', 'third' => 'three'];
    $this->assertSame(['second' => 'two'], Arr::pull($test, 'second', -1));
  }


  /**
   * Test Pull Empty
   *
   * Tests pulling an empty array.
   * @return void
   */
  public function testPullEmpty( ) : void {
    $test = [ ];
    $this->assertSame([ ], Arr::pull($test, 0));
  }


  /**
   * Test Pull Over Length
   *
   * Test pulling over the length.
   * @return void
   */
  public function testPullOverLength( ) : void {
    $test = ['one', 'two', 'three'];
    $this->assertSame(['two', 'three'], Arr::pull($test, 1, 100));
  }

  
  /**
   * Test Push Preserve Original
   *
   * Test that push preserves the original.
   * @return void
   */
  public function testPushPreserveOriginal( ) : void {
    $test = ['first'];
    Arr::push($test, 'second');
    $this->assertSame(['first'], $test);
  }


  /**
   * Test Push Indexed
   *
   * Tests pushing onto an indexed array.
   * @return void
   */
  public function testPushIndexed( ) : void {
    $test = ['one', 'two'];
    $this->assertSame(['one', 'two', 'three'], Arr::push($test, 'three'));
  }


  /**
   * Test Push Associative
   *
   * Test pushing onto an associative array.
   * @return void
   */
  public function testPushAssociative( ) : void {
    $test = ['first' => 'one', 'second' => 'two'];
    $this->assertSame(['first' => 'one', 'second' => 'two', 0 => 'three'], Arr::push($test, 'three'));
  }


  /**
   * Test Push Empty
   *
   * Test pushing onto an empty array.
   * @return void
   */
  public function testPushEmpty( ) : void {
    $test = [ ];
    $this->assertSame(['one'], Arr::push($test, 'one'));
  }


  /**
   * Test Reduce Preserve Original
   *
   * Test reduce preserves the original.
   * @return void
   */
  public function testReducePreserveOriginal( ) : void {
    $test = ['test'];
    Arr::reduce($test, function($carry, $next) { return "{$carry}{$next}"; });
    $this->assertSame(['test'], $test);
  }


  /**
   * Test Reduce Default
   *
   * Test reduce with a correct set.
   * @return void
   */
  public function testReduceDefault( ) : void {
    $test = ['one', 'two'];
    $this->assertSame('onetwo', Arr::reduce($test, function($carry, $next) { return "{$carry}{$next}"; }));

    $mock   = $this->getMockBuilder(stdClass::class)->setMethods(['__invoke'])->getMock( );
    $mock->expects($this->any( ))->method('__invoke');
    Arr::reduce($test, $mock);
  }


  /**
   * Test Reduce Incorrect Args
   *
   * Tests reduce with incorrect number of args to the callback.
   * @return void
   */
  public function testReduceIncorrectArgs( ) : void {
    $test = ['first', 'second'];

    $this->expectException(ArgumentCountError::class);
    $method = function($a, $b, $c) { return 1; };
    Arr::reduce($test, $method);

    $method = function( ) { return 1; };
    Arr::reduce($test, $method);
  }


  /**
   * Test Reduce Initial
   *
   * Test reduce with an initial value.
   * @return void
   */
  public function testReduceInitial( ) : void {
    $test = ['one', 'two'];
    $this->assertSame('zeroonetwo', Arr::reduce($test, function($carry, $next) { return "{$carry}{$next}"; }, "zero"));
  }


  /**
   * Test Replace Preserve Original
   *
   * Tests replace preserves the original.
   * @return void
   */
  public function testReplacePreserveOriginal( ) : void {
    $test = ['test'];
    Arr::replace($test, ['new'], 0, 1);
    $this->assertSame(['test'], $test);
  }


  /**
   * Test Replace Both Positive
   *
   * Test replace with length and start positive.
   * @return void
   */
  public function testReplaceBothPositive( ) : void {
    $test = ['one', 'two', 'three', 'four'];
    $this->assertSame(['one', 'second', 'third', 'four'], Arr::replace($test, ['second', 'third'], 1, 2));
  }


  /**
   * Test Replace Start Negative
   *
   * Test replace with a negative start
   * @return void
   */
  public function testReplaceStartNegative( ) : void {
    $test = ['one', 'two', 'three', 'four'];
    $this->assertSame(['one', 'two', 'third', 'fourth'], Arr::replace($test, ['third', 'fourth'], -2, 2));
  }


  /**
   * Test Replace Length Negative
   * @return void
   */
  public function testReplaceLengthNegative( ) : void {
    $test = ['one', 'two', 'three', 'four'];
    $this->assertSame(['one', 'second', 'third', 'four'], Arr::replace($test, ['second', 'third'], 1, -1));
  }


  /**
   * Test Replace Both Negative
   *
   * Tests replace when both start and length are negative.
   * @return void
   */
  public function testReplaceBothNegative( ) : void {
    $test = ['one', 'two', 'three', 'four'];
    $this->assertSame(['one', 'second', 'third', 'four'], Arr::replace($test, ['second', 'third'], -3, -1));
  }


  /**
   * Test Replace Associative
   *
   * Test replace on an associative array.
   * @return void
   */
  public function testReplaceAssociative( ) : void {
    $test = ['first' => 'one', 'second' => 'two'];
    $this->assertSame(['first' => 'one', 'replaced'], Arr::replace($test, ['replaced'], 1, 1));
  }


  /**
   * Test Replace String Key
   *
   * Test replace with a string key.
   * @return void
   */
  public function testReplaceStringKey( ) : void {
    $test = ['first' => 'one', 'second' => 'two'];
    $this->assertSame(['first' => 'one', 'replaced'], Arr::replace($test, ['replaced'], 'second', 1));
  }


  /**
   * Test Replace Mismatched Lengths
   *
   * Test replace with mismatched length of replacement to replacement
   * array.
   * @return void
   */
  public function testReplaceMismatchedLengths( ) : void {
    $test = ['one', 'two', 'three', 'four'];
    $this->assertSame(['one', 'second', 'four'], Arr::replace($test, ['second'], 1, 2));
  }

  /**
   * Test Reverse Preserve Original
   *
   * Tests reverse preserves the original.
   * @return void
   */
  public function testReversePreserveOriginal( ) : void {
    $test = ['one', 'two'];
    Arr::reverse($test);
    $this->assertSame(['one', 'two'], $test);
  }


  /**
   * Test Reverse Correct
   *
   * Tests reverse functions correctly.
   * @return void
   */
  public function testReverseCorrect( ) : void {
    $test = ['one', 'two'];
    $this->assertSame(['two', 'one'], Arr::reverse($test));
  }


  /**
   * Test Reverse Empty
   *
   * Tests reverse on an empty array.
   * @return void
   */
  public function testReverseEmpty( ) : void {
    $test = [ ];
    $this->assertSame([ ], Arr::reverse($test));
  }


  /**
   * Test Search Preserve Original
   *
   * Tests the search method preserves the original.
   * @return void
   */
  public function testSearchPreserveOriginal( ) : void {
    $test = ['first' => 'one'];
    Arr::search($test, 'one');
    $this->assertSame($test, ['first' => 'one']);
  }


  /**
   * Test Search Correctly
   *
   * Tests the search function retrieves the key.
   * @return void
   */
  public function testSearchCorrectly( ) : void {
    $test = ['first' => 'one', 'second' => 'two'];
    $this->assertSame('second', Arr::search($test, 'two'));
  }


  /**
   * Test Search Missing
   *
   * Tests searching for a value that does not exist.
   * @return void
   */
  public function testSearchMissing( ) : void {
    $test = ['first' => 'one', 'second' => 'two'];
    $this->assertNull(Arr::search($test, 'third'));
  }


  /**
   * Test Search Default
   *
   * Tests retrieving the default when the item is not found.
   * @return void
   */
  public function testSearchDefault( ) : void {
    $test = ['first' => 'one', 'second' => 'two'];
    $this->assertSame('default', Arr::search($test, 'third', 'default'));
  }


  /**
   * Test Search Strict
   *
   * Tests searching using strict values.
   * @return void
   */
  public function testSearchStrict( ) : void {
    $test = [0, false, '0'];
    $this->assertSame(2, Arr::search($test, '0', null, true));
  }


  /**
   * Test Search Multiple
   *
   * Tests search locates the first item when multiple exist.
   * @return void
   */
  public function testSearchMultiple( ) : void {
    $test = ['one', 'two', 'two', 'two', 'three'];
    $this->assertSame(1, Arr::search($test, 'two'));
  }


  /**
   * Test Section Preserve Original
   *
   * Tests section preserves the original.
   * @return void
   */
  public function testSectionPreserveOriginal( ) : void {
    $test = ['one', 'two', 'three', 'four'];
    Arr::section($test, 4);
    $this->assertSame(['one', 'two', 'three', 'four'], $test);
  }

  /**
   * Test Section Evenly
   *
   * Tests section into even amounts.
   * @return void
   */
  public function testSectionEvenly( ) : void {
    $test = ['one', 'two', 'three', 'four', 'five', 'six'];
    $this->assertSame([['one', 'two', 'three'], ['four', 'five', 'six']], Arr::section($test, 2));
  }


  /**
   * Test Section Unevenly
   *
   * Tests sectioning into uneven amounts.
   * @return void
   */
  public function testSectionUnevenly( ) : void {
    $test = ['one', 'two', 'three', 'four', 'five'];
    $this->assertSame([['one', 'two'], ['three', 'four'], ['five']], Arr::section($test, 3));
    $this->assertSame([['one', 'two'], ['three'], ['four'], ['five']], Arr::section($test, 4));
  }


  /**
   * Test Section Negative Sections
   *
   * Test section with negative number of sections.
   * @return void
   */
  public function testSectionNegativeSections( ) : void {
    $test = ['one', 'two', 'three', 'four'];
    $this->assertSame([['one', 'two'], ['three', 'four']], Arr::section($test, -2));
    $this->assertSame([['one'], ['two'], ['three', 'four']], Arr::section($test, -3));
  }


  /**
   * Test Section Zero Sections
   *
   * Tests section with 0 sections.
   * @return void
   */
  public function testSectionZeroSections( ) : void {
    $test = ['one', 'two'];
    $this->assertSame([ ], Arr::section($test, 0));
  }


  /**
   * Test Section Empty
   *
   * Tests sectioning an empty array.
   * @return void
   */
  public function testSectionEmpty( ) : void {
    $test = [ ];
    $this->assertSame([[ ]], Arr::section($test, 1));
    $this->assertSame([[ ], [ ]], Arr::section($test, 2));
  }


  /**
   * Test Section More Than Length
   *
   * Tests when sectioning into more sections than elements.
   * @return void
   */
  public function testSectionMoreThanLength( ) : void {
    $test = ['one', 'two'];
    $this->assertSame([['one'], ['two'], [ ]], Arr::section($test, 3));
  }


  /**
   * Test Set Preserve Original
   *
   * Tests that set does not mutate the original array.
   * @return void
   */
  public function testSetPreserveOriginal( ) : void {
    $test = ['first' => 'one'];
    Arr::set($test, 'second', 'two');
    $this->assertSame(['first' => 'one'], $test);
  }


  /**
   * Test Set Correctly
   *
   * Tests that the value is set correctly.
   * @return void
   */
  public function testSetCorrectly( ) : void {
    $test = ['first' => 'one'];
    $this->assertSame(['first' => 'one', 'second' => 'two'], Arr::set($test, 'second', 'two'));
  }


  /**
   * Test Set Nested
   *
   * Test setting a nested value.
   * @return void
   */
  public function testSetNested( ) : void {
    $test = ['first' => ['second' => 'two']];
    $this->assertSame(['first' => ['second' => 'two', 'third' => 'three']], Arr::set($test, 'first.third', 'three'));

    $test = ['first' => 'one'];
    $this->assertSame(['first' => 'one', 'fourth' => ['fifth' => 'five']], Arr::set($test, 'fourth.fifth', 'five'));
  }


  /**
   * Test Set Overwrite
   *
   * Tests overwriting a value.
   * @return void
   */
  public function testSetOverwrite( ) : void {
    $test = ['first' => 'one'];
    $this->assertSame(['first' => 'two'], Arr::set($test, 'first', 'two'));

    $test = ['first' => ['second' => 'two']];
    $this->assertSame(['first' => ['second' => 'three']], Arr::set($test, 'first.second', 'three'));
  }


  /**
   * Test Set Empty
   *
   * Tests setting on an empty array.
   * @return void
   */
  public function testSetEmpty( ) : void {
    $test = [ ];
    $this->assertSame(['first' => 'one'], Arr::set($test, 'first', 'one'));

    $test = [ ];
    $this->assertSame(['first' => ['second' => 'one']], Arr::set($test, 'first.second', 'one'));
  }


  /**
   * Test Shift Mutates Original
   *
   * Test that shift mutates the original array
   * @return void
   */
  public function testShiftMutatesOriginal( ) : void {
    $test = ['first', 'second'];
    Arr::shift($test);
    $this->assertSame(['second'], $test);
  }


  /**
   * Test Shift Indexed
   *
   * Tests shift with an indexed array.
   * @return void
   */
  public function testShiftIndexed( ) : void {
    $test = ['first', 'second'];
    $this->assertSame('first', Arr::shift($test));
    $this->assertSame([0 => 'second'], $test);
  }


  /**
   * Test Shift Associative
   *
   * Tests shifting an associative array.
   * @return void
   */
  public function testShiftAssociative( ) : void {
    $test = ['first' => 'one', 'second' => 'two'];
    $this->assertSame('one', Arr::shift($test));
    $this->assertSame(['second' => 'two'], $test);
  }


  /**
   * Test Shift Empty
   *
   * Tests shifting an empty array
   * @return void
   */
  public function testShiftEmpty( ) : void {
    $test = [ ];
    $this->assertNull(Arr::shift($test));
    $this->assertSame([ ], $test);
  }


  /**
   * Test Shift Default
   *
   * Tests shifting with a default value
   * @return void
   */
  public function testShiftDefault( ) : void {
    $test = [ ];
    $this->assertSame('default', Arr::shift($test, 'default'));
  }


  /**
   * Test Slice Preserve Original
   *
   * Test that slice preserves the original array.
   * @return void
   */
  public function testSlicePreserveOriginal( ) : void {
    $test = ['test'];
    Arr::slice($test, 1, 1);
    $this->assertSame(['test'], $test);
  }


  /**
   * Test Slice Indexed Array
   *
   * Test slice on an indexed array.
   * @return void
   */
  public function testSliceIndexedArray( ) : void {
    $test = ['first', 'second', 'third', 'fourth'];
    $this->assertSame(['second', 'third'], Arr::slice($test, 1, 2));
    $this->assertSame(['second', 'third', 'fourth'], Arr::slice($test, 1));
  }


  /**
   * Test Slice Associative Array
   *
   * Test slicing an associative array.
   * @return void
   */
  public function testSliceAssociativeArray( ) : void {
    $test = ['first' => 'one', 'second' => 'two', 'third' => 'three', 'fourth' => 'four'];
    $this->assertSame(['second' => 'two', 'third' => 'three'], Arr::slice($test, 1, 2));
    $this->assertSame(['second' => 'two', 'third' => 'three', 'fourth' => 'four'], Arr::slice($test, 1));
  }


  /**
   * Test Slice By Key
   *
   * Tests slicing by a named key.
   * @return void
   */
  public function testSliceByKey( ) : void {
    $test = ['first' => 'one', 'second' => 'two', 'third' => 'three', 'fourth' => 'four'];
    $this->assertSame(['second' => 'two', 'third' => 'three'], Arr::slice($test, 'second', 2));
    $this->assertSame(['second' => 'two', 'third' => 'three', 'fourth' => 'four'], Arr::slice($test, 'second'));
  }


  /**
   * Test Slice Not Found
   *
   * Test when a slice is not found.
   * @return void
   */
  public function testSliceNotFound( ) : void {
    $test = ['first' => 'one', 'second' => 'two', 'third' => 'three', 'fourth' => 'four'];
    $this->assertSame([ ], Arr::slice($test, 'fifth'));
    $this->assertSame([ ], Arr::slice($test, 5));
  }


  /**
   * Test Slice Empty
   *
   * Test slicing an empty array.
   * @return void
   */
  public function testSliceEmpty( ) : void {
    $test = [ ];
    $this->assertSame([ ], Arr::slice($test, 'second'));
    $this->assertSame([ ], Arr::slice($test, 2));
  }


  /**
   * Test Slice Negative Start
   * @return void
   */
  public function testSliceNegativeStart( ) : void {
    $test = ['one', 'two', 'three', 'four'];
    $this->assertSame(['two', 'three'], Arr::slice($test, -3, 2));
    $this->assertSame(['two', 'three', 'four'], Arr::slice($test, -3));
  }


  /**
   * Test Slice Negative Length
   *
   * Test slicing with a negative length.
   * @return void
   */
  public function testSliceNegativeLength( ) : void {
    $test = ['first' => 'one', 'second' => 'two', 'third' => 'three', 'fourth' => 'four'];
    $this->assertSame(['second' => 'two', 'third' => 'three'], Arr::slice($test, 1, -1));
    $this->assertSame(['second' => 'two', 'third' => 'three'], Arr::slice($test, 'second', -1));
  
  }


  /**
   * Test Slice Over Length
   *
   * Test slicing for longer than the array.
   * @return void
   */
  public function testSliceOverLength( ) : void {
    $test = ['first' => 'one', 'second' => 'two', 'third' => 'three'];
    $this->assertSame(['second' => 'two', 'third' => 'three'], Arr::slice($test, 1, 100));
    $this->assertSame(['second' => 'two', 'third' => 'three'], Arr::slice($test, 'second', 100));
  }


  /**
   * Test Sort Preserve Original
   *
   * Tests the sort function preserves the original.
   * @return void
   */
  public function testSortPreserveOriginal( ) : void {
    $test = [5, 4, 3, 2, 1];
    Arr::sort($test);
    $this->assertSame([5, 4, 3, 2, 1], $test);
  }


  /**
   * Test Sort Default
   *
   * Tests the default sort.
   * @return void
   */
  public function testSortDefault( ) : void {
    $test = [5, 4, 3, 2, 1];
    $this->assertSame([1, 2, 3, 4, 5], Arr::sort($test));

    $test = ['q', 'z', 'a'];
    $this->assertSame(['a', 'q', 'z'], Arr::sort($test));
  }


  /**
   * Test Sort Custom
   *
   * Test sorting with a custom callback.
   * @return void
   */
  public function testSortCustom( ) : void {
    $test   = [1, 5, 3];
    $method = function($a, $b) { return $b <=> $a; };
    
    $this->assertSame([5, 3, 1], Arr::sort($test, $method));

    $mock   = $this->getMockBuilder(stdClass::class)->setMethods(['__invoke'])->getMock( );
    $mock->expects($this->any( ))->method('__invoke');
    Arr::sort($test, $mock);
  }


  /**
   * Test Sort Empty
   *
   * Tests sorting an empty array.
   * @return void
   */
  public function testSortEmpty( ) : void {
    $test = [ ];
    $this->assertSame([ ], Arr::sort($test));
  }


  /**
   * Test Sum Preserve Original
   *
   * Tests the sum method preserves the original.
   * @return void
   */
  public function testSumPreserveOriginal( ) : void {
    $test = ['test'];
    Arr::sum($test);
    $this->assertSame($test, ['test']);
  }


  /**
   * Test Sum Numeric
   *
   * Tests the sum method on a numeric array.
   * @return void
   */
  public function testSumNumeric( ) : void {
    $test = [1, 2, 4];
    $this->assertSame(7, Arr::sum($test));

    $test = [1.1, 2.5, 3.7];
    $this->assertSame(7.3, Arr::sum($test));
  }


  /**
   * Test Sum Strings
   *
   * Tests sum on string data.
   * @return void
   */
  public function testSumStrings( ) : void {
    $test = ['one', 'two', 'three'];
    $this->assertSame(0, Arr::sum($test));

    $test = ['a', 'b', 'c'];
    $this->assertSame(0, Arr::sum($test));
  }


  /**
   * Test Sum Arrays
   *
   * Tests the sum of arrays.
   * @return void
   */
  public function testSumArrays( ) : void {
    $test = [['one'], ['two'], ['three']];
    $this->assertSame(0, Arr::sum($test));
  }


  /**
   * Test Sum Booleans
   *
   * Tests the sum of booleans.
   * @return void
   */
  public function testSumBooleans( ) : void {
    $test = [false, false];
    $this->assertSame(0, Arr::sum($test));

    $test = [false, true];
    $this->assertSame(1, Arr::sum($test));

    $test = [true, true];
    $this->assertSame(2, Arr::sum($test));
  }


  /**
   * Test Sum Mixed
   *
   * Test sum of a mix of data types.
   * @return void
   */
  public function testSumMixed( ) : void {
    $test = ['one', 1, false];
    $this->assertSame(1, Arr::sum($test));
  }


  /**
   * Test Sum Empty
   *
   * Tests sum on an empty array.
   * @return void
   */
  public function testSumEmpty( ) : void {
    $test = [ ];
    $this->assertSame(0, Arr::sum($test));
  }


  /**
   * Test Take Mutate Original
   *
   * Tests the take function mutates the original array.
   * @return void
   */
  public function testTakeMutateOriginal( ) : void {
    $test = ['first' => 'one', 'second' => 'two'];
    Arr::take($test, 'first');
    $this->assertSame(['second' => 'two'], $test);
  }


  /**
   * Test Take Retrieves Item
   *
   * Tests the take function retrieves the correct item.
   * @return void
   */
  public function testTakeRetrievesItem( ) : void {
    $test = ['first' => 'one', 'second' => 'two'];
    $this->assertSame('one', Arr::take($test, 'first'));
  }


  /**
   * Test Take Handles Missing
   *
   * Tests the take function handles missing items.
   * @return void
   */
  public function testTakeHandlesMissing( ) : void {
    $test = ['first' => 'one', 'second' => 'two'];
    $this->assertNull(Arr::take($test, 'third'));
  }


  /**
   * Test Take Provides Default
   *
   * Tests take correctly provides a default.
   * @return void
   */
  public function testTakeProvidesDefault( ) : void  {
    $test = ['first' => 'one', 'second' => 'two'];
    $this->assertSame('three', Arr::take($test, 'third', 'three'));
  }


  /**
   * Test Take Indexed
   *
   * Test the take method with an indexed array.
   * @return void
   */
  public function testTakeIndexed( ) : void {
    $test = ['first', 'second', 'third'];
    $this->assertSame('second', Arr::take($test, 1));
    $this->assertSame([0 => 'first', 1 => 'third'], $test);
  }


  /**
   * Test Take Empty
   *
   * Tests the take method functions correctly with an empty array.
   * @return void
   */
  public function testTakeEmpty( ) : void {
    $test = [ ];
    $this->assertNull(Arr::take($test, 1));
    $this->assertSame([ ], $test);
  }


  /**
   * Test Unique Preserve Original
   *
   * Tests the unique function preserves the original array.
   * @return void
   */
  public function testUniquePreserveOriginal( ) : void {
    $test = ['test', 'test'];
    Arr::unique($test);
    $this->assertSame(['test', 'test'], $test);
  }


  /**
   * Test Unique Correct
   *
   * Tests the unique function operates correctly.
   * @return void
   */
  public function testUniqueCorrect( ) : void {
    $test = ['test', 'test'];
    $this->assertSame(['test'], Arr::unique($test));
  }


  /**
   * Test Unique Value Types
   *
   * Tests the unique function correctly handles at value types.
   * @return void
   */
  public function testUniqueValueTypes( ) : void {
    $test = ['1', 1];
    $this->assertSame(['1'], Arr::unique($test));
  }


  /**
   * Test Unshift Preserve Original
   *
   * Tests unshift preserves the original array.
   * @return void
   */
  public function testUnshiftPreserveOriginal( ) : void {
    $test = ['second'];
    Arr::unshift($test, 'first');
    $this->assertSame(['second'], $test);
  }


  /**
   * Test Unshift Correct
   *
   * Tests the unshift function appends correctly
   * @return void
   */
  public function testUnshiftCorrect( ) : void {
    $test = ['second'];
    $this->assertSame(['first', 'second'], Arr::unshift($test, 'first'));
  }


  /**
   * Test Unshift Empty
   *
   * Tests unshift on an empty array.
   * @return void
   */
  public function testUnshiftEmpty( ) : void {
    $test = [ ];
    $this->assertSame(['first'], Arr::unshift($test, 'first'));
  }


  /**
   * Test Values Preserve Original
   *
   * Tests that values preserves the original array.
   * @return void
   */
  public function testValuesPreserveOriginal( ) : void {
    $test = ['first' => 'one'];
    Arr::values($test);
    $this->assertSame(['first' => 'one'], $test);
  }


  /**
   * Test Values Flat
   *
   * Tests the values function works as expected.
   * @return void
   */
  public function testValuesFlat( ) : void {
    $test = ['first' => 'one'];
    $this->assertSame(['one'], Arr::values($test));
  }


  /**
   * Test Values Nested
   *
   * Tests values functions correctly with nested arrays.
   * @return void
   */
  public function testValuesNested( ) : void {
    $test = ['first' => ['inner' => 'one']];
    $this->assertSame([['inner' => 'one']], Arr::values($test));
  }


  /**
   * Test Values Empty
   *
   * Tests the values function with an empty array.
   * @return void
   */
  public function testValuesEmpty( ) : void {
    $test = [ ];
    $this->assertSame([ ], Arr::values($test));
  }
}