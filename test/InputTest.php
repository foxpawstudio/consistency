<?php

declare(strict_types=1);

use Foxpaw\Consistency\Input;
use PHPUnit\Framework\TestCase;


/**
 * Input Test
 *
 * Tests the Input class.
 */
final class InputTest extends TestCase {

  /** 
   * Set Up
   *
   * Set up for each test. Adds data to get, post and http server vars to
   * emulate an HTTP request to test input retrieval.
   * @return void
   */
  public function setUp( ) : void {
    $_GET  = ['first' => 'one', 'second' => 'two'];
    $_POST = ['first' => 'one', 'second' => 'two'];
    $_SERVER['HTTP_FIRST']  = 'one';
    $_SERVER['HTTP_SECOND'] = 'two';
    $_SERVER['first']  = 'one';
    $_SERVER['second'] = 'two';
  }


  /**
   * Tear Down
   *
   * Tears down the input changes used when running the tests.
   * @return void
   */
  public function tearDown( ) : void {
    $_GET  = [ ];
    $_POST = [ ];
    unset($_SERVER['HTTP_FIRST']);
    unset($_SERVER['HTTP_SECOND']);
    unset($_SERVER['first']);
    unset($_SERVER['second']);
  }


  /**
   * Test Get All
   *
   * Tests getting all get variables.
   * @return void
   */
  public function testGetAll( ) : void {
    $this->assertSame(['first' => 'one', 'second' => 'two'], Input::get( ));
  }


  /**
   * Test Get Single
   *
   * Test retrieving a single get input.
   * @return void
   */
  public function testGetSingle( ) : void {
    $this->assertSame('one', Input::get('first'));
  }


  /**
   * Test Get Missing
   *
   * Tests retrieving a missing get input.
   * @return void
   */
  public function testGetMissing( ) : void {
    $this->assertNull(Input::get('third'));
    $this->assertNull(Input::get('FIRST'));
  }


  /**
   * Test Get Defautl
   *
   * Tests retrieving the default.
   * @return void
   */
  public function testGetDefault( ) : void {
    $this->assertSame('default', Input::get('third', 'default'));
  }


  /**
   * Test Header All
   *
   * Tests getting all header variables.
   * @return void
   */
  public function testHeaderAll( ) : void {
    $this->assertSame(['First' => 'one', 'Second' => 'two'], Input::header( ));
  }


  /**
   * Test Header Single
   *
   * Test retrieving a single header input.
   * @return void
   */
  public function testHeaderSingle( ) : void {
    $this->assertSame('one', Input::header('First'));
  }


  /**
   * Test Header Missing
   *
   * Tests retrieving a missing header input.
   * @return void
   */
  public function testHeaderMissing( ) : void {
    $this->assertNull(Input::header('third'));
    $this->assertNull(Input::header('FIRST'));
  }


  /**
   * Test Header Defautl
   *
   * Tests retrieving the default.
   * @return void
   */
  public function testHeaderDefault( ) : void {
    $this->assertSame('default', Input::header('third', 'default'));
  }


  /**
   * Test Post All
   *
   * Tests getting all post variables.
   * @return void
   */
  public function testPostAll( ) : void {
    $this->assertSame(['first' => 'one', 'second' => 'two'], Input::post( ));
  }


  /**
   * Test Post Single
   *
   * Test retrieving a single post input.
   * @return void
   */
  public function testPostSingle( ) : void {
    $this->assertSame('one', Input::post('first'));
  }


  /**
   * Test Post Missing
   *
   * Tests retrieving a missing post input.
   * @return void
   */
  public function testPostMissing( ) : void {
    $this->assertNull(Input::post('third'));
    $this->assertNull(Input::post('FIRST'));
  }


  /**
   * Test Post Defautl
   *
   * Tests retrieving the default.
   * @return void
   */
  public function testPostDefault( ) : void {
    $this->assertSame('default', Input::post('third', 'default'));
  }


  /**
   * Test Server All
   *
   * Tests getting all server variables.
   * @return void
   */
  public function testServerAll( ) : void {
    $this->assertSame($_SERVER, Input::server( ));
  }


  /**
   * Test Server Single
   *
   * Test retrieving a single server input.
   * @return void
   */
  public function testServerSingle( ) : void {
    $this->assertSame('one', Input::server('first'));
  }


  /**
   * Test Server Missing
   *
   * Tests retrieving a missing server input.
   * @return void
   */
  public function testServerMissing( ) : void {
    $this->assertNull(Input::server('third'));
    $this->assertNull(Input::server('FIRST'));
  }


  /**
   * Test Server Defautl
   *
   * Tests retrieving the default.
   * @return void
   */
  public function testServerDefault( ) : void {
    $this->assertSame('default', Input::server('third', 'default'));
  }
}