<?php

declare(strict_types=1);

use Foxpaw\Consistency\Num;
use PHPUnit\Framework\Error\Error;
use PHPUnit\Framework\Error\Warning;
use PHPUnit\Framework\TestCase;


/**
 * Num Test
 *
 * Tests the Num class.
 */
final class NumTest extends TestCase {

  /**
   * Test Abs Positive
   *
   * Test abs on a positive number.
   * @return void
   */
  public function testAbsPositive( ) : void {
    $this->assertSame(3.0, Num::abs(3));
  }


  /**
   * Test Abs Negative
   *
   * Test abs on a a negative number.
   * @return void
   */
  public function testAbsNegative( ) : void {
    $this->assertSame(3.0, Num::abs(-3));
  }


  /**
   * Test Abs Zero
   *
   * Test abs on zero.
   * @return void
   */
  public function testAbsZero( ) : void {
    $this->assertSame(0.0, Num::abs(0));
  }


  /**
   * Test Add
   *
   * Tests add.
   * @return void
   */
  public function testAdd( ) : void {
    $this->assertSame(3.0, Num::add(1.0, 2.0));
    $this->assertSame(1.0, Num::add(-1.0, 2.0));
    $this->assertSame(-1.0, Num::add(1.0, -2.0));
    $this->assertSame(-3.0, Num::add(-1.0, -2.0));
    $this->assertSame(2.0, Num::add(0.0, 2.0));
  }


  /**
   * Test Ceil Positive
   * 
   * Test ceil on a positive number.
   * @return void
   */
  public function testCeilPositive( ) : void {
    $this->assertSame(1.0, Num::ceil(0.7));
    $this->assertSame(1.0, Num::ceil(0.1));
  }

  /**
   * Test Ceil Negative
   *
   * Test ceil on a negative number.
   * @return void
   */
  public function testCeilNegative( ) : void {
    $this->assertSame(-1.0, Num::ceil(-1.7));
    $this->assertSame(-1.0, Num::ceil(-1.1));
  }


  /**
   * Test Ceil Zero
   *
   * Test ceil on zero.
   * @return void
   */
  public function testCeilZero( ) : void {
    $this->assertSame(0.0, Num::ceil(0));
    $this->assertSame(0.0, Num::ceil(-0));
  }


  /**
   * Test Divide Correct
   *
   * Test a correct divide.
   * @return void
   */
  public function testDivideCorrect( ) : void {
    $this->assertSame(2.0, Num::divide(4, 2));
    $this->assertSame(-2.0, Num::divide(4, -2));
    $this->assertSame(-2.0, Num::divide(-4, 2));
    $this->assertSame(2.0, Num::divide(-4, -2));
  }


  /**
   * Test Divide Zero
   *
   * Test dividing with zeros.
   * @return void
   */
  public function testDivideZero( ) : void {
    $this->assertSame(0.0, Num::divide(0, 4));

    $this->expectException(Warning::class);
    Num::divide(4, 0);

    $this->expectException(Warning::class);
    Num::divide(0, 0);
  }


  /**
   * Test Floor Positive
   * 
   * Test floor on a positive number.
   * @return void
   */
  public function testFloorPositive( ) : void {
    $this->assertSame(1.0, Num::floor(1.7));
    $this->assertSame(1.0, Num::floor(1.1));
  }

  /**
   * Test Floor Negative
   *
   * Test floor on a negative number.
   * @return void
   */
  public function testFloorNegative( ) : void {
    $this->assertSame(-1.0, Num::floor(-0.7));
    $this->assertSame(-1.0, Num::floor(-0.1));
  }


  /**
   * Test Floor Zero
   *
   * Test floor on zero.
   * @return void
   */
  public function testFloorZero( ) : void {
    $this->assertSame(0.0, Num::floor(0));
    $this->assertSame(0.0, Num::floor(-0));
  }


  /**
   * Test Format Simple
   *
   * Test a simple format.
   * @return void
   */
  public function testFormatSimple( ) : void {
    $this->assertSame('5', Num::format(5));
    $this->assertSame('-5', Num::format(-5));
  }


  /**
   * Test Format Places
   *
   * Test format using places.
   * @return void
   */
  public function testFormatPlaces( ) : void {
    $this->assertSame('5.00', Num::format(5, 2));
    $this->assertSame('-5.00', Num::format(-5, 2));

  }


  /**
   * Test Format Decimal
   *
   * Test format with different decimal place separators.
   * @return void
   */
  public function testFormatDecimal( ) : void {
    $this->assertSame('5+00', Num::format(5, 2, '+'));
    $this->assertSame('-5+00', Num::format(-5, 2, '+'));
  }


  /**
   * Test Format Thousands
   *
   * Test formatting with the thousands separator.
   * @return void
   */
  public function testFormatThousands( ) : void {
    $this->assertSame('5 000.00', Num::format(5000, 2, '.', ' '));
    $this->assertSame('-5 000.00', Num::format(-5000, 2, '.', ' '));
  }


  /**
   * Test Zeros And Empties
   *
   * Test using zeros and empty strings.
   * @return void
   */
  public function testFormatZerosAndEmpties( ) : void {
    $this->assertSame('0', Num::format(0));
    $this->assertSame('0', Num::format(0, 0));
    $this->assertSame('000', Num::format(0, 2, ''));
    $this->assertSame('100000', Num::format(1000, 2, '', ''));
  }


  /**
   * Test Is Divisble By
   *
   * Tests the isDivisibleBy method.
   * @return void
   */
  public function testIsDivisbleBy( ) : void {
    $this->assertTrue(Num::isDivisibleBy(3, 3));
    $this->assertTrue(Num::isDivisibleBy(6, 3));
    $this->assertTrue(Num::isDivisibleBy(-6, 3));
    $this->assertTrue(Num::isDivisibleBy(6, -3));
    $this->assertTrue(Num::isDivisibleBy(-6, -3));
    $this->assertFalse(Num::isDivisibleBy(7, 3));
  }

  /**
   * Test Is Even
   *
   * Test the isEven method.
   * @return void
   */
  public function testIsEven( ) : void {
    $this->assertTrue(Num::isEven(4));
    $this->assertTrue(Num::isEven(2));
    $this->assertTrue(Num::isEven(-2));
    $this->assertFalse(Num::isEven(-3));
    $this->assertFalse(Num::isEven(0));
  }


  /**
   * Test Is Odd
   *
   * Test the isOdd method.
   * @return void
   */
  public function testIsOdd( ) : void {
    $this->assertTrue(Num::isOdd(3));
    $this->assertTrue(Num::isOdd(1));
    $this->assertTrue(Num::isOdd(-1));
    $this->assertFalse(Num::isOdd(4));
    $this->assertFalse(Num::isOdd(0));
  }


  /**
   * Test Log Simple
   *
   * Test a simple log.
   * @return void
   */
  public function testLogSimple( ) : void {
    $this->assertSame(1.0, Num::log(M_E));
    $this->assertSame(1.0, Num::log(M_E, M_E));
    $this->assertSame(0.0, Num::log(1, M_E));
    $this->assertSame(0.0, Num::log(1, 10));
    $this->assertSame(1.0, Num::log(10, 10));
    $this->assertSame(2.0, Num::log(100, 10));
  }


  /**
   * Test Log Errors
   *
   * Test logs of 0 and negatives.
   * @return void
   */
  public function testLogErrors( ) : void {
    $this->expectException(Error::class);
    Num::log(0);

    $this->expectException(Error::class);
    Num::log(-1);

    $this->expectException(Error::class);
    Num::log(10, -1);
  }


  /**
   * Test Mod Positive
   *
   * Test mod with positive numbers.
   * @return void
   */
  public function testModPositive( ) : void {
    $this->assertSame(1.0, Num::mod(5, 2));
    $this->assertSame(1.0, Num::mod(5.7, 2));
  }


  /**
   * Test Mod Negative
   * 
   * Test mod with negative numbers.
   * @return void
   */
  public function testModNegative( ) : void {
    $this->assertSame(-1.0, Num::mod(-5, 2));
    $this->assertSame(1.0, Num::mod(5, -2));
    $this->assertSame(-1.0, Num::mod(-5, -2));
  }


  /**
   * Test Mod Zero
   *
   * Tests mod with zeros.
   * @return void
   */
  public function testModZero( ) : void {
    $this->assertSame(0.0, Num::mod(0, 2));

    $this->expectException(DivisionByZeroError::class);
    $this->assertSame(1.0, Num::mod(2, 0));

    $this->expectException(DivisionByZeroError::class);
    $this->assertSame(1.0, Num::mod(0, 0));
  }


  /**
   * Test Multiply
   *
   * Tests the multiply function.
   * @return void
   */
  public function testMultiply( ) : void {
    $this->assertSame(2.0, Num::multiply(1.0, 2.0));
    $this->assertSame(4.0, Num::multiply(2.0, 2.0));

    $this->assertSame(-2.0, Num::multiply(-1.0, 2.0));
    $this->assertSame(-2.0, Num::multiply(1.0, -2.0));
    $this->assertSame(2.0, Num::multiply(-1.0, -2.0));

    $this->assertSame(0.0, Num::multiply(0.0, 2.0));
    $this->assertSame(0.0, Num::multiply(2.0, 0.0));
    $this->assertSame(0.0, Num::multiply(0.0, 0.0));
  }


  /**
   * Test Pow Integer
   *
   * Test integer powers.
   * @return void
   */
  public function testPowInteger( ) : void {
    $this->assertSame(9.0, Num::pow(3, 2));
    $this->assertSame(9.0, Num::pow(-3, 2));
    $this->assertSame(27.0, Num::pow(3, 3));
    $this->assertSame(-27.0, Num::pow(-3, 3));
  }


  /**
   * Test Pow Decimal
   *
   * Test decimal powers.
   * @return void
   */
  public function testPowDecimal( ) : void {
    $this->assertSame(3.0, Num::pow(9, 0.5));
    $this->assertSame(8.0, Num::pow(4, 1.5));

    $this->assertNAN(Num::pow(-9, 0.5));
  }


  /**
   * Test Pow Negative
   *
   * Test negative powers.
   * @return void
   */
  public function testPowNegative( ) : void {
    $this->assertSame(0.5, Num::pow(2, -1));
    $this->assertSame(0.25, Num::pow(2, -2));
  }


  /**
   * Test Pow Zero
   *
   * Test pow with zeros.
   * @return void
   */
  public function testPowZero( ) : void {
    $this->assertSame(0.0, Num::pow(0, 5));
    $this->assertSame(0.0, Num::pow(0, -5));

    $this->assertSame(1.0, Num::pow(5, 0));
    $this->assertSame(1.0, Num::pow(-5, 0));

    $this->assertNAN(Num::pow(0, 0));
  }


  /**
   * Test Root Integer
   *
   * Test integer roots.
   * @return void
   */
  public function testRootInteger( ) : void {
    $this->assertSame(3.0, Num::root(9, 2));
    $this->assertNAN(Num::root(-9, 2));
    $this->assertSame(3.0, Num::root(27, 3));
    $this->assertSame(-3.0, Num::root(-27, 3));

  }


  /**
   * Test Root Decimal
   *
   * Test decimal roots.
   * @return void
   */
  public function testRootDecimal( ) : void {
    $this->assertSame(9.0, Num::root(3, 0.5));
    $this->assertSame(4.0, Num::root(8, 1.5));
    $this->assertSame(81.0, Num::root(-9, 0.5));
  }


  /**
   * Test Root Negative
   *
   * Test negative roots.
   * @return void
   */
  public function testRootNegative( ) : void {
    $this->assertSame(2.0, Num::root(0.5, -1));
    $this->assertSame(2.0, Num::root(0.25, -2));
  }


  /**
   * Test Root Zero
   *
   * Test root with zeros.
   * @return void
   */
  public function testRootZero( ) : void {
    $this->assertSame(0.0, Num::root(0, 5));
    $this->assertSame(0.0, Num::root(0, -5));

    $this->expectException(Warning::class);
    $this->assertNAN(Num::root(5, 0));

    $this->expectException(Warning::class);
    $this->assertNAN(Num::root(0, 0));
  }


  /**
   * Test Round Default
   *
   * Tests the round method with the default rounding.
   * @return void
   */
  public function testRoundDefault( ) : void {
    $this->assertSame(1.0, Num::round(1));
    $this->assertSame(2.0, Num::round(2));

    $this->assertSame(2.0, Num::round(1.5));
    $this->assertSame(1.0, Num::round(1.4));

    $this->assertSame(-2.0, Num::round(-1.5));
    $this->assertSame(-1.0, Num::round(-1.4));
  }

  /**
   * Test Round Specified
   *
   * Test round to the specified number value.
   * @return void
   */
  public function testRoundSpecified( ) : void {
    $this->assertSame(0.0, Num::round(1, 5));
    $this->assertSame(5.0, Num::round(3, 5));
    $this->assertSame(5.0, Num::round(2.5, 5));
    $this->assertSame(10.0, Num::round(8, 5));

    $this->assertSame(0.0, Num::round(-1, 5));
    $this->assertSame(-5.0, Num::round(-2.5, 5));

    $this->assertSame(0.0, Num::round(0.2, 0.5));
    $this->assertSame(0.5, Num::round(0.25, 0.5));
    $this->assertSame(1.5, Num::round(1.25, 0.5));
    $this->assertSame(-1.5, Num::round(-1.25, 0.5));
  }


  /**
   * Test Subtract
   *
   * Tests subtract.
   * @return void
   */
  public function testSubtract( ) : void {
    $this->assertSame(1.0, Num::subtract(2.0, 1.0));
    $this->assertSame(-1.0, Num::subtract(1.0, 2.0));
    $this->assertSame(-3.0, Num::subtract(-1.0, 2.0));
    $this->assertSame(3.0, Num::subtract(1.0, -2.0));
    $this->assertSame(1.0, Num::subtract(-1.0, -2.0));
    $this->assertSame(-2.0, Num::subtract(0.0, 2.0));
  }
}