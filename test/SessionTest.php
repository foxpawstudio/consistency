<?php

declare(strict_types=1);

use Foxpaw\Consistency\Session;
use PHPUnit\Framework\TestCase;


/**
 * Session Test
 *
 * Tests the Session class.
 */
final class SessionTest extends TestCase {

  /**
   * Prepare
   *
   * Set up the tests.
   * @return void
   */
  public function prepare( ) : void {
    @session_start( );
    $_SESSION['first'] = 'one';
    $_SESSION['second'] = 'two';
    $_SESSION['_flash'] = ['flashFirst' => 'one'];  

    $class = new ReflectionClass(Session::class);
    $method = $class->getMethod('extractFlashData');
    $method->setAccessible(true);
    $_SESSION = $method->invokeArgs(null, [$_SESSION]);
  }


  /**
   * Tear Down
   *
   * Tear down the text fixtures.
   * @return void
   */
  public function tearDown( ) : void {
    $_SESSION = [ ];
    session_abort( );
    session_id("");
  }


  /**
   * Test Delete Returns Data
   *
   * Test delete returns the session data.
   * @return void
   */
  public function testDeleteReturnsData( ) : void {
    $this->prepare( );
    $this->assertSame(['second' => 'two'], Session::delete('first'));
  }


  /**
   * Test Delete Changes Session
   *
   * Test that delete changes the session superglobal.
   * @return void
   */
  public function testDeleteChangesSession( ) : void {
    $this->prepare( );
    Session::delete('second');
    $this->assertSame(['first' => 'one'], $_SESSION);
  }


  /**
   * Test Delete Missing
   *
   * Tests delete correctly handles deleting missing data.
   * @return void
   */
  public function testDeleteMissing( ) : void {
    $this->prepare( );
    $this->assertSame(['first' => 'one', 'second' => 'two'], Session::delete('third'));
    $this->assertSame(['first' => 'one', 'second' => 'two'], $_SESSION);
  }


  /**
   * Test Flash Under Array
   *
   * Tests flash puts the data under the _flash array.
   * @return void
   */
  public function testFlashUnderArray( ) : void {
    $this->prepare( );
    Session::flash('flashSecond', 'two');
    $this->assertSame(['flashSecond' => 'two'], $_SESSION['_flash']);
  }


  /**
   * Test Flash Returns Session
   *
   * Tests flash returns session.
   * @return void
   */
  public function testFlashReturnsSession( ) : void {
    $this->prepare( );
    $this->assertSame(
      ['first' => 'one', 'second' => 'two', '_flash' => ['flashSecond' => 'two']],
      Session::flash('flashSecond', 'two')
    );
  }


  /**
   * Test Get All
   *
   * Tests getting all the session data.
   * @return void
   */
  public function testGetAll( ) : void {
    $this->prepare( );
    $this->assertSame(['first' => 'one', 'second' => 'two'], Session::get( ));
  }


  /**
   * Test Get Single
   *
   * Test getting a single session variable.
   * @return void
   */
  public function testGetSingle( ) : void {
    $this->prepare( );
    $this->assertSame('one', Session::get('first'));
  }


  /**
   * Test Get Nested
   *
   * Test getting a nested attribute.
   * @return void
   */
  public function testGetNested( ) : void {
    $this->prepare( );
    $_SESSION['nested'] = ['find' => 'this'];
    $this->assertSame('this', Session::get('nested.find'));
  }


  /**
   * Test Get Flashed
   *
   * Test getting a flashed piece of data.
   * @return void
   */
  public function testGetFlashed( ) : void {
    $this->prepare( );
    $this->assertSame('one', Session::get('flashFirst'));
  }


  /**
   * Test Get Missing
   *
   * Test getting a missing piece of data.
   * @return void
   */
  public function testGetMissing( ) : void {
    $this->prepare( );
    $this->assertNull(Session::get('third'));
  }


  /**
   * Test Get Default
   *
   * Tests getting the default provided.
   * @return void
   */
  public function testGetDefault( ) : void {
    $this->prepare( );
    $this->assertSame('default', Session::get('third', 'default'));
  }


  /**
   * Test Set Alters Session
   *
   * Test set alters the session.
   * @return void
   */
  public function testSetAltersSession( ) : void {
    $this->prepare( );
    Session::set('third', 'three');
    $this->assertSame(['first' => 'one', 'second' => 'two', 'third' => 'three'], $_SESSION);
  }


  /**
   * Test Set Overwrites
   *
   * Test set overwrites data.
   * @return void
   */
  public function testSetOverwrites( ) : void {
    $this->prepare( );
    Session::set('first', 'two');
    $this->assertSame(['first' => 'two', 'second' => 'two'], $_SESSION);
  }


  /**
   * Test Set Returns Session
   *
   * Tests set returns the session.
   * @return void
   */
  public function testSetReturnsSession( ) : void {
    $this->prepare( );
    $this->assertSame(['first' => 'one', 'second' => 'two', 'third' => 'three'], Session::set('third', 'three'));
  }


  /**
   * Test Set Nested
   *
   * Test setting a nested value.
   * @return void
   */
  public function testSetNested( ) : void {
    $this->prepare( );
    Session::set('nested.attribute', 'value');
    $this->assertSame(['first' => 'one', 'second' => 'two', 'nested' => ['attribute' => 'value']], $_SESSION);
  }


  /**
   * Test Start
   *
   * Test starting the session runs smoothly.
   * @return void
   */
  public function testStart( ) : void {
    $_SESSION['_flash'] = ['flash' => 'data'];
    
    $this->assertSame("", session_id( ));
    $this->assertArrayHasKey('_flash', $_SESSION);

    $id = @Session::start( );

    $this->assertNotSame("", session_id( ));
    $this->assertSame($id, session_id( ));
    $this->assertArrayNotHasKey('_flash', $_SESSION);
  }
}