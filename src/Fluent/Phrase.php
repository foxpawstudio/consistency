<?php

declare(strict_types=1);

namespace Foxpaw\Consistency\Fluent;

use Foxpaw\Consistency\Arr;
use Foxpaw\Consistency\Str;
use Foxpaw\Consistency\Components\Creates;
use Foxpaw\Consistency\Components\Pipes;
use Foxpaw\Consistency\Contracts\Arrays;
use Foxpaw\Consistency\Contracts\Strings;
use ArrayIterator, Traversable;


/**
 * Phrase
 *
 * Fluent phrase object to manage a single string.
 */
class Phrase implements Arrays, Strings {

  use Creates, Pipes;

  /** @var array  The convert methods and their return types. */
  protected $converts = [
    'chunk' => Collection::class,
    'contains' => null,
    'count' => Number::class,
    'endsWith' => null,
    'length' => Number::class,
    'pos' => Number::class,
    'regexSplit' => Collection::class,
    'section' => Collection::class,
    'split' => Collection::class,
    'startsWith' => null,
    'words' => Collection::class
  ];

  /** @var string  The destination to pipe to. */
  protected $destination = Str::class;

  /** @var string  The internal string. */
  protected $str;


  /**
   * Construct
   *
   * Create a new Phrase.
   * @param  string $initial The initial string
   * @return void
   */
  public function __construct(string $initial='') {
    $this->str = $initial;
  }


  /**
   * Call
   *
   * Call method to pass through to the static Str helepr.
   * @param  string $method    The method to call
   * @param  array  $arguments The arguments to provide
   * @return mixed             The chainable new phrase or the return type if converted
   */
  public function __call(string $method, array $arguments) {
    return $this->pipe($method, Arr::unshift($arguments, $this->str));
  }


  /**
   * To String
   *
   * Magic toString method.
   * @return string  The string
   */
  public function __toString( ) : string {
    return $this->toString( );
  }


  /**
   * All
   *
   * Return all the characters of the phrase.
   * @return array  The characters of the phrase in an array
   */
  public function all( ) : array {
    return $this->toArray( );
  }


  /**
   * Get Iterator
   *
   * Retrieve an iterator for this Phrase.
   * @return ArrayIterator  The iterator for the phrase
   */
  public function getIterator( ) : Traversable {
    return new ArrayIterator($this->toArray( ));
  }


  /**
   * Offset Get
   *
   * Get the character at the position.
   * @param  int $offset The offset to get
   * @return char        The character at that position
   */
  public function offsetGet($offset) : self {
    return $this->at($offset);
  }


  /**
   * Offset Exists
   *
   * Check if the offset exists.
   * @param  int $offset The offset to check
   * @return bool        If it exists
   */
  public function offsetExists($offset) : bool {
    return $this->at($offset)->raw( ) !== '';
  }


  /**
   * Offset Set
   *
   * Set an offset at a position.
   * @param  int    $offset The offset to set
   * @param  char   $value  The value to set
   * @return void
   */
  public function offsetSet($offset, $value) : void {
    $this->str = $this->set($value, $offset)->str;
  }


  /**
   * Offset Unset
   *
   * Unset an offset.
   * @param  int $offset The offset to unset
   * @return void
   */
  public function offsetUnset($offset) : void {
    $this->str = $this->delete($offset)->str;
  }


  /**
   * Raw
   *
   * Return the raw internal value.
   * @return void
   */
  public function raw( ) {
    return $this->str;
  }


  /**
   * To Array
   *
   * Convert the phrase characters to an array.
   * @return array  The characters of the phrase as an array
   */
  public function toArray( ) : array {
    return $this->chunk(1)->raw( );
  }


  /**
   * To String
   *
   * Convert the phrase into a string.
   * @return string  The string
   */
  public function toString( ) : string {
    return $this->str;
  }
}