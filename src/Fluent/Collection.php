<?php

declare(strict_types=1);

namespace Foxpaw\Consistency\Fluent;

use Foxpaw\Consistency\Arr;
use Foxpaw\Consistency\Components\Creates;
use Foxpaw\Consistency\Components\Pipes;
use Foxpaw\Consistency\Contracts\Arrays;
use Foxpaw\Consistency\Contracts\Jsons;
use Foxpaw\Consistency\Contracts\Strings;
use ArrayIterator, Traversable;


/**
 * Collection
 *
 * Fluent collection object to manage a single array.
 */
class Collection implements Arrays, Jsons, Strings {

  use Creates, Pipes;

  /** @var array  The convert methods and their return types. */
  protected $converts = [
    'at' => null,
    'count' =>  Number::class,
    'exists' => null,
    'find' => null,
    'first' => null,
    'has' => null,
    'join' => Phrase::class,
    'last' => null,
    'pop' => null,
    'reduce' => null,
    'search' => null,
    'shift' => null,
    'sum' => null,
    'take' => null
  ];

  /** @var string  The destination to pipe to. */
  protected $destination = Arr::class;

  /** @var array  The internal array. */
  protected $arr;


  /**
   * Construct
   *
   * Create a new Collection.
   * @param  array $initial The initial array
   * @return void
   */
  public function __construct(array $initial=[ ]) {
    $this->arr = $initial;
  }


  /**
   * Call
   *
   * Call method to pass through to the static Arr helepr.
   * @param  string $method    The method to call
   * @param  array  $arguments The arguments to provide
   * @return mixed             The chainable new collection or the return type if converted
   */
  public function __call(string $method, array $arguments) {
    return $this->pipe($method, Arr::unshift($arguments, $this->arr));
  }


  /**
   * To String
   *
   * Magic toString method.
   * @return string  The string
   */
  public function __toString( ) : string {
    return $this->toString( );
  }


  /**
   * All
   *
   * Return all the characters of the collection.
   * @return array  The characters of the collection in an array
   */
  public function all( ) : array {
    return $this->toArray( );
  }


  /**
   * Get Iterator
   *
   * Retrieve an iterator for this Collection.
   * @return ArrayIterator  The iterator for the collection
   */
  public function getIterator( ) : Traversable {
    return new ArrayIterator($this->toArray( ));
  }


  /**
   * Offset Get
   *
   * Get the character at the position.
   * @param  int $offset The offset to get
   * @return char        The character at that position
   */
  public function offsetGet($offset) {
    return $this->find($offset);
  }


  /**
   * Offset Exists
   *
   * Check if the offset exists.
   * @param  int $offset The offset to check
   * @return bool        If it exists
   */
  public function offsetExists($offset) : bool {
    return $this->find($offset) !== null;
  }


  /**
   * Offset Set
   *
   * Set an offset at a position.
   * @param  int    $offset The offset to set
   * @param  char   $value  The value to set
   * @return void
   */
  public function offsetSet($offset, $value) : void {
    $this->arr = !is_null($offset) ? 
      $this->set($offset, $value)->arr :
      $this->push($value)->arr;
  }


  /**
   * Offset Unset
   *
   * Unset an offset.
   * @param  int $offset The offset to unset
   * @return void
   */
  public function offsetUnset($offset) : void {
    $this->arr = $this->delete($offset)->arr;
  }


  /**
   * Raw
   *
   * Return the raw internal value.
   * @return void
   */
  public function raw( ) {
    return $this->arr;
  }


  /**
   * To Array
   *
   * Convert the collection to an array.
   * @return array  The characters of the collection as an array
   */
  public function toArray( ) : array {
    return $this->arr;
  }


  /**
   * To Json
   *
   * Convert the collection to json.
   * @return string  The json encoded collection.
   */
  public function toJson( ) : string {
    return json_encode($this->toArray( ));
  }


  /**
   * To String
   *
   * Convert the collection into a string.
   * @return string  The string
   */
  public function toString( ) : string {
    return $this->toJson( );
  }
}