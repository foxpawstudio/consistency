<?php

declare(strict_types=1);

namespace Foxpaw\Consistency\Fluent;

use Foxpaw\Consistency\Arr;
use Foxpaw\Consistency\Fluent\Collection;
use Foxpaw\Consistency\Components\Attributes;
use Foxpaw\Consistency\Components\Creates;
use Foxpaw\Consistency\Contracts\Arrays;
use Foxpaw\Consistency\Contracts\Jsons;
use Foxpaw\Consistency\Contracts\Strings;
use InvalidArgumentException;


/**
 * Cookie
 *
 * Fluent cookie object to manage a single user cookie.
 */
class Cookie implements Arrays, Jsons, Strings {

  use Attributes, Creates;

  /** @var array  The cache of cookies. */
  protected static $cache;

  /** @var array  The fillable attributes on the cookie. */
  protected $fillable = ['name', 'value', 'expires', 'path', 'domain', 'secure', 'http'];


  /**
   * Construct
   *
   * Create a new cookie object.
   * @param  array $attributes The initial attributes
   * @return void
   */
  public function __construct(array $attributes=[ ]) {
    $defaults = [
      'name' => '',
      'value' => '',
      'expires' => 0,
      'path' => '',
      'domain' => '',
      'secure' => false,
      'http' => false
    ];
    $attributes = Arr::merge($defaults, $attributes);
    $this->attributes = $this->fill($attributes)->attributes;
  }


  /**
   * Delete
   *
   * Delete a cookie.
   * @return bool  Success of delete
   */
  public function delete( ) : bool {
    if(!static::$cache) static::$cache = new Collection;
    $_COOKIE = Arr::delete($_COOKIE, $this->getAttribute('name'));
    static::$cache = static::$cache->delete($this->getAttribute('name'));

    $this->setAttribute('expires', time( ) - 3600)->save( );
    return !Arr::exists($_COOKIE, $this->getAttribute('name'));
  }


  /**
   * Find
   *
   * Find a cookie.
   * @param  string  $name    The cookie to find
   * @param  mixed   $default The default if not found
   * @param  bool    $force   Whether to force reload
   * @return Cookie?          The found cookie
   */
  public static function find(string $name, $default=null, bool $force=false) {
    return self::get($force)->find($name, $default);
  }


  /**
   * Get
   *
   * Retrieve all the cookies.
   * @param  bool  $force Whether to force reload
   * @return array
   */
  public static function get(bool $force=false) : Collection {
    if($force || !static::$cache)
      static::$cache = Collection::create(Arr::map(
        $_COOKIE,
        function($cookie, $name) { return new static(json_decode($cookie, true) ?? ['name' => $name, 'value' => $cookie]); }
      ));
    return static::$cache;
  }


  /**
   * Save
   *
   * Save cookie changes.
   * @return bool  Success of save
   */
  public function save( ) : bool {
    if(!$this->getAttribute('name')) throw new InvalidArgumentException("Must define a name for cookie.");
    if(!static::$cache) static::$cache = new Collection;
    if($this->getAttribute('expires', 0) === 0 || $this->getAttribute('expires') > time( )) {
      $_COOKIE = Arr::set($_COOKIE, $this->getAttribute('name'), $this->toString( ));
      static::$cache[$this->getAttribute('name')] = $this;
    }

    return setcookie(
      $this->getAttribute('name'),
      $this->toString( ),
      $this->getAttribute('expires'),
      $this->getAttribute('path'),
      $this->getAttribute('domain'),
      $this->getAttribute('secure'),
      $this->getAttribute('http')
    );
  }
}