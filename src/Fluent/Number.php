<?php

declare(strict_types=1);

namespace Foxpaw\Consistency\Fluent;

use Foxpaw\Consistency\Arr;
use Foxpaw\Consistency\Num;
use Foxpaw\Consistency\Components\Creates;
use Foxpaw\Consistency\Components\Pipes;
use Foxpaw\Consistency\Contracts\Strings;


/**
 * Number
 *
 * Fluent phrase object to manage a single string.
 */
class Number implements Strings {

  use Creates, Pipes;

  /** @var array  The convert methods and their return types. */
  protected $converts = [
    'format' => Phrase::class,
    'isDivisibleBy' => null,
    'isEven' => null,
    'isOdd' => null,
  ];

  /** @var string  The destination to pipe to. */
  protected $destination = Num::class;

  /** @var float  The internal number this fluent object stored. */
  protected $num;


  /**
   * Construct
   *
   * Create a new fluent Number.
   * @param  float|integer $initial The initial number to use
   * @return void
   */
  public function __construct(float $initial=0) {
    $this->num = $initial;
  }


  /**
   * Call
   *
   * Call method to pass through to the static Num helepr.
   * @param  string $method    The method to call
   * @param  array  $arguments The arguments to provide
   * @return mixed             The chainable new phrase or the return type if converted
   */
  public function __call(string $method, array $arguments) {
    return $this->pipe($method, Arr::unshift($arguments, $this->num));
  }


  /**
   * To String
   *
   * Magic toString method to convert this object to a string.
   * @return string  The string version of this object
   */
  public function __toString( ) : string {
    return $this->toString( );
  }


  /**
   * Raw
   *
   * Retrieve the raw value.
   * @return float  The raw internal value
   */
  public function raw( ) : float {
    return $this->num;
  }


  /**
   * To String
   *
   * Method to convert this object to a string.
   * @return string  The string version of this object
   */
  public function toString( ) : string {
    return (string)$this->num;
  }
}