<?php

declare(strict_types=1);

namespace Foxpaw\Consistency;


/**
 * Input
 *
 * Class based helper for retrieving input from get, post and HTTP
 * headers.
 */
class Input {

  /**
   * Get
   *
   * Retrieve one or all get variables.
   * @param  string? $attribute The attribute to retrieve, if not supplied will return all
   * @param  mixed?  $default   The default if not found
   * @return mixed              The requested get values
   */
  public static function get(string $attribute=null, $default=null) {
    $get = is_array($_GET) ? $_GET : [ ];
    if(is_null($attribute)) return $get;
    return Arr::find($get, $attribute, $default);
  }


  /**
   * Header
   *
   * Retrieve one or all headers.
   * @param  string? $attribute The attribute to retrieve, if not supplied will return all
   * @param  mixed?  $default   The default if not found
   * @return mixed              The requested header value
   */
  public static function header(string $attribute=null, $default=null) {
    $namespace = __NAMESPACE__;
    if(!function_exists('getallheaders') && !function_exists("{$namespace}\\getallheaders")) {
      function getallheaders( ) {
        $headers = [ ];
        foreach(Input::server( ) as $key => $value)
          if(Str::startsWith($key, 'HTTP_'))
            $headers[Str::hyphen(Str::title(Str::substr($key, 5)))] = $value;
        return $headers;
      }
    }

    if(is_null($attribute)) return getallheaders( );
    return Arr::find(getallheaders( ), $attribute, $default);
  }


  /**
   * Post
   * 
   * Retrieve one or all post variables.
   * @param  string? $attribute The attribute to retrieve, if not supplied will return all
   * @param  mixed?  $default   The default if not found
   * @return mixed              The requested post values
   */
  public static function post(string $attribute=null, $default=null) {
    $post = is_array($_POST) ? $_POST : [ ];
    if(is_null($attribute)) return $post;
    return Arr::find($post, $attribute, $default);
  }


  /**
   * Server
   * 
   * Retrieve one or all server variables.
   * @param  string? $attribute The attribute to retrieve, if not supplied will return all
   * @param  mixed?  $default   The default if not found
   * @return mixed              The requested server values
   */
  public static function server(string $attribute=null, $default=null) {
    $server = is_array($_SERVER) ? $_SERVER : [ ];
    if(is_null($attribute)) return $server;
    return Arr::find($server, $attribute, $default);
  }
}