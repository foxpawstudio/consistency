<?php

declare(strict_types=1);

namespace Foxpaw\Consistency;


/**
 * Arr
 *
 * Class based helper for array functions. All functions are not
 * mutative unless specified.
 */
class Arr {

  /**
   * At
   *
   * Retrieve an item by its position.
   * @param  array  $items    The items to retrieve from
   * @param  int    $position The position to retrieve from
   * @param  mixed  $default  The default if not found
   * @return mixed            The value or default           
   */
  public static function at(array $items, int $position, $default=null) {
    if($position < 0) $position += self::count($items);
    return self::find(self::values($items), $position, $default);
  }


  /**
   * Chunk
   *
   * Chunk an array into sub arrays of a specified size. If it does not
   * chunk evenly the last element will be smaller.
   * @param  array  $items The array of items
   * @param  int    $size  The size to chunk to
   * @return array         The array chunked
   */
  public static function chunk(array $items, int $size) : array {
    if($size === 0) return [ ];
    return $size > 0 ?
      array_chunk($items, $size) :
      self::reverse(self::map(array_chunk(self::reverse($items), -$size), [static::class, 'reverse']));
  }


  /**
   * Combine
   *
   * Combine a key array with a value array into an associative array.
   * Keys from both arrays will be discarded.
   * @param  array  $keys   The array to use for keys
   * @param  array  $values The array to use for values
   * @return array          The combined array keys to values
   */
  public static function combine(array $keys, array $values) : array {
    return array_combine($keys, $values);
  }


  /**
   * Count
   *
   * Determine the number of elements in an array.
   * @param  array  $items The array to count
   * @return int           The number of elements in the array
   */
  public static function count(array $items) : int {
    return count($items);
  }


  /**
   * Delete
   *
   * Delete an item from the array. Will mutate the original array.
   * Supports dot notation indexing
   * @param  array      $items     The items to delete from
   * @param  int|string $attribute The attribute to delete
   * @return array                 The items with the value deleted
   */
  public static function delete(array $items, $attribute) : array {
    if(!is_string($attribute) || !Str::contains($attribute, '.')) {
      unset($items[$attribute]);
      return $items;
    }

    $part = Str::until($attribute, '.');
    if(!self::exists($items, $part)) return $items;

    $items[$part] = self::delete($items[$part], ltrim(Str::from($attribute, '.'), '.'));
    return $items;
  }


  /**
   * Diff
   *
   * Computes the difference between two arrays. Uses an optional
   * callback function to determine difference. The callback should
   * return 0, positive or negative values for equal, greater or less
   * than respectively.
   * @param  array         $items    The items to diff
   * @param  array         $diff     The items to diff against
   * @param  callable?     $callback Custom equality callback ($a, $b)
   * @return array                   The elements in items which do not appear in diff
   */
  public static function diff(array $items, array $diff, callable $callback=null) : array {
    return $callback ? array_udiff($items, $diff, $callback) : array_diff($items, $diff);
  }


  /**
   * Diff Associated
   *
   * Computes the difference between two arrays using both keys and
   * values. Has optional callback parameters for a value compare
   * callback and a key compare callback. The callback should
   * return 0, positive or negative values for equal, greater or less
   * than respectively.
   * @param  array         $items         The items to diff
   * @param  array         $diff          The items to diff against
   * @param  callable?     $valueCallback The callback on the values ($a, $b)
   * @param  callable?     $keyCallback   The callback on the keys ($a, $b)
   * @return array                        The key, element pairs in items which do not appear in diff
   */
  public static function diffAssociated(array $items, array $diff,
    callable $valueCallback=null, callable $keyCallback=null
  ) : array {
      if(is_null($valueCallback) && is_null($keyCallback)) return array_diff_assoc($items, $diff);
      if(is_null($keyCallback)) return array_udiff_assoc($items, $diff, $valueCallback);
      if(is_null($valueCallback)) return array_diff_uassoc($items, $diff, $keyCallback);
      return array_udiff_uassoc($items, $diff, $keyCallback, $valueCallback);
  }


  /**
   * Diff Key
   *
   * Computes the difference between two arrays using the keys.
   * Has optional callback parameter to compute equality. The callback
   * should return 0, positive or negative values for equal, greater or
   * less than respectively.
   * @param  array         $items    The items to diff
   * @param  array         $diff     The items to diff against
   * @param  callable?     $callback The callback to compute equality ($a, $b)
   * @return array                   The elements in items which are different by key in diff
   */
  public static function diffKey(array $items, array $diff, callable $callback=null) : array {
    return $callback ? array_diff_ukey($items, $diff, $callback) : array_diff_key($items, $diff);
  }


  /**
   * Exists
   *
   * Determines whether a key exists in the provided array. Supports
   * nested queries using dot notation.
   * @param  array  $items The items to check for the key
   * @param  mixed  $key   The key to check for
   * @return bool          Whether the key exists
   */
  public static function exists(array $items, $key) : bool {
    if(!is_string($key) || !Str::contains($key, '.'))
      return array_key_exists($key, $items);

    $part = Str::until($key, '.');
    if(!array_key_exists($part, $items)) return false;
    return self::exists($items[$part], ltrim(Str::from($key, '.'), '.'));
  }


  /** 
   * Expand
   *
   * Expand a flattened array.
   * @param  array  $items The items to expand
   * @return array         The expanded array
   */
  public static function expand(array $items) : array {
    $expanded = [ ];
    $callback = function($key, $value, &$expanded) use(&$callback) {
      if(!Str::contains("{$key}", '.'))
        return !!$expanded[$key] = $value;

      $first    = Str::until($key, '.');
      $continue = ltrim(Str::from($key, '.'), '.');
      if(!is_array(self::find($expanded, $first))) $expanded[$first] = [ ];
      return $callback($continue, $value, $expanded[$first]);
    };

    foreach($items as $key => $item) $callback($key, $item, $expanded);
    return $expanded;
  }


  /**
   * Fill
   *
   * Fill an array with values.
   * @param  int    $items  The start index, if negative first index will be as specified then start from zero
   * @param  uint   $length The length of the array to fill
   * @param  mixed  $value  The value to fill with
   * @return array          The filled array
   */
  public static function fill(int $start, int $length, $value) : array {
    return array_fill($start, $length, $value);
  }


  /**
   * Filter
   *
   * Filters an array of 'empty' values. Optional filter callback,
   * return falsey value to remove.
   * @param  array         $items    The items to filter
   * @param  callable?     $callback The callback to filter by ($value, $key)
   * @return array                   The filtered array
   */
  public static function filter(array $items, callable $callback=null) : array {
    return $callback ? array_filter($items, $callback, ARRAY_FILTER_USE_BOTH) : array_filter($items);
  }


  /**
   * Find
   *
   * Find an item from the array by key. Supports dot notation for
   * nested paths.
   * @param  array      $items   The items to find from
   * @param  string|int $key     The key to look for, if negative will retrieve from end of array
   * @param  mixed?     $default The default value if not found
   * @return mixed               The value if found or default if not
   */
  public static function find(array $items, $key, $default=null) {
    if(!is_string($key) || !Str::contains($key, '.'))
      return self::exists($items, $key) ? $items[$key] : $default;

    $part = Str::until($key, '.');
    return self::exists($items, $part) ? 
      self::find($items[$part], ltrim(Str::from($key, '.'), '.')) :
      $default;
  }


  /**
   * First
   *
   * Return the first value in the array.
   * 
   * @param  array  $items The items to retrieve the first for
   * @param  mixed         The default if there's no items
   * @return mixed         The first item
   */
  public static function first(array $items, $default=null) {
    $values = self::values($items);
    return self::count($items) ? reset($values) : $default;
  }


  /**
   * Flatten
   *
   * Flatten an array to dot notation.
   * @param  array  $items The items to flatten
   * @return array         The flattened items
   */
  public static function flatten(array $items) : array {
    $flattened = [ ];
    $callback  = function(array $items, string $prefix='') use(&$callback, &$flattened) {
      foreach($items as $key => $item) {
        if(is_array($item)) $callback($item, "{$prefix}{$key}.");
        else                $flattened["{$prefix}{$key}"] = $item;
      }
    };
    $callback($items);
    return $flattened;
  }


  /**
   * Flip
   *
   * Flip an array.
   * 
   * @param  array  $items The items to flip
   * @return array         The flipped items
   */
  public static function flip(array $items) : array {
    return array_flip($items);
  }


  /**
   * Has
   *
   * Determine whether an array has a value.
   * 
   * @param  array   $items  The items to search in
   * @param  mixed   $item   The item to find
   * @param  bool    $strict Whether to use strict matching
   * @return bool            Whether the array has the value
   */
  public static function has(array $items, $item, bool $strict=false) : bool {
    return array_search($item, $items, $strict) !== false;
  }


  /**
   * Intersect
   *
   * Determine the intersection between original and filter. The callback
   * should return 0, positive or negative values for equal, greater or
   * less than respectively.
   * @param  array      $original The array to filter
   * @param  array      $filter   The array to compare against
   * @param  callable?  $callback The callback to compare with ($a, $b)
   * @return array                The array of intersected values
   */
  public static function intersect(array $original, array $filter, callable $callback=null) : array {
    return $callback ? array_uintersect($original, $filter, $callback) : array_intersect($original, $filter);
  }


  /**
   * Intersect Associated
   *
   * Determine the intersection between original and filter based on
   * both the key and value, Two optional callbacks for determining the
   * key and value intersection. The callback should return 0, positive
   * or negative values for equal, greater or less than respectively.
   * @param  array         $original      The array to filter
   * @param  array         $filter        The array to compare against
   * @param  callable?     $valueCallback The callback to compare the values with ($a, $b)
   * @param  callable?     $keyCallback   The callback to compare the keys with ($a, $b)
   * @return array                        The intersection between arrays
   */
  public static function intersectAssociated(
    array $original, array $filter,
    callable $valueCallback=null, callable $keyCallback=null
    ) : array {
      if(is_null($valueCallback) && is_null($keyCallback)) return array_intersect_assoc($original, $filter);
      if(is_null($keyCallback)) return array_uintersect_assoc($original, $filter, $valueCallback);
      if(is_null($valueCallback)) return array_intersect_uassoc($original, $filter, $keyCallback);
      return array_uintersect_uassoc($original, $filter, $keyCallback, $valueCallback);
  }


  /**
   * Intersect Key
   *
   * Determine the intersection between two arrays based on the keys.
   * Optional callback to determine the intersection. The callback should
   * return 0, positive or negative values for equal, greater or less
   * than respectively.
   * @param  array         $original The original array
   * @param  array         $filter   The array to intersect with
   * @param  callable?     $callback The callback to determine intersection
   * @return array                   The result of the intersection
   */
  public static function intersectKey(array $original, array $filter, callable $callback=null) : array {
    return $callback ? array_intersect_ukey($original, $filter, $callback) : array_intersect_key($original, $filter);
  }


  /**
   * Join
   *
   * Join an array by a delimiter into a string.
   * @param  array   $items     The items to join
   * @param  string? $delimiter The delimiter to join with
   * @return string             The joined string
   */
  public static function join(array $items, string $delimiter='') : string {
    return implode($delimiter, $items);
  }


  /**
   * Keys
   * 
   * Retrieve the keys of the array. 
   * @param  array  $items The items to retrieve from
   * @return array         The array keys
   */
  public static function keys(array $items) : array {
    return array_keys($items);
  }

  /**
   * Last
   *
   * Retrieve the last item in the array.
   * @param  array  $items   The array to retrieve the last item from
   * @param  mixed  $default The default if the array is empty
   * @return mixed           The last item in the array
   */
  public static function last(array $items, $default=null) {
    if(!self::count($items)) return $default;
    $values = self::values($items);
    return end($values);
  }


  /**
   * Map
   *
   * Apply a callback to each element of an array.
   * @param  array        $items     The items to map
   * @param  callable     $callback  The callback to apply
   * @return array                   The new array
   */
  public static function map(array $items, callable $callback) : array {
    $map = [ ];
    foreach($items as $key => $value)
      $map[$key] = $callback($value, $key);
    return $map;
  }


  /**
   * Merge
   *
   * Merge two arrays together.
   * @param  array  $original  The original array
   * @param  array  $additions The array to merge 
   * @return array             The merged array
   */
  public static function merge(array $original, array $additions) : array {
    return array_merge($original, $additions);
  }


  /**
   * Pad
   * 
   * Pad an array to a specified length. If length is positive will
   * pad from the right, negative will pad on the left.
   * @param  array  $items  The array to pad
   * @param  int    $length The length to pad to
   * @param  mixed  $with   The value to pad with
   * @return array          The padded array
   */
  public static function pad(array $items, int $length, $with=null) : array {
    return array_pad($items, $length, $with);
  }

  /**
   * Pop
   *
   * Pop an element of the end of the array. Will mutate the original
   * array.
   * @param  array  $items   The array to pop from
   * @param  mixed  $default The default if the array is empty
   * @return mixed           The popped item
   */
  public static function pop(array &$items, $default=null) {
    return self::count($items) ? array_pop($items) : $default;
  }


  /**
   * Pull
   * 
   * Pull a set of items out of the array. Will mutate the original
   * array.
   * @param  array      $items  The items to pull from
   * @param  int|string $start  The start index, negative will start from end
   * @param  int?       $length The number of items to pull, if negative will pull until that many from end
   * @return array              The pulled items
   */
  public static function pull(array &$items, $start, int $length=null) : array {
    if(!is_int($start)) $start = self::find(self::flip(self::keys($items)), $start);
    if(is_null($start)) return [ ];
    return array_splice($items, $start, $length ?: self::count($items));
  }


  /**
   * Push
   *
   * Push an item onto the end of the array.
   * @param  array  $items The array to push the item on to
   * @param  mixed  $item  The item to add
   * @return array         The array with the item pushed on
   */
  public static function push(array $items, $item) : array {
    $items[ ] = $item;
    return $items;
  }


  /**
   * Reduce
   *
   * Reduce an array to a single value.
   * @param  array    $items    The array to reduce
   * @param  callable $callback The callback to reduce($carry, $item)
   * @param  mixed?   $initial  The initial value
   * @return mixed              The reduced value
   */
  public static function reduce(array $items, callable $callback, $initial=null) {
    return array_reduce($items, $callback, $initial);
  }


  /**
   * Replace
   *
   * Replace from from start, length items with the provided
   * replacement array.
   * @param  array       $items  The array to replace items from
   * @param  array       $with   The replacement
   * @param  int|string  $start  The start key
   * @param  int         $length The length to replace
   * @return array               The original array with replacements
   */
  public static function replace(array $items, array $with, $start, int $length) : array {
    if(!is_int($start)) $start = self::find(self::flip(self::keys($items)), $start);
    if(is_null($start)) return null;
    array_splice($items, $start, $length, $with);
    return $items;
  }


  /**
   * Reverse
   *
   * Reverse the array.
   * @param  array  $items The array to reverse
   * @return array         The reversed array
   */
  public static function reverse(array $items) : array {
    return array_reverse($items);
  }


  /**
   * search
   *
   * search a key for a value within the provided array.
   * @param  array  $items   The items to search in
   * @param  mixed  $item    The item to search for
   * @param  mixed? $default The default to return if not found
   * @param  bool?  $strict  Whether to use strict matching of types
   * @return int|string      The key of the found item
   */
  public static function search(array $items, $item, $default=null, bool $strict=false) {
    $found = array_search($item, $items, $strict);
    return $found === false ? $default : $found;
  }


  /**
   * Section
   *
   * Break an array into a specified number of sections.
   * @param  array  $items    The array to section
   * @param  int    $sections The number of sections, negative will stack end heavy
   * @return array            The array broken into sections
   */
  public static function section(array $items, int $sections) : array {
    if($sections === 0) return [ ];
    $count    = self::count($items);
    $result   = self::pad([ ], $sections, [ ]);
    if($count === 0) return $result;

    $cost     = abs($sections) / $count;
    $counter  = 0;
    $index    = 0;

    if($sections < 0) $items = self::reverse($items);

    foreach($items as $item) {
      if($sections > 0) $result[$index][ ] = $item;
      else array_unshift($result[$index], $item);

      if(($counter += $cost) >= ($index + 1)) $index++;
    }
    return $sections > 0 ? $result : self::reverse($result);
  }


  /**
   * Set
   *
   * Set an array value. Supports dot index notation.
   * @param  array      $items     The items
   * @param  int|string $attribute The attribute to set
   * @param  mixed      $value     The value to set
   * @return array                 The altered items
   */
  public static function set(array $items, $attribute, $value) : array {
    if(!is_string($attribute) || !Str::contains($attribute, '.')) {
      $items[$attribute] = $value;
      return $items;
    }

    $part = Str::until($attribute, '.');
    if(!self::exists($items, $part)) $items[$part] = [ ];
    $items[$part] = self::set($items[$part], ltrim(Str::from($attribute, '.'), '.'), $value);
    return $items;  
  }


  /**
   * Shift
   *
   * Shift an item off the front of the array. Will mutate the original
   * array.
   * @param  array  $items   The array to shift on to
   * @param  mixed  $default Default to return if array empty
   * @return mixed           The item shifted off
   */
  public static function shift(array &$items, $default=null) {
    return array_shift($items) ?: $default;
  }


  /**
   * Slice
   *
   * Retrieve a segment of the array.
   * @param  array       $items  The array to slice from
   * @param  int|string  $start  The element to start from
   * @param  int?        $length The number of items to slice, or until the end
   * @return array               The slice of array
   */
  public static function slice(array $items, $start, int $length=null) : array {
    if(!is_int($start)) $start = self::find(self::flip(self::keys($items)), $start);
    if(is_null($start)) return [ ];
    return array_slice($items, $start, $length);
  }


  /**
   * Sort
   *
   * Sort the array.
   * @param  array      $items    The items to sort
   * @param  callable?  $callback An optional sort callback($a, $b)
   * @return array                The sorted array
   */
  public static function sort(array $items, callable $callback=null) : array {
    $callback ? usort($items, $callback) : sort($items);
    return $items;
  }


  /**
   * Sum
   *
   * Sums the array. Converts non numeric values to ints, and strings to
   * zero.
   * @param  array  $items The array to sum
   * @return mixed         The summed array
   */
  public static function sum(array $items) {
    return array_sum($items);
  }


  /**
   * Take
   *
   * Take an item out of the array. Will mutate the original
   * array. Will reindex if numeric keys.
   * @param  array      $items   The array to take from
   * @param  int|string $key     The key to take
   * @param  mixed      $default The default to return if key doesn't exist.
   * @return mixed               The taken item
   */
  public static function take(array &$items, $key, $default=null) {
    return self::first(self::pull($items, $key, 1), $default);
  }


  /**
   * Unique
   *
   * Remove duplicates from the array.
   * @param  array  $items The items to remove duplicates from
   * @return array         The array with duplicated removed
   */
  public static function unique(array $items) : array {
    return array_unique($items);
  }


  /**
   * Unshift
   *
   * Append an item to the start of the array.
   * @param  array  $items The array to add to
   * @param  mixed  $item  The item to add
   * @return array         The array with the item added
   */
  public static function unshift(array $items, $item) : array {
    array_unshift($items, $item);
    return $items;
  }


  /**
   * Values
   *
   * Retrieve the values of an array.
   * @param  array  $items The array to retreive the values from
   * @return array         The array values indexed numerically from 0
   */
  public static function values(array $items) : array {
    return array_values($items);
  }
}