<?php

declare(strict_types=1);

namespace Foxpaw\Consistency\Contracts;

use ArrayAccess, IteratorAggregate;


/**
 * Arrays
 *
 * Arrays interface used to contract an array implementation.
 */
interface Arrays extends ArrayAccess, IteratorAggregate {

  /**
   * All
   *
   * Return all the data as an array but do not convert any items.
   * @return array  The array
   */
  public function all( ) : array;

  /**
   * To Array
   *
   * Convert this object and all sub objects to an array.
   * @return array  The array
   */
  public function toArray( ) : array;
}