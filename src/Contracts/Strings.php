<?php

declare(strict_types=1);

namespace Foxpaw\Consistency\Contracts;


/**
 * Strings
 *
 * Strings interface used to contract an string implementation.
 */
interface Strings {

  /**
   * To String
   *
   * To string magic method.
   * @return string  The stringified version of the object
   */
  public function __toString( ) : string;

  /**
   * To String
   *
   * Method to convert the object to a string.
   * @return string  The stringified version of the object
   */
  public function toString( ) : string;
}