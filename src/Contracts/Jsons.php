<?php

declare(strict_types=1);

namespace Foxpaw\Consistency\Contracts;


/**
 * Jsons
 *
 * Jsons interface used to contract an json string implementation.
 */
interface Jsons {

  /**
   * To Json
   *
   * Convert this object to a json form.
   * @return string  The json version of the string
   */
  public function toJson( ) : string;
}