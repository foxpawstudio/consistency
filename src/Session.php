<?php

declare(strict_types=1);

namespace Foxpaw\Consistency;


/**
 * Session
 *
 * Class based helper for manipulating and retrieving session
 * variables.
 */
class Session {

  /** @var array  The flashed data */
  private static $flash;


  /**
   * Delete
   *
   * Delete a session variable.
   * @param  int|string  $attribute  The attribute to delete
   * @return array|bool              The new session data or false if not available
   */
  public static function delete($attribute) : array {
    if(!self::start( )) return false;
    $_SESSION = Arr::delete($_SESSION, $attribute);
    return $_SESSION;
  }



  /**
   * Extract Flash Data
   *
   * Extracts the flash data from the provided session.
   * @param  array  $session The session data
   * @return array           The session data with the flash data extracted
   */
  protected static function extractFlashData(array $session) : array {
    self::$flash = Arr::find($session, '_flash', [ ]);
    return Arr::delete($session, '_flash');
  }


  /**
   * Flash
   *
   * Flash a piece of data to session memory. Will only last for this
   * request.
   * @param  int|string  $attribute The attribute to flash
   * @param  mixed       $value     The value to flash
   * @return array|bool             False if not set or session data if set
   */
  public static function flash($attribute, $value) {
    return self::set("_flash.{$attribute}", $value);
  }


  /**
   * Get
   *
   * Get a session variable or the entire block if none specified.
   * @param  int|string    $attribute The attribute name
   * @param  mixed?        $default  The default if not found
   * @return mixed                   The requested data
   */
  public static function get($attribute=null, $default=null) {
    if(!self::start( )) return;
    if(is_null($attribute)) return $_SESSION;

    return Arr::exists(self::$flash, $attribute) ?
      Arr::find(self::$flash, $attribute, $default) :
      Arr::find($_SESSION, $attribute, $default);
  }
  

  /**
   * Set
   *
   * Set a session variable. Supports dot nesting of attributes
   * @param  int|string    $attribute The attribute to set
   * @param  mixed         $value     The value to set
   * @return array|bool               False if not set or session data if set
   */
  public static function set($attribute, $value) {
    if(!self::start( )) return false;
    $_SESSION = Arr::set($_SESSION, $attribute, $value);
    return $_SESSION;
  }


  /**
   * Start
   *
   * Starts the session. Will automatically be called however can
   * manually be called to retrieve the session id.
   * @return string         The session id
   */
  public static function start( ) : string {
    if(session_id( )) return session_id( );
    session_start( );
    $_SESSION = self::extractFlashData($_SESSION ?: [ ]);
    return session_id( );
  }
}