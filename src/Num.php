<?php

declare(strict_types=1);

namespace Foxpaw\Consistency;


/**
 * Num
 *
 * Class based helper for numeric functions. All functions are not
 * mutative unless specified.
 */
class Num {

  /**
   * Abs
   *
   * Find the absolute value of a value.
   * @param  float  $var The value to find the absolute value of
   * @return float       The absolute value.
   */
  public static function abs(float $var) : float {
    return abs($var);
  }


  /**
   * Add
   *
   * Add two numbers.
   * @param  float $one The first number to add
   * @param  float $two The second number to add
   * @return flaot      The addition of the numbers
   */
  public static function add(float $one, float $two) : float {
    return $one + $two;
  }


  /**
   * Ceil
   *
   * Ceil the provided number.
   * @param  float  $var The value to ceil
   * @return int         The ceiled number
   */
  public static function ceil(float $var) : float {
    return ceil($var);
  }


  /**
   * Divide
   *
   * Divide first number by the second.
   * @param  float  $one The number to divide
   * @param  float  $two The divisor
   * @return float       The result of the division
   */
  public static function divide(float $one, float $two) : float {
    return $one / $two;
  }


  /**
   * Floor
   *
   * Round a number down.
   * @param  float  $var The value to floor
   * @return float       The floored value
   */
  public static function floor(float $var) : float {
    return floor($var);
  }


  /**
   * Format
   *
   * Format a number to the number of decimal places with the specified
   * separators.
   * @param  float       $var       The value to format
   * @param  int?        $places    The number of decimal places
   * @param  string?     $dec       The decimal separator
   * @param  string?     $thousands The thousands separator
   * @return string                 The formatted number
   */
  public static function format(float $var, int $places=0, string $dec='.', string $thousands=',') : string {
    return number_format($var, $places, $dec, $thousands);
  }



  /**
   * Is Divisible By
   *
   * Determine if the number is evenly divisible by the other.
   * @param  float   $var     The value to test
   * @param  float   $divisor The value to test
   * @return boolean          If the number is divisible by the divisor
   */
  public static function isDivisibleBy(float $var, float $divisor) : bool {
    return self::mod($var, $divisor) === 0.0;
  }



  /**
   * Is Even
   *
   * Determine if a number is even.
   * @param  float   $var The value to test
   * @return boolean      If the value is even
   */
  public static function isEven(float $var) : bool {
    return $var !== 0.0 && self::isDivisibleBy($var, 2);
  }



  /**
   * Is Odd
   *
   * Test if a number is odd.
   * @param  float   $var The value to test
   * @return boolean      If the value is odd
   */
  public static function isOdd(float $var) : bool {
    return $var !== 0.0 && !self::isDivisibleBy($var, 2);
  }


  /**
   * Log
   *
   * Find the logarithm of a number.
   * @param  float  $var  The number to log
   * @param  float? $base The base, defaults to the natural log
   * @return float        The logarithm requested
   */
  public static function log(float $var, float $base=M_E) : float {
    return log($var, $base);
  }


  /**
   * Mod
   *
   * Calculate the modulus between two numbers.
   * @param  float  $var The value to mod
   * @param  float  $mod The modulus value
   * @return float       The result
   */
  public static function mod(float $var, float $mod) : float {
    return $var % $mod;
  }


  /**
   * Multiply
   *
   * Multiply two numbers together.
   * @param  float  $one The first number
   * @param  float  $two The second number
   * @return float       The result
   */
  public static function multiply(float $one, float $two) : float {
    return $one * $two;

  }


  /**
   * Pow
   *
   * Raise the value to a power.
   * @param  float  $var   The value to raise
   * @param  float  $power The power to raise to
   * @return float         The result
   */
  public static function pow(float $var, float $power) : float {
    if($var === 0.0) return $power ? 0 : NAN;
    return $var ** $power;
  }


  /**
   * Root
   *
   * Take the nth root of a value.
   * @param  float  $var  The value to root
   * @param  float  $root The root to take
   * @return float        The result
   */
  public static function root(float $var, float $root) : float {
    return $var < 0 && self::isOdd($root) ? -self::pow(-$var, 1 / $root) : self::pow($var, 1 / $root);
  }


  /**
   * Round
   *
   * Round the number to the provided presicion away from zero.
   * @param  float $var The value to round
   * @param  float $to  What to round to
   * @return float      The rounded value
   */
  public static function round(float $var, float $to=1.0) : float {
    $to = abs($to);
    $modifier = $var > 0 ? $to / 2 : $to / -2;
    return intval(($var + $modifier) / $to) * $to;
  }
  

  /**
   * Subtract
   *
   * Subtract the second number from the first.
   * @param  float  $one The first number
   * @param  float  $two The number to subtract
   * @return float       The result
   */
  public static function subtract(float $one, float $two) : float {
    return $one - $two;
  }
}