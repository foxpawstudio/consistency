<?php

declare(strict_types=1);

namespace Foxpaw\Consistency\Components;

use Foxpaw\Consistency\Arr;
use Foxpaw\Consistency\Str;
use ArrayIterator, Traversable;


/**
 * Attributes
 *
 * Attributes trait that exposes a accessor/mutator pattern when
 * storing data on a class. Also adds ability to define the fillable
 * attributes of an array. Most methods are self chaininable where
 * possible and will return a clone instead of mutating the original.
 */
trait Attributes {

  /** @var array  Any extra non-stored attributes to append when exporting. */
  // protected $appends = [ ];

  /** @var array  The attributes on the object. */
  protected $attributes = [ ];

  /** @var array  The fillable attributes on the object. */
  // protected $fillable = [ ];

  /** @var array  The attributes to hide when exporting the object. */
  // protected $hides = [ ];


  /**
   * Get
   *
   * Get magic method shorthand for get method.
   * @param  string $attribute The attribute to retrieve
   * @return mxied             The attribute value
   */
  public function __get(string $attribute) {
    return $this->getAttribute($attribute);
  }


  /**
   * Isset
   *
   * Isset magic method to determine if an attribute has been set.
   * @param  string  $attribute The attribute to check
   * @return bool               If the attribute is set and not null
   */
  public function __isset(string $attribute) : bool {
    return $this->hasAttribute($attribute);
  }


  /**
   * Set
   *
   * Set magic method shorthand for set method.
   * @param  string $attribute The attribute to set
   * @param  mixed  $value     The value to set
   * @return void
   */
  public function __set(string $attribute, $value) : void {
    $this->attributes = $this->setAttribute($attribute, $value)->attributes;
  }


  /**
   * To String
   *
   * To string magic method to convert to a string.
   * @return string  The string version of this object
   */
  public function __toString( ) : string {
    return $this->toString( );
  }


  /**
   * Unset
   *
   * Unset magic method to delete the attribute.
   * @param  string $attribute The attribute to delete
   * @return void
   */
  public function __unset(string $attribute) : void {
    $this->attributes = $this->deleteAttribute($attribute)->attributes;
  }


  /**
   * All
   *
   * Retrieve all the attribute data.
   * @return array  The attributes as an array
   */
  public function all( ) : array {
    $attributes = $this->attributes;
    foreach($this->getAppends( ) as $key) $attributes[$key] = null;
    foreach($this->getHides( ) as $key) unset($attributes[$key]);
    return Arr::map($attributes, function($value, $key) { return $this->accessAttribute($key, $value); });
  }


  /**
   * Access Attribute
   *
   * Access an attribute value if there is a accessor method.
   * @param  string $attribute The attribute to access
   * @param  mixe   $value     The provided value to access
   * @return mixed             The mutated value
   */
  public function accessAttribute(string $attribute, $value) {
    if(!$this->hasAccessor($attribute)) return $value;
    $accessor = $this->accessorFor($attribute);
    return $this->$accessor($value);
  }


  /**
   * Accessor For
   *
   * Return the accessor method for the attribute.
   * @param  string $attribute The attribute to access
   * @return string            The accessor method name
   */
  protected function accessorFor(string $attribute) : string {
    return 'access' . Str::studly($attribute);
  }


  /**
   * Delete Attribute
   *
   * Delete the attribute.
   * @param  string $attribute The attribute to delete
   * @return self              The current object
   */
  public function deleteAttribute(string $attribute) : self {
    $new = clone $this;
    $new->attributes = Arr::delete($new->attributes, $attribute);
    return $new;
  }


  /**
   * Fill
   *
   * Fill the object with the specified attributes.
   * @param  array  $with The new attributes
   * @return self         The current object
   */
  public function fill(array $with) : self {
    $new = clone $this;
    foreach($with as $attribute => $value)
      $new = $new->setAttribute($attribute, $value);
    return $new;
  }


  /**
   * Get Appends
   *
   * Get the appended attributes.
   * @return array  The appended attributes
   */
  protected function getAppends( ) : array {
    return property_exists($this, 'appends') && is_array($this->appends) ? $this->appends : [ ];
  }


  /**
   * Get Attribute
   *
   * Gets the attribute.
   * @param  string $attribute The attribute to retrieve
   * @param  mixed? $default   The default value to pass through if attribute does not exist
   * @return mxied             The attribute value
   */
  public function getAttribute(string $attribute, $default=null) {
    return $this->accessAttribute($attribute, Arr::find($this->attributes, $attribute, $default));
  }


  /**
   * Get Fillable
   *
   * Get the fillable attributes.
   * @return array  The fillable attributes
   */
  protected function getFillable( ) : array {
    return property_exists($this, 'fillable') && is_array($this->fillable) ? $this->fillable : [ ];
  }


  /**
   * Get Hides
   *
   * Get the hidden attributes.
   * @return array  The hidden attributes
   */
  protected function getHides( ) : array {
    return property_exists($this, 'hides') && is_array($this->hides) ? $this->hides : [ ];
  }


  /**
   * Get Iterator
   *
   * Get an iterator for the object.
   * @return ArrayIterator  The iterator for the internal attributes
   */
  public function getIterator( ) : Traversable {
    return new ArrayIterator($this->attributes);
  }


  /**
   * Has Accessor
   *
   * Determine if this object has an accessor method.
   * @param  string  $attribute The attribute to check
   * @return bool               If the object has an accessor method
   */
  public function hasAccessor(string $attribute) : bool {
    return method_exists($this, $this->accessorFor($attribute));
  }


  /**
   * Has Attribute
   *
   * Determine if the object has the attribute.
   * @param  string  $attribute The attribute
   * @return bool               If the object has the attribute
   */
  public function hasAttribute(string $attribute) : bool {
    return $this->getAttribute($attribute) !== null;
  }


  /**
   * Has Mutator
   *
   * Determine if this object has an mutator method.
   * @param  string  $attribute The attribute to check
   * @return bool               If the object has an mutator method
   */
  public function hasMutator(string $attribute) : bool {
    return method_exists($this, $this->mutatorFor($attribute));
  }


  /**
   * Is Fillable
   *
   * Determine if an attribute is fillable on the array.
   * @param  string $attribute The attribute to check
   * @return bool              If the attribute is fillable
   */
  public function isFillable(string $attribute) : bool {
    return !Arr::count($this->getFillable( )) ||
      Arr::has($this->getFillable( ), $attribute);
  }


  /**
   * Mutate Attribute
   *
   * Mutate an attribute value if there is a mutator method.
   * @param  string $attribute The attribute to mutate
   * @param  mixe   $value     The provided value to mutate
   * @return mixed             The mutated value
   */
  public function mutateAttribute(string $attribute, $value) {
    if(!$this->hasMutator($attribute)) return $value;
    $mutator = $this->mutatorFor($attribute);
    return $this->$mutator($value);
  }


  /**
   * Mutator For
   *
   * Return the mutator method for the attribute.
   * @param  string $attribute The attribute to mutate
   * @return string            The mutator method name
   */
  protected function mutatorFor(string $attribute) : string {
    return 'mutate' . Str::studly($attribute);
  }


  /**
   * Offset Exists
   *
   * Allows for isset checks using array notation on attributes.
   * @param  int|string $offset The attribute to check
   * @return bool               If the attribute is set
   */
  public function offsetExists($attribute) : bool {
    return $this->hasAttribute($attribute);
  }


  /**
   * Offset Get
   *
   * Allows for array style getting of attributes.
   * @param  int|string $offset The offset to get
   * @return mixed              The value at that offset
   */
  public function offsetGet($offset) {
    return $this->getAttribute($offset);
  }


  /**
   * Offset Set
   *
   * Allows for array style setting of attributes.
   * @param  int|string $offset The offset to set
   * @param  mixed      $value  The value to set
   * @return void
   */
  public function offsetSet($offset, $value) : void {
    $this->attributes = $this->setAttribute($offset, $value)->attributes;
  }
  

  /**
   * Offset Unset
   *
   * Allows for array style unsetting.
   * @param  int|string $offset The offset to unset
   * @return void
   */
  public function offsetUnset($offset) : void {
    $this->attributes = $this->deleteAttribute($offset)->attributes;
  }


  /**
   * Raw
   *
   * Retreive the raw data stored.
   * @return array  The raw attributes
   */
  public function raw( ) : array {
    return $this->attributes;
  }


  /**
   * Set Attribute
   *
   * Set the attribute.
   * @param  string $attribute The attribute to set
   * @param  mixed  $value     The value to set
   * @return self              The current object
   */
  public function setAttribute(string $attribute, $value) : self {
    $new = clone $this;
    if($new->isFillable($attribute))
      $new->attributes = Arr::set($new->attributes, $attribute, $new->mutateAttribute($attribute, $value));
    return $new;
  }


  /**
   * To Array
   *
   * To array method, converts all attributes and any attribute with a
   * toArray method to an array.
   * @return array  The array
   */
  public function toArray( ) : array {
    return Arr::map(
      $this->all( ),
      function($value) { 
        if(method_exists($value, 'toArray')) $value = $value->toArray( );
        return $value;
      }
    );
  }


  /**
   * To Json
   *
   * Retrieve the json version of the object.
   * @return string  The json string
   */
  public function toJson( ) : string {
    return json_encode($this->toArray( ));
  }


  /**
   * To String
   *
   * Retrieve the string version of this object.
   * @return string  The string version of the object
   */
  public function toString( ) : string {
    return $this->toJson( );
  }
}