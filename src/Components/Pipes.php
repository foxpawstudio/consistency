<?php

declare(strict_types=1);

namespace Foxpaw\Consistency\Components;

use Foxpaw\Consistency\Arr;
use InvalidArgumentException;


/**
 * Pipes
 *
 * Pipes trait which allows for simple piping of method calls to
 * another object and remapping of return types.
 */
trait Pipes {

  /** @var object|string  The pipe destination. */
  // protected $destination;

  /** @var array  The convert methods and their return types. */
  // protected $converts = [ ];


  /**
   * Call
   *
   * Call method to pass through to the destination.
   * @param  string $method    The method to call
   * @param  array  $arguments The arguments to provide
   * @return mixed             The return of the pipe
   */
  public function __call(string $method, array $arguments) {
    return $this->pipe($method, $arguments);
  }


  /**
   * Pipe
   *
   * Pipe a method call with the provided arguments to the provided
   * object.
   * @param  string         $method    The method to call
   * @param  array          $arguments The arguments to provide
   * @param  object|string? $to        The object or class to pipe to (if null will use the)
   * @return mixed                     The chained object or the return type defined
   */
  public function pipe(string $method, array $arguments, $to=null) {
    $to = $to ?? ($this->destination ?? null);
    if(!is_object($to) && !class_exists($to))
      throw new InvalidArgumentException('Must define destination on object or provide to pipe call.');

    $result   = is_string($to) ? $to::$method(...$arguments) : $to->$method(...$arguments);
    $converts = is_array($this->converts) ? $this->converts : [ ];

    if(!Arr::exists($converts, $method)) return new static($result);

    $type = Arr::find($converts, $method);
    if($type === null) return $result;
    if(class_exists($type)) return new $type($result);
    settype($result, $type);
    return $result;
  }
}