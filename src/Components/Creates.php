<?php

declare(strict_types=1);

namespace Foxpaw\Consistency\Components;


/**
 * Creates
 *
 * Creates trait which exposes the creates static method for instant
 * fluent creation and chaining.
 */
trait Creates {

  /**
   * Create
   *
   * Create a new instance of this object.
   * @return static   The created object
   */
  public static function create( ) : self {
    return new static(...func_get_args( ));
  }
}