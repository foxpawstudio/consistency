<?php

declare(strict_types=1);

namespace Foxpaw\Consistency;


/**
 * Str
 *
 * Class based helper for string functions. All functions are not
 * mutative unless specified.
 */
class Str {

  /**
   * Append
   *
   * Append end to start.
   * @param  string $start The start of the string
   * @param  string $end   The end of the string
   * @return string        The appended string
   */
  public static function append(string $start, string $end) : string {
    return "{$start}{$end}";
  }

  /**
   * At
   *
   * Find a character at a position.
   * @param  string $str The string to look at
   * @param  int    $pos The position to look at 
   * @return char        THe character at that position
   */
  public static function at(string $str, int $pos) : string {
    if($pos < 0) $pos += self::length($str);
    return self::length($str) <= $pos || $pos < 0 ? '' : $str[$pos];
  }


  /**
   * Camel
   *
   * Return a camel case version of a string.
   * @param  string $str The string to camel case
   * @return string      The camel case version
   */
  public static function camel(string $str) : string {
    return lcfirst(
        Arr::join(
          Arr::map(self::words($str), function($word) { return ucfirst(self::lower($word)); })
        )
    );
  }


  /**
   * Chunk
   *
   * Chunk a string into chunks of specified size.
   * @param  string $str  The string to chunk
   * @param  int    $size The size to chunk
   * @return array        The chunked string
   */
  public static function chunk(string $str, int $size) : array {
    if($size === 0) return [ ];

    return $size > 0 ?
      str_split($str, $size) :
      Arr::map(Arr::reverse(str_split(self::reverse($str), -$size)), [static::class, 'reverse']);
  }
  

  /**
   * Contains
   *
   * Determine whether a string contains a substring.
   * @param  string $str    The string to search in
   * @param  string $search The string to search for
   * @return bool           If the string contains the substring
   */
  public static function contains(string $str, string $search) : bool {
    if(!self::length($str) || !self::length($search)) return false;
    return strpos($str, $search) !== false;
  }


  /**
   * Count
   *
   * Determine the number of occurences of a substring contained in
   * string.
   * @param  string $str    The string to count in
   * @param  string $search The string to search for
   * @return int            The number of occurences
   */
  public static function count(string $str, string $search) : int {
    if(!self::length($str) || !self::length($search)) return 0;
    return substr_count($str, $search);
  }


  /**
   * Delete
   *
   * Delete a single character by position. Negative offset will delete
   * from end.
   * @param  string $str    The string to replace
   * @param  int    $offset The offset to delete
   * @return string         The string with the character removed
   */
  public static function delete(string $str, int $offset) : string {
    return self::set($str, '', $offset);
  }


  /**
   * Ends With
   *
   * Determine if the string ends with the specified string.
   * @param  string $str  The string to observe
   * @param  string $with The string to check with
   * @return bool         If the string ends with the other string
   */
  public static function endsWith(string $str, string $with) : bool {
    $length = self::length($with);
    return $length > 0 && (substr($str, -$length) === $with);
  }


  /**
   * From
   *
   * Retrieve a string from a certain delimiter.
   * @param  string $str       The string to analyse
   * @param  string $delimiter The delimiter to look for
   * @return string            The original string from the delimiter
   */
  public static function from(string $str, string $delimiter) : string {
    if(!self::length($str) || !self::length($delimiter)) return '';
    return strstr($str, $delimiter) ?: '';
  }


  /**
   * Hyphen
   *
   * Return the string as hyphen-case.
   * @param  string $str The string to hyphenate
   * @return string      The string in hyphen case
   */
  public static function hyphen(string $str) : string {
    return Arr::join(self::words($str), '-');
  }


  /**
   * Length
   *
   * Determine the length of a string.
   * @param  string $str The string to determine the length of
   * @return int         The length of the string
   */
  public static function length(string $str) : int {
    return strlen($str);
  }


  /**
   * Lower
   *
   * Convert a string to lower case.
   * @param  string $str The string to operate on
   * @return string      The lower case string
   */
  public static function lower(string $str) : string {
    return strtolower($str);
  }


  /**
   * Pos
   *
   * Determine the first position search occurs in string.
   * @param  string $str    The string to search in
   * @param  string $search The string to search for
   * @return int            The first position of search in string
   */
  public static function pos(string $str, string $search) : int {
    if(!self::length($str) || !self::length($search)) return -1;
    $pos = strpos($str, $search);
    return $pos === false ? -1 : $pos;
  }


  /**
   * Prepend
   *
   * Prepend prefix to the original.
   * @param  string $original The oringinal string
   * @param  string $prefix   The prefix
   * @return string           The prepended string
   */
  public static function prepend(string $original, string $prefix) : string {
    return "{$prefix}{$original}";
  }


  /**
   * Regex Replace
   *
   * Replace on a string using a regex pattern.
   * @param  string          $str     The string to operate on
   * @param  string          $regex   The regex string
   * @param  callable|string $replace A callback to replace or a string to replace with
   * @return string                   The replaced string
   */
  public static function regexReplace(string $str, string $regex, $replace) : string {
    if(!self::length($str) || !self::length($regex)) return $str;
    return is_callable($replace) ? preg_replace_callback($regex, $replace, $str) : preg_replace($regex, $replace, $str);
  }


  /**
   * Regex Split
   *
   * Split a string based on a regex pattern.
   * @param  string $str   The string to split
   * @param  string $regex The regex to split on
   * @return array         The split string
   */
  public static function regexSplit(string $str, string $regex) : array {
    if(!self::length($str) || !self::length($regex)) return [$str];
    return preg_split($regex, $str);
  }
  

  /**
   * Remove
   *
   * Removes the first occurance of a substring from a string.
   * @param  string $str    The string to remove from
   * @param  string $remove The string to remove
   * @return string         The string with the part removed
   */
  public static function remove(string $str, string $remove) : string {
    if(!self::length($str) || !self::length($remove)) return $str;
    return self::replace($str, $remove, '');
  }

  
  /**
   * Replace
   *
   * Replace the first instance of search withing string.
   * @param  string $str     The string to operate on
   * @param  string $search  The string to search for
   * @param  string $replace The string to replace with
   * @return string          The altered string
   */
  public static function replace(string $str, string $search, string $replace) : string {
    $pos = self::pos($str, $search);
    if($pos < 0) return $str;
    return substr_replace($str, $replace, $pos, self::length($search));
  }


  /**
   * Reverse
   *
   * Reverse the given string.
   * @param  string $str The string to reverse
   * @return string      The reveresed string
   */
  public static function reverse(string $str) : string {
    return strrev($str);
  }


  /**
   * Section
   *
   * Split a string into a number of sections.
   * @param  string $str      String to section
   * @param  int    $sections Number of sections
   * @return array            The string in sections
   */
  public static function section(string $str, int $sections) : array {
    if($sections === 0) return [ ];
    $length   = self::length($str);
    $truncate = $sections > 0 ? 'ceil' : 'floor';
    $chunks   = $length ? intval($truncate($length / $sections)) : 1;

    return Arr::pad(self::chunk($str, $chunks), $sections, '');
  }


  /**
   * Set
   *
   * Set a character by position in a string. Negative offset will 
   * set offset from end.
   * @param string $str     The string
   * @param char   $replace The replacement character
   * @param int    $offset  The position
   */
  public static function set(string $str, string $replace, int $offset) : string {
    if(abs($offset) >= self::length($str)) return $str;
    return substr_replace($str, $replace, $offset, 1);
  }


  /**
   * Snake
   *
   * Convert a string to snake case (underscored).
   * @param  string $str The string to convert
   * @return string      The converted string
   */
  public static function snake(string $str) : string {
    return Arr::join(self::words($str), '_');
  }


  /**
   * Snippet
   *
   * Retrieve a snippet of text from a string.
   * @param  string       $str        The string to extract rom
   * @param  int          $length     The length of the snippet
   * @param  bool         $breakWords Whether or not to break words
   * @return string                   The snippet
   */
  public static function snippet(string $str, int $length, bool $breakWords=false) : string {
    if(abs($length) > self::length($str)) return $str;

    $snippet = $length < 0 ? self::substr($str, $length) : self::substr($str, 0, $length);
    $spaceIndex = $length < 0 ? $length - 1 : $length;
    if(!$breakWords && self::at($str, $spaceIndex) != ' ')
      $snippet = $length < 0 ? 
        self::from($snippet, ' ') : 
        self::reverse(self::from(self::reverse($snippet), ' '));

    return trim($snippet);
  }


  /**
   * Split
   *
   * Split a string based on a delimiter.
   * @param  string $str       The string to split
   * @param  string $delimiter The delimiter to split on
   * @return array             The string split into an array
   */
  public static function split(string $str, string $delimiter) : array {
    if(!self::length($str)) return [$str];
    if(!self::length($delimiter)) return str_split($str);
    return explode($delimiter, $str);
  }


  /**
   * Starts With
   *
   * Determine if the provided string starts with the specified section.
   * @param  string $str  The string to observe
   * @param  string $with The string to check it starts with
   * @return bool         If the string starts with the specified prefix
   */
  public static function startsWith(string $str, string $with) : bool {
    if(!self::length($str) || !self::length($with)) return false;
    return self::substr($str, 0, self::length($with)) === $with;
  }


  /**
   * Studly
   *
   * Convert a stirng to studly case.
   * @param  string $str The string to convert
   * @return string      The studly case string
   */
  public static function studly(string $str) : string {
    return ucfirst(self::camel($str));
  }


  /**
   * Substr
   *
   * Retrieve a subtring based on positions. A null length will 
   * retrieve until the end of the string, a negative length will
   * retrieve until that many characters from the end. A negative start
   * value will start that many characters from the end.
   * @param  string   $str    The string to retrieve from
   * @param  int      $start  The start position
   * @param  int?     $length The length of the substring.
   * @return string           The substring
   */
  public static function substr(string $str, int $start, int $length=null) : string {
    if(self::length($str) < abs($start)) return '';
    return substr($str, $start, $length ?? self::length($str) - $start);
  }


  /**
   * Title
   *
   * Convert a string to title case.
   * @param  string $str The string to convert
   * @return string      The converted string
   */
  public static function title(string $str) : string {
    return Arr::join(
      Arr::map(self::words($str), function($word) { return ucfirst(self::lower($word)); }),
      ' '
    );
  }


  /**
   * Until
   *
   * Retreive a string until a demiliter is found.
   * @param  string $str       The string to search in
   * @param  string $delimiter The delimiter to search for
   * @return string            The string until the delimiter
   */
  public static function until(string $str, string $delimiter) : string {
    $from = self::from($str, $delimiter);
    if(!self::length($from)) return $str; 
    return self::remove($str, $from);
  }


  /**
   * Upper
   *
   * Convert a string to uppercase.
   * @param  string $str The string to operate on
   * @return string      The uppercase string
   */
  public static function upper(string $str) : string {
    return strtoupper($str);
  }


  /** 
   * Words
   *
   * Split a string into words. Words are delimited by _ , . ; : or
   * transitions between lower and upper case letters such as 
   * camelCase.
   * @param  string  $str The string to find words for
   * @return array        The words
   */
  public static function words(string $str) : array {
    return Arr::filter(self::regexSplit($str, '/[ _,\.;:]|(?<=[a-z])(?=[A-Z])/'));
  }
}